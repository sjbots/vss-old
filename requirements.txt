sphinx
sphinx_rtd_theme

numpy
scipy
matplotlib
numba

jupyter
ipython
sympy

pyqt5
opencv-python
opencv-contrib-python
pyserial

peewee
