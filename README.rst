Bem vindo ao repositório da equipe de robótica SJBOTS
=======================================================

Para obter uma cópia desse repositório será necessário cloná-lo com os comandos:

>>> git clone https://gitlab.com/sjbots/vss.git
>>> git submodule init

O primeiro comando clona a biblioteca. O segundo clone as dependências.

Em seguida, siga para o link da documentação do projeto clicando
`aqui <https://sjbots.gitlab.io/vss/>`_.
