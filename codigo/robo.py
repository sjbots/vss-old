# -*- coding: utf-8 -*-


class RoboVSS:
    """
    Classe Robo

    Essa é classe representa os robôs.

    Matematicamente falando, o comportamento cinemático do robô pode ser descrito com o uso
    de seis variáveis representadas no eixo de coordenadas inercial:

    * x: posição do robô no eixo x (em metros)
    * y: posição sobre o eixo y (em metros)
    * theta: ângulo de orientação (em radianos)
    * __dx: velocidade sobre o eixo x (em metros/segundo)
    * dy velocidade sobre o eixo y (em metros/segundo)
    * dtheta velocidade de rotação (em radianos/segundos)

    Adicionalmente, também são necessários alguns parâmetros relacionados com a construção
    do robô. São eles:

    :param L: é a largura do robô (em metros e, no caso do VSS, é no máximo 0,08m)
    :param r: é o raio das rodinhas (em metros)
    :param m: é a massa total do robô (em kg)
    :param mMotor: é a massa de cada motor (já com o encoder e rodinha, em kg)
    :param mBateria: é a massa da bateria (em kg)
    :param wMax: é a maior velocidade de rotação das rodinhas que o robô consegue atingir (em m/s)
    :param vMax: é a maior velocidade linear que o robô consegue atingir (em m/s)
    :param dthetaMax: é a maior velocidade angular que o robô consegue atingir (em rad/s)



    """
    def __init__(self, m, L, r):
        """
        Construtor da classe Robo
        """

        # Estado do robô
        self.x = 0
        self.y = 0
        self.theta = 0
        self.dx = 0
        self.dy = 0
        self.dtheta = 0

        # Parâmetros de construção do robô
        self.m = m
        self.L = L
        self.r = r

        # Começar a construção das matrizes do modelo linear
        # e calcular os parâmetros do modelo não-linear

    def calculaMomentoDeInerciaRobo(self, metodo):
        return -1

    def calculaMomentoDeInterciaMotor(self, metodo):
        return -1

