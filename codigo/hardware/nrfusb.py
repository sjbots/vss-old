from serial import Serial
import time
import struct
from enum import IntEnum


# Definitions
NRFUSB_ID = 0x41
BROADCAST_ID = 0xFE
MEMORY_TABLE_SIZE = 256
WHO_AM_I_DEFAULT = 65


class PA_LEVEL(IntEnum):
    MIN = 0
    LOW = 1
    HIGH = 2
    MAX = 3
    ERROR = 4


class DATA_RATE(IntEnum):
    RATE_250KBPS = 2
    RATE_1MBPS = 0
    RATE_2MBPS = 1


class CRC(IntEnum):
    DISABLED = 0
    LENGTH_8 = 1
    LENGTH_16 = 2


def time_ms():
    return time.time_ns() / 1e6


# Message Class
class Message:
    class Field(IntEnum):
        HEADER_1 = 0
        HEADER_2 = 1
        ID = 2
        LENGTH = 3
        COMMAND = 4
        ERROR = 4
        PARAMETER_FIRST = 5
        CHECKSUM = 6

    HEADER_1_VALUE = 0xFF
    HEADER_2_VALUE = 0xFF

    def __init__(self):
        # Fields
        self.header_1 = None
        self.header_2 = None
        self.id = None
        self.length = None
        self.parameters = list()
        self.checksum = None

    def set_header_1(self, new_value=HEADER_1_VALUE):
        self.header_1 = new_value

    def set_header_2(self, new_value=HEADER_2_VALUE):
        self.header_2 = new_value

    def set_id(self, new_value=NRFUSB_ID):
        self.id = new_value

    def set_length(self, new_value=2):
        self.length = new_value

    def append_parameter(self, value: int):
        raise NotImplementedError()

    # TEST THIS FUNCTION AS SOON AS POSSIBLE!
    def insert_parameter(self, index:int, value: int):
        if index > self.get_number_of_parameters():
            self.length = self.length + 1

        self.parameters.insert(index, value)

    def set_checksum(self, new_value: int):
        self.checksum = new_value

    def get_header_1(self):
        return self.header_1

    def get_header_2(self):
        return self.header_2

    def get_parameter(self, what_parameter: int):
        return self.parameters[what_parameter]

    def get_parameters(self):
        return self.parameters

    def get_number_of_parameters(self):
        return len(self.parameters)

    def get_id(self):
        return self.id

    def get_length(self):
        return self.length

    def get_checksum(self):
        return self.checksum

    def is_from_quark(self):
        got_my_id = False
        if self.get_id() == NRFUSB_ID:
            got_my_id = True

        return got_my_id

    def is_for_broadcast(self):
        got_broadcast_id = False
        if self.get_id() == BROADCAST_ID:
            got_broadcast_id = True

        return got_broadcast_id

    def clean(self):
        raise NotImplementedError()

    def update_checksum_value(self):
        calculated_checksum = self.calculate_checksum()
        self.set_checksum(calculated_checksum)

    def calculate_checksum(self):
        raise NotImplementedError()

    def is_checksum_valid(self):
        is_valid = False
        if self.checksum == self.calculate_checksum():
            is_valid = True
        return is_valid

    def get_packet(self):
        raise NotImplementedError()

    def get_total_packet_size(self):
        return self.length + 4

    def is_valid(self):
        is_it = True

        if self.get_header_1() != self.HEADER_1_VALUE:
            is_it = False

        if self.get_header_2() != self.HEADER_2_VALUE:
            is_it = False

        if not self.is_checksum_valid():
            is_it = False

        return is_it

    def print(self):
        data = self.get_packet()
        print(' '.join(['%02X' % field for field in data]))


class InstructionPacket(Message):
    class Command(IntEnum):
        PING = 0x01
        READ = 0x02
        WRITE = 0x03
        REG_WRITE = 0x04
        ACTION = 0x05
        FACTORY_RESET = 0x06
        REBOOT = 0x08
        SYNC_WRITE = 0x83
        BULK_READ = 0x92

    def __init__(self):
        super().__init__()
        self.command = None

    def clean(self):
        self.header_1 = None
        self.header_2 = None
        self.id = None
        self.length = None
        self.command = None
        self.parameters = list()
        self.checksum = None

    def set_command(self, new_value: Command):
        self.command = new_value

    def get_command(self):
        return self.command

    def calculate_checksum(self):
        calculated_checksum = self.id + self.length + self.command + sum(self.parameters)
        return (~calculated_checksum) & 0xFF

    def get_packet(self):
        return [self.header_1, self.header_2, self.id, self.length, self.command] + self.parameters + [self.checksum]

    def create_new_message(self, command: Command):
        self.clean()
        self.set_header_1()
        self.set_header_2()
        self.set_id()
        self.set_length()
        self.set_command(command)

    def append_parameter(self, value: int):
        self.parameters.append(value)
        self.length = self.length + 1


class StatusPacket(Message):
    class Error(IntEnum):
        INVALID_INSTRUCTION = 1 << 6
        OVERLOAD_ERROR = 1 << 5
        INVALID_CHECKSUM = 1 << 4
        RANGE_ERROR = 1 << 3
        OVERHEATING_ERROR = 1 << 2
        ANGLE_LIMIT_ERROR = 1 << 1
        INPUT_VOLTAGE_ERROR = 1 << 0
        NONE = 0

    def __init__(self):
        super().__init__()
        self.error = None

    def clean(self):
        self.header_1 = None
        self.header_2 = None
        self.id = None
        self.length = None
        self.error = None
        self.parameters = list()
        self.checksum = None

    def append_parameter(self, value: int):
        self.parameters.append(value)

    def set_error(self, new_value: Error):
        self.error = new_value

    def get_error(self):
        return self.error

    def clear_error(self, what_error: Error):
        actual_errors = self.get_error()
        self.set_error(actual_errors & ((~what_error) & 0xFF))

    def clear_errors(self):
        self.set_error(self.Error.NONE)

    def append_error(self, what_error: Error):
        actual_errors = self.get_error()
        self.set_error(actual_errors | what_error)

    def calculate_checksum(self):
        calculated_checksum = self.id + self.length + self.error + sum(self.parameters)
        return (~calculated_checksum) & 0xFF

    def get_packet(self):
        return [self.header_1, self.header_2, self.id, self.length, self.error] + self.parameters + [self.checksum]

    def create_new_message(self):
        self.set_header_1()
        self.set_header_2()
        self.set_id()
        self.set_length()
        self.set_error(self.Error.NONE)


class Messenger:
    class State:
        LOOKING_FOR_HEADER_1 = 0
        LOOKING_FOR_HEADER_2 = 1
        LOOKING_FOR_ID = 2
        LOOKING_FOR_LENGTH = 3
        LOOKING_FOR_CMD = 4
        RECEIVING_PARAMETERS = 5
        LOOKING_FOR_CHECKSUM = 6

    def __init__(self, port, baud=115200, timeout_ms=2):
        self.response_packet = StatusPacket()
        self.instruction_packet = InstructionPacket()
        self.new_msg = False
        self.timeout_ms = timeout_ms
        self.state = self.State.LOOKING_FOR_HEADER_1

        self.uart = Serial(
            port,
            baudrate=baud,
            timeout=timeout_ms
        )

        self.uart.flushInput()

    def isr_receveing_msg(self, new_byte: bytes):
        if self.state == self.State.LOOKING_FOR_HEADER_1:
            if new_byte[0] == self.response_packet.HEADER_1_VALUE:
                self.response_packet.clean()
                self.response_packet.set_header_1()
                self.state = self.State.LOOKING_FOR_HEADER_2

        elif self.state == self.State.LOOKING_FOR_HEADER_2:
            if new_byte[0] == self.response_packet.HEADER_2_VALUE:
                self.response_packet.set_header_2()
                self.state = self.State.LOOKING_FOR_ID

            else:
                self.state = self.State.LOOKING_FOR_HEADER_1

        elif self.state == self.State.LOOKING_FOR_ID:
            self.response_packet.set_id(new_byte[0])
            self.state = self.State.LOOKING_FOR_LENGTH

        elif self.state == self.State.LOOKING_FOR_LENGTH:
            self.response_packet.set_length(new_byte[0])
            self.state = self.State.RECEIVING_PARAMETERS

        elif self.state == self.State.RECEIVING_PARAMETERS:
            length = self.response_packet.get_length() - 2
            if (self.response_packet.error is None) and (len(self.response_packet.parameters) == 0):
                self.response_packet.error = new_byte[0]

            # The checksum was received
            elif len(self.response_packet.parameters) == length:
                self.response_packet.set_checksum(new_byte[0])
                self.state = self.State.LOOKING_FOR_HEADER_1

                # If the message is from Quark
                if self.response_packet.is_from_quark():
                    self.new_msg = True

            else:
                self.response_packet.append_parameter(new_byte[0])

    def send(self, packet: Message):
        msg = packet.get_packet()
        self.uart.write(msg)
        self.uart.flush()


class NrfUsb:
    class Register(IntEnum):
        STATUS = 0
        POWER_CONFIG = 1
        CHANNEL = 2
        TRANSMISSION = 3
        PAYLOAD_SIZE = 4
        AUTO_ACK = 5
        ADDRESS_WIDTH = 6
        PIPE0_ADDRESS = 7
        PIPE1_ADDRESS = 12
        PIPE2_ADDRESS = 17
        PIPE3_ADDRESS = 18
        PIPE4_ADDRESS = 19
        PIPE5_ADDRESS = 20
        OPENED_PIPES = 21
        SPI_SPEED = 22
        WHO_AM_I = 23
        RETRIES = 62
        WRITE_AS = 63
        PIPE0_DATA = 64
        PIPE1_DATA = 96
        PIPE2_DATA = 128
        PIPE3_DATA = 160
        PIPE4_DATA = 192
        PIPE5_DATA = 224

    class STATUS(IntEnum):
        IS_CHIP_CONNECTED = 0
        IS_CHIP_CONNECTED_MASK = (1 << IS_CHIP_CONNECTED)
        IS_P_VARIANT = 1
        IS_P_VARIANT_MASK = (1 << IS_P_VARIANT)
        IS_LEGITIMATE_RADIO = 2
        IS_LEGITIMATE_RADIO_MASK = (1 << IS_LEGITIMATE_RADIO)
        CARRIER_ON_CURRENT_CHANNEL = 3
        CARRIER_ON_CURRENT_CHANNEL_MASK = (1 << CARRIER_ON_CURRENT_CHANNEL)
        IS_CURRENT_CHANNEL_NOISY = 4
        IS_CURRENT_CHANNEL_NOISY_MASK = (1 << IS_CURRENT_CHANNEL_NOISY)
        TX_OK = 5
        TX_OK_MASK = (1 << TX_OK)
        TX_FAIL = 6
        TX_FAIL_MASK = (1 << TX_FAIL)

    class POWER_CONFIG(IntEnum):
        LOW_POWER_MODE = 0
        LOW_POWER_MODE_MASK = (1 << LOW_POWER_MODE)
        PA_LEVEL = 1
        PA_LEVEL_MASK = (0b11 << PA_LEVEL)
        LISTENING = 3
        LISTENING_MASK = (1 << LISTENING)

    class TRANSMISSION(IntEnum):
        DATA_RATE = 0
        DATA_RATE_MASK = (0b11 << DATA_RATE)
        ENABLE_DYNAMIC_PAYLOADS = 2
        ENABLE_DYNAMIC_PAYLOADS_MASK = (1 << ENABLE_DYNAMIC_PAYLOADS)
        ENABLE_DYNAMIC_ACK = 3
        ENABLE_DYNAMIC_ACK_MASK = (1 << ENABLE_DYNAMIC_ACK)
        ENABLE_ACK_PAYLOAD = 4
        ENABLE_ACK_PAYLOAD_MASK = (1 << ENABLE_ACK_PAYLOAD)
        CRC_LENGTH = 5
        CRC_LENGTH_MASK = (0b11 << CRC_LENGTH)

    class AUTO_ACK(IntEnum):
        PIPE0 = 0
        PIPE0_MASK = (1 << PIPE0)
        PIPE1 = 1
        PIPE1_MASK = (1 << PIPE1)
        PIPE2 = 2
        PIPE2_MASK = (1 << PIPE2),
        PIPE3 = 3
        PIPE3_MASK = (1 << PIPE3)
        PIPE4 = 4
        PIPE4_MASK = (1 << PIPE4)
        PIPE5 = 5
        PIPE5_MASK = (1 << PIPE5)

    class OPENED_PIPES(IntEnum):
        PIPE0_AS_WRITING = 0
        PIPE0_AS_WRITING_MASK = (1 << PIPE0_AS_WRITING)
        PIPE0_AS_READING = 0
        PIPE0_AS_READING_MASK = (1 << PIPE0_AS_READING)
        PIPE1 = 2
        PIPE1_MASK = (1 << PIPE1)
        PIPE2 = 3
        PIPE2_MASK = (1 << PIPE2),
        PIPE3 = 4
        PIPE3_MASK = (1 << PIPE3)
        PIPE4 = 5
        PIPE4_MASK = (1 << PIPE4)
        PIPE5 = 6
        PIPE5_MASK = (1 << PIPE5)

    class RETRIES(IntEnum):
        RETRIES = 0
        RETRIES_MASK = 0x0F
        DELAY = 4
        DELAY_MASK = 0xF0

    class WRITE_AS(IntEnum):
        SEND = 0
        SEND_MASK = (1 << SEND)
        MULTICAST = 1
        MULTICAST_MASK = (1 << MULTICAST)
        FAST = 2
        FAST_MASK = (1 << FAST)
        BLOCKING = 3
        BLOCKING_MASK = (1 << BLOCKING)
        WRITE_ACK_PAYLOAD = 4
        WRITE_ACK_PAYLOAD_MASK = (1 << WRITE_ACK_PAYLOAD)
        FLUSH_TX = 5
        FLUSH_TX_MASK = (1 << FLUSH_TX)

    def __init__(self, port, baud=115200, timeout_ms=2):
        # Device memory table
        self.buffer = dict()
        self.__clear_buffer()

        # Try to open the uart port
        self.messenger = Messenger(port, baud=baud, timeout_ms=timeout_ms)

        # Possible commands
        self.CMD_READ = self.messenger.instruction_packet.Command.READ
        self.CMD_WRITE = self.messenger.instruction_packet.Command.WRITE

        time.sleep(1)
        # self.read_all()

    def __clear_buffer(self):
        self.buffer = dict()
        self.buffer[self.Register.PIPE0_DATA] = [0] * 32
        self.buffer[self.Register.PIPE1_DATA] = [0] * 32
        self.buffer[self.Register.PIPE2_DATA] = [0] * 32
        self.buffer[self.Register.PIPE3_DATA] = [0] * 32
        self.buffer[self.Register.PIPE4_DATA] = [0] * 32
        self.buffer[self.Register.PIPE5_DATA] = [0] * 32

    def __send_instruction(self):
        self.messenger.send(self.messenger.instruction_packet)

    def __receive_response(self):
        now_ms = time_ms()
        timeout_ms = self.messenger.timeout_ms * 1e15

        # Receive the response with timeout protection
        self.messenger.new_msg = False
        while timeout_ms > (time_ms() - now_ms) and self.messenger.new_msg is False:
            raw_byte = self.messenger.uart.read(1)
            self.messenger.isr_receveing_msg(raw_byte)

        if self.messenger.new_msg and self.messenger.response_packet.is_valid():
            self.__decode_response()
        else:
            # TODO: What to do with failed transmissions?
            return None

    def __decode_response(self):
        # If it is a response from WRITE command
        if self.messenger.instruction_packet.command == self.CMD_WRITE:
            pass

        # If it is a response from READ command
        elif self.messenger.instruction_packet.command == self.CMD_READ:
            first = self.messenger.instruction_packet.get_parameter(0)
            n_parameters = self.messenger.response_packet.get_number_of_parameters()
            read_registers = list(range(first, first + n_parameters))
            list_of_registers = list(self.Register)

            # Update buffer
            if len(read_registers) > 0:
                parameters = self.messenger.response_packet.get_parameters()
                for register in read_registers:
                    if register in list_of_registers:
                        start_position = register - first
                        if register == self.Register.STATUS:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.STATUS] = value[0]

                        elif register == self.Register.POWER_CONFIG:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.POWER_CONFIG] = value[0]

                        elif register == self.Register.CHANNEL:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.CHANNEL] = value[0]

                        elif register == self.Register.TRANSMISSION:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.TRANSMISSION] = value[0]

                        elif register == self.Register.PAYLOAD_SIZE:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PAYLOAD_SIZE] = value[0]

                        elif register == self.Register.AUTO_ACK:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.AUTO_ACK] = value[0]

                        elif register == self.Register.ADDRESS_WIDTH:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.ADDRESS_WIDTH] = value[0]

                        elif register == self.Register.PIPE0_ADDRESS:
                            end_position = start_position + 5
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE0_ADDRESS] = value[0]

                        elif register == self.Register.PIPE1_ADDRESS:
                            end_position = start_position + 5
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE1_ADDRESS] = value[0]

                        elif register == self.Register.PIPE2_ADDRESS:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE2_ADDRESS] = value[0]

                        elif register == self.Register.PIPE3_ADDRESS:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE3_ADDRESS] = value[0]

                        elif register == self.Register.PIPE4_ADDRESS:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE4_ADDRESS] = value[0]

                        elif register == self.Register.PIPE5_ADDRESS:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE5_ADDRESS] = value[0]

                        elif register == self.Register.OPENED_PIPES:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.OPENED_PIPES] = value[0]

                        elif register == self.Register.SPI_SPEED:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.SPI_SPEED] = value[0]

                        elif register == self.Register.WHO_AM_I:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.WHO_AM_I] = value[0]

                        elif register == self.Register.RETRIES:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.RETRIES] = value[0]

                        elif register == self.Register.WRITE_AS:
                            end_position = start_position + 1
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.WRITE_AS] = value[0]

                        elif register == self.Register.PIPE0_DATA:
                            end_position = start_position + 32
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE0_DATA] = value[0:32]

                        elif register == self.Register.PIPE1_DATA:
                            end_position = start_position + 32
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE1_DATA] = value[0:32]

                        elif register == self.Register.PIPE2_DATA:
                            end_position = start_position + 32
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE2_DATA] = value[0:32]

                        elif register == self.Register.PIPE3_DATA:
                            end_position = start_position + 32
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE3_DATA] = value[0:32]

                        elif register == self.Register.PIPE4_DATA:
                            end_position = start_position + 32
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE4_DATA] = value[0:32]

                        elif register == self.Register.PIPE5_DATA:
                            end_position = start_position + 32
                            value = parameters[start_position:end_position]
                            self.buffer[self.Register.PIPE5_DATA] = value[0:32]

        self.messenger.new_msg = False

    def __read_bytes(self, first_register: Register, how_many: int):
        if self.messenger.uart.in_waiting > 0:
            self.__receive_response()

        self.messenger.instruction_packet.create_new_message(self.CMD_READ)
        self.messenger.instruction_packet.append_parameter(first_register)
        self.messenger.instruction_packet.append_parameter(how_many)
        self.messenger.instruction_packet.update_checksum_value()
        self.__send_instruction()
        return self.__receive_response()

    def __read_byte(self, what_register: Register):
        self.__read_bytes(what_register, 1)

    def __read_word(self, what_register: Register):
        self.__read_bytes(what_register, 2)

    def __read_dword(self, what_register: Register):
        self.__read_bytes(what_register, 4)

    def __write_bytes(self, first_register: Register, values: list):
        self.messenger.instruction_packet.create_new_message(self.CMD_WRITE)
        self.messenger.instruction_packet.append_parameter(first_register)
        for value in values:
            self.messenger.instruction_packet.append_parameter(value)
        self.messenger.instruction_packet.update_checksum_value()
        self.__send_instruction()

    def __write_byte(self, what_register: Register, value: int):
        self.__write_bytes(what_register, [value])

    def __write_word(self, what_register: Register, value: int):
        self.__write_bytes(what_register, [value])

    def __write_dword(self, what_register: Register, value):
        self.__write_bytes(what_register, value)

    def is_chip_connected(self):
        self.__read_byte(self.Register.STATUS)
        is_connected = False
        if (self.buffer[self.Register.STATUS] & self.STATUS.IS_CHIP_CONNECTED_MASK) > 0:
            is_connected = True
        return is_connected

    def start_listening(self):
        reg = self.Register.POWER_CONFIG
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] | self.POWER_CONFIG.LISTENING_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def stop_listening(self):
        reg = self.Register.POWER_CONFIG
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] & ~self.POWER_CONFIG.LISTENING_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def read(self, pipe_number: int):
        reg = None
        if pipe_number == 0:
            reg = self.Register.PIPE0_DATA
        elif pipe_number == 1:
            reg = self.Register.PIPE1_DATA
        elif pipe_number == 2:
            reg = self.Register.PIPE2_DATA
        elif pipe_number == 3:
            reg = self.Register.PIPE3_DATA
        elif pipe_number == 4:
            reg = self.Register.PIPE4_DATA
        elif pipe_number == 5:
            reg = self.Register.PIPE5_DATA

        self.__read_bytes(reg, 32)

    def __load_write_msg(self, msg: list):
        if self.Register.PAYLOAD_SIZE not in self.buffer:
            self.__read_byte(self.Register.PAYLOAD_SIZE)
        payload_size = self.buffer[self.Register.PAYLOAD_SIZE]

        # First load data to PIPE0 data register
        msg_as_int_list = [ord(item) for item in list(msg[0:payload_size])]
        self.__write_bytes(self.Register.PIPE0_DATA, msg_as_int_list)
        self.__receive_response()

    def write(self, msg, multicast=False):
        self.__load_write_msg(msg)

        # Then ask radio to send a message
        writing_order = self.WRITE_AS.SEND_MASK
        if multicast is True:
            writing_order |= self.WRITE_AS.MULTICAST_MASK
        self.__write_byte(self.Register.WRITE_AS, writing_order)
        self.__receive_response()

    def write_fast(self, msg: list, multicast: bool):
        self.__load_write_msg(msg)

        # Then ask radio to send a message
        writing_order = self.WRITE_AS.SEND_MASK | self.WRITE_AS.FAST_MASK
        if multicast is True:
            writing_order |= self.WRITE_AS.MULTICAST_MASK
        self.__write_byte(self.Register.WRITE_AS, writing_order)
        self.__receive_response()

    def write_blocking(self, msg: list, multicast: bool):
        self.__load_write_msg(msg)

        # Then ask radio to send a message
        writing_order = self.WRITE_AS.SEND_MASK | self.WRITE_AS.BLOCKING_MASK
        if multicast is True:
            writing_order |= self.WRITE_AS.MULTICAST_MASK
        self.__write_byte(self.Register.WRITE_AS, writing_order)
        self.__receive_response()

    def write_ack_payload(self, pipe_number, msg: list):
        print('The function write_ack_payload is NOT IMPLEMENTED!')

    def open_writing_pipe(self, address):
        mask = self.OPENED_PIPES.PIPE0_AS_WRITING_MASK
        pipe_addr_reg = self.Register.PIPE0_ADDRESS

        if self.Register.ADDRESS_WIDTH not in self.buffer:
            self.__read_byte(self.Register.ADDRESS_WIDTH)
        addr_width = self.buffer[self.Register.ADDRESS_WIDTH]

        # First update the address of the pipe
        address_as_int_list = [ord(item) for item in list(address[0:addr_width])]
        self.__write_bytes(pipe_addr_reg, address_as_int_list)
        self.__receive_response()

        # Then open the pipe as reading
        reg = self.Register.OPENED_PIPES
        if reg not in self.buffer:
            self.__read_byte(reg)
        self.buffer[reg] = self.buffer[reg] | mask
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def open_reading_pipe(self, pipe_number, address):
        mask = None
        pipe_addr_reg = None
        if pipe_number == 0:
            mask = self.OPENED_PIPES.PIPE0_AS_READING_MASK
            pipe_addr_reg = self.Register.PIPE0_ADDRESS

        elif pipe_number == 1:
            mask = self.OPENED_PIPES.PIPE1_MASK
            pipe_addr_reg = self.Register.PIPE1_ADDRESS

        elif pipe_number == 2:
            mask = self.OPENED_PIPES.PIPE2_MASK
            pipe_addr_reg = self.Register.PIPE2_ADDRESS

        elif pipe_number == 3:
            mask = self.OPENED_PIPES.PIPE3_MASK
            pipe_addr_reg = self.Register.PIPE3_ADDRESS

        elif pipe_number == 4:
            mask = self.OPENED_PIPES.PIPE4_MASK
            pipe_addr_reg = self.Register.PIPE4_ADDRESS

        elif pipe_number == 5:
            mask = self.OPENED_PIPES.PIPE5_MASK
            pipe_addr_reg = self.Register.PIPE5_ADDRESS

        # Only update if a valid pipe was given
        if mask is not None:
            if self.Register.ADDRESS_WIDTH not in self.buffer:
                self.__read_byte(self.Register.ADDRESS_WIDTH)
            addr_width = self.buffer[self.Register.ADDRESS_WIDTH]

            # First update the address of the pipe
            if pipe_number <= 1:
                address_as_int_list = [ord(item) for item in list(address[0:addr_width])]
                self.__write_bytes(pipe_addr_reg, address_as_int_list)
                self.__receive_response()
            else:  # pipes 2, 3, 4 or 5
                self.__write_byte(pipe_addr_reg, ord(address[0]))
                self.__receive_response()

            # Then open the pipe as reading
            reg = self.Register.OPENED_PIPES
            if reg not in self.buffer:
                self.__read_byte(reg)
            self.buffer[reg] = self.buffer[reg] | mask
            self.__write_byte(reg, self.buffer[reg])
            self.__receive_response()

    def close_reading_pipe(self, pipe_number):
        mask = None
        if pipe_number == 0:
            mask = self.OPENED_PIPES.PIPE0_AS_READING_MASK

        elif pipe_number == 1:
            mask = self.OPENED_PIPES.PIPE1_MASK

        elif pipe_number == 2:
            mask = self.OPENED_PIPES.PIPE2_MASK

        elif pipe_number == 3:
            mask = self.OPENED_PIPES.PIPE3_MASK

        elif pipe_number == 4:
            mask = self.OPENED_PIPES.PIPE4_MASK

        elif pipe_number == 5:
            mask = self.OPENED_PIPES.PIPE5_MASK

        if mask is not None:
            # Then open the pipe as reading
            reg = self.Register.OPENED_PIPES
            if reg not in self.buffer:
                self.__read_byte(reg)
            self.buffer[reg] = self.buffer[reg] & ~mask
            self.__write_byte(reg, self.buffer[reg])
            self.__receive_response()

    def power_down(self):
        reg = self.Register.POWER_CONFIG
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] | self.POWER_CONFIG.LOW_POWER_MODE_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def power_up(self):
        reg = self.Register.POWER_CONFIG
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] & ~self.POWER_CONFIG.LOW_POWER_MODE_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def test_carrier(self):
        self.__read_byte(self.Register.STATUS)
        has_carrier = False
        if (self.buffer[self.Register.STATUS] & self.STATUS.CARRIER_ON_CURRENT_CHANNEL_MASK) > 0:
            has_carrier = True
        return has_carrier

    def test_rpd(self):
        self.__read_byte(self.Register.STATUS)
        is_noisy = False
        if (self.buffer[self.Register.STATUS] & self.STATUS.IS_CURRENT_CHANNEL_NOISY_MASK) > 0:
            is_noisy = True
        return is_noisy

    def is_valid(self):
        self.__read_byte(self.Register.STATUS)
        is_p = False
        if (self.buffer[self.Register.STATUS] & self.STATUS.IS_LEGITIMATE_RADIO_MASK) > 0:
            is_p = True
        return is_p

    def set_address_width(self, addr_width):
        reg = self.Register.ADDRESS_WIDTH
        if (addr_width >= 3) and (addr_width <= 5):
            self.buffer[reg] = addr_width
            self.__write_byte(reg, self.buffer[reg])
            self.__receive_response()

    def set_retries(self, delay, retries):
        reg = self.Register.RETRIES
        if reg not in self.buffer:
            self.__read_byte(reg)

        mask = self.RETRIES.RETRIES_MASK
        pos = self.RETRIES.RETRIES
        self.buffer[reg] = (retries >> pos) & mask

        mask = self.RETRIES.DELAY_MASK
        pos = self.RETRIES.DELAY
        self.buffer[reg] = (delay >> pos) & mask

        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def set_channel(self, new_channel):
        reg = self.Register.CHANNEL
        if (new_channel <= 125) and (new_channel >= 0):
            self.buffer[reg] = new_channel
            self.__write_byte(reg, self.buffer[reg])
            self.__receive_response()

    def set_payload_size(self, new_payload_size):
        reg = self.Register.PAYLOAD_SIZE
        if (new_payload_size <= 32) and (new_payload_size > 0):
            self.buffer[reg] = new_payload_size
            self.__write_byte(reg, self.buffer[reg])
            self.__receive_response()

    def enable_ack_payload(self):
        reg = self.Register.TRANSMISSION
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] | self.TRANSMISSION.ENABLE_ACK_PAYLOAD_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def enable_dynamic_payloads(self):
        reg = self.Register.TRANSMISSION
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] | self.TRANSMISSION.ENABLE_DYNAMIC_PAYLOADS_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def disable_dynamic_payloads(self):
        reg = self.Register.TRANSMISSION
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] & ~self.TRANSMISSION.ENABLE_DYNAMIC_PAYLOADS_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def enable_dynamic_ack(self):
        reg = self.Register.TRANSMISSION
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] | self.TRANSMISSION.ENABLE_DYNAMIC_ACK_MASK
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def is_p_variant(self):
        self.__read_byte(self.Register.STATUS)
        is_p = False
        if (self.buffer[self.Register.STATUS] & self.STATUS.IS_P_VARIANT_MASK) > 0:
            is_p = True
        return is_p

    def set_auto_ack(self, pipe_number: int, enable: bool):
        mask = None
        if pipe_number == 0:
            mask = self.AUTO_ACK.PIPE0_MASK
        elif pipe_number == 1:
            mask = self.AUTO_ACK.PIPE1_MASK
        elif pipe_number == 2:
            mask = self.AUTO_ACK.PIPE2_MASK
        elif pipe_number == 3:
            mask = self.AUTO_ACK.PIPE3_MASK
        elif pipe_number == 4:
            mask = self.AUTO_ACK.PIPE4_MASK
        elif pipe_number == 5:
            mask = self.AUTO_ACK.PIPE5_MASK

        reg = self.Register.AUTO_ACK
        if reg not in self.buffer:
            self.__read_byte(reg)

        if (enable is True) and (mask is not None):
            self.buffer[reg] = self.buffer[reg] | mask
        else:
            self.buffer[reg] = self.buffer[reg] & ~mask

        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def set_pa_level(self, new_pa_level):
        reg = self.Register.POWER_CONFIG
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] & ~self.POWER_CONFIG.PA_LEVEL_MASK
        self.buffer[reg] = self.buffer[reg] | (new_pa_level << self.POWER_CONFIG.PA_LEVEL)
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def set_data_rate(self, new_data_rate):
        reg = self.Register.TRANSMISSION
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] & ~self.TRANSMISSION.DATA_RATE_MASK
        self.buffer[reg] = self.buffer[reg] | (new_data_rate << self.TRANSMISSION.DATA_RATE)
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def set_crc(self, new_crc):
        reg = self.Register.TRANSMISSION
        if reg not in self.buffer:
            self.__read_byte(reg)

        self.buffer[reg] = self.buffer[reg] & ~self.TRANSMISSION.CRC_LENGTH_MASK
        self.buffer[reg] = self.buffer[reg] | (new_crc << self.TRANSMISSION.CRC_LENGTH)
        self.__write_byte(reg, self.buffer[reg])
        self.__receive_response()

    def who_am_i(self):
        self.__read_byte(self.Register.WHO_AM_I)
        return self.buffer[self.Register.WHO_AM_I]


# nrf = NrfUsb('/dev/ttyACM0')
# value = nrf.who_am_i()
# print(value)
# value = nrf.is_chip_connected()
# print(value)
# value = nrf.is_valid()
# print(value)
# value = nrf.is_p_variant()
# print(value)
# value = nrf.test_carrier()
# print(value)
# value = nrf.test_rpd()
# print(value)
# nrf.set_pa_level(PA_LEVEL.MAX)
# nrf.set_pa_level(PA_LEVEL.HIGH)
# nrf.power_down()
# nrf.power_up()
# nrf.start_listening()
# nrf.stop_listening()
# nrf.set_data_rate(DATA_RATE.RATE_2MBPS)
# nrf.set_crc(CRC.LENGTH_8)
# nrf.open_reading_pipe(1, '1ifsp')
# nrf.open_writing_pipe('0ifsp')
# nrf.close_reading_pipe(1)
# nrf.write('Hello, world!\n', True)
# nrf.read(0)

# nrf.messenger.uart.close()
