from PyQt5.QtWidgets import *
import sys
from math import *


class Calculadora(QDialog):
    def __init__(self, classe_pai=None):
        # A função super faz chamar o construtor da classe pai (se informado)
        super(Calculadora, self).__init__(classe_pai)

        # Cria os dois widgets dessa calculadora: uma linha para digitar texto
        # e um "browser" de texto para mostrar o que foi digitado e o resultado
        self.lista_resultados = QTextBrowser()
        self.linha_de_entrada = QLineEdit("Digite uma expressão matemática e aperte ENTER")

        # O alinhamento dos dois widgets criados será automático na vertical
        layout = QVBoxLayout()
        layout.addWidget(self.lista_resultados)
        layout.addWidget(self.linha_de_entrada)

        self.setLayout(layout)

        self.linha_de_entrada.selectAll()
        self.linha_de_entrada.setFocus()

        # O que essa linha faz?
        # Quando o usuário pressionar o "Enter" do teclado, um sinal é gerado
        # automaticamente pelo Qt. Se nós quisermos usar esse sinal, então é
        # necessário contar para o PyQt qual função deve ser chamada quando o
        # sinal acontecer. Em outras palavras, "Querido PyQt, por favor chame
        # a função 'computar' quando alguém apertar ENTER dentro da caixa de
        # texto 'linha_de_entrada'. "
        self.linha_de_entrada.returnPressed.connect(self.calcular)

    # Essa função é a calculadora. Ela pega o texto informado pelo usuário na
    # caixa 'linha_de_entrada' e tenta executá-lo. Se não der bom, uma excessão
    # será gerada e será mostrado "Código inválido", em letras vermelhas
    def calcular(self):
        try:
            text = self.linha_de_entrada.text()
            self.lista_resultados.append("{0} = <b>{1}</b>".format(text, eval(text)))

        except:
            self.lista_resultados.append("<font color=red><b>Código inválido</b></font>")


app = QApplication(sys.argv)
form = Calculadora()
form.show()
app.exec_()
