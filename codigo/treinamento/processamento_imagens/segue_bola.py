import cv2
import time
import imutils

# Documentação
# https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/

# O número que for passado para o construtor abaixo, é o número da webcam
webcam = cv2.VideoCapture(2)

# Configura a resolução para 640x480
webcam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
webcam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

# Cores
laranjaMin = (5, 179, 193)
laranjaMax = (48, 255, 255)

# allow the camera or video file to warm up
time.sleep(2.0)

while True:
    deuBom, imagemCapturada = webcam.read()

    imagemCapturada = imutils.resize(imagemCapturada, width=600)

    blurred = cv2.GaussianBlur(imagemCapturada, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, laranjaMin, laranjaMax)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    center = None

    if len(cnts) > 0:
        # find the largest contour in the mask, then use
        # it to compute the minimum enclosing circle and
        # centroid
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

        if radius > 10:
            # draw the circle and centroid on the frame
            cv2.circle(imagemCapturada, (int(x), int(y)), int(radius),  (0, 255, 255), 2)
            cv2.circle(imagemCapturada, center, 5, (0, 0, 255), -1)

    cv2.imshow("Frame", mask)

    # Se pressionou a letra 'q' então encerra o programa
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

webcam.release()
cv2.destroyAllWindows()

