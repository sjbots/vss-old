import cv2
import time

# O número que for passado para o construtor abaixo, é o número da webcam
webcam = cv2.VideoCapture(2)

# Configura a resolução para 640x480
webcam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
webcam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

print('FPS:', webcam.get(cv2.CAP_PROP_FPS))

while True:
    # Captura a imagem
    tempoInicio = time.time()
    deuBom, imagemCapturada = webcam.read()
    tempoFim = time.time()

    if deuBom is True:
        print('Tempo de captura: %.2f ms' % (1000 * (tempoFim - tempoInicio)))

        # Mostra a imagem
        cv2.imshow('frame', imagemCapturada)
    else:
        print('Deu ruim!')

    # Se pressionou a letra 'q' então encerra o programa
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


webcam.release()
cv2.destroyAllWindows()
