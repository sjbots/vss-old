from PyQt5.QtWidgets import *
import sys
from math import *
import urllib.request
from bs4 import BeautifulSoup


class ConversorMoedas(QDialog):
    def __init__(self, parent=None):
        super(ConversorMoedas, self).__init__(parent)

        # Carrega a página da corretora
        self.pagina = ''
        self.carregar_pagina_corretora()

        # Extrai a data de hoje
        self.data_hoje = self.pegar_data_hoje()

        # Extrai as cotações das moedas da página da corretora
        self.cotacoes = []
        self.pegar_cotacoes()

        # Cria os widgets do conversor
        self.widget_data = QLabel('Cotado no dia ' + self.data_hoje)
        self.widget_moeda_entrada = QComboBox()
        self.widget_moeda_saida = QComboBox()

        moedas = self.lista_moedas()
        self.widget_moeda_entrada.addItems(moedas)
        self.widget_moeda_saida.addItems(moedas)

        self.widget_valor_para_converter = QDoubleSpinBox()
        self.widget_valor_para_converter.setRange(0.01, 1e6)
        self.widget_valor_para_converter.setValue(1.00)

        self.widget_valor_convertido = QLabel("1.00")

        layout = QGridLayout()
        layout.addWidget(self.widget_data, 0, 0)
        layout.addWidget(self.widget_moeda_entrada, 1, 0)
        layout.addWidget(self.widget_moeda_saida, 2, 0)
        layout.addWidget(self.widget_valor_para_converter, 1, 1)
        layout.addWidget(self.widget_valor_convertido, 2, 1)
        self.setLayout(layout)

        # Sinais (signals)
        self.widget_moeda_entrada.currentIndexChanged.connect(self.atualizar_interface)
        self.widget_moeda_saida.currentIndexChanged.connect(self.atualizar_interface)
        self.widget_valor_para_converter.valueChanged.connect(self.atualizar_interface)

    def carregar_pagina_corretora(self):
        # Abre a página da corretora
        with urllib.request.urlopen("https://www.xe.com/currencytables/?from=USD") as objeto:
            pagina_corretora = objeto.read()

        # Transforma a página da corretora num objeto BeautifulSoup
        self.pagina = BeautifulSoup(pagina_corretora)

    def pegar_cotacoes(self):
        lista_moedas = self.pagina.find('div', {'class': 'historicalRateTable-wrap'})
        linhas_tabela = lista_moedas.table.tbody.find_all('tr')

        lista_moedas = []
        for linha in linhas_tabela:
            aux = linha.find_all('td')

            # Organização dos dados
            # [0] contém o acrônimo da moeda (exemplo: EU)
            # [1] contém o nome da moeda (exemplo Euro)
            # [2] contém o valor da moeda em dolares
            # [3] contém o valor de quantos USD são necessários para comprar uma unidade dessa moeda
            moeda = aux[1].get_text()
            valor_em_dolares = aux[2].get_text()
            lista_moedas.append([moeda, float(valor_em_dolares)])

        self.cotacoes = lista_moedas

    def pegar_data_hoje(self):
        lugar = self.pagina.find('p', {'class': 'historicalRateTable-date'})
        data_bruto = lugar.get_text().split()[0]
        data = data_bruto.split('-')
        data.reverse()
        return '/'.join(data)

    def lista_moedas(self):
        return [linha[0] for linha in self.cotacoes]

    def lista_valores_em_dolares(self):
        return [linha[1] for linha in self.cotacoes]

    def calcular_valores(self, da_moeda, para_moeda, valor):
        k = self.cotacoes[para_moeda][1] / self.cotacoes[da_moeda][1]
        return k * valor

    def atualizar_interface(self):
        da_moeda = self.widget_moeda_entrada.currentIndex()
        para_moeda = self.widget_moeda_saida.currentIndex()
        valor = self.widget_valor_para_converter.value()

        valor_convertido = self.calcular_valores(da_moeda, para_moeda, valor)
        self.widget_valor_convertido.setText("%0.2f" % valor_convertido)


app = QApplication(sys.argv)
conversor = ConversorMoedas()
conversor.show()
app.exec_()
