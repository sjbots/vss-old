#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from numpy.polynomial import Polynomial
from scipy.integrate import solve_ivp


class EspacoEstados:
    """"
    Essa classe implementa modelos de sistemas dinâmicos que estejam na forma
    :eq:`eq-EspacoEstados-dx` - :eq:`eq-EspacoEstados-y`.

    Os parâmetros de entrada são:

    :param A: matriz de estados
    :param B: matriz de controle (valor padrão é None)
    :param C: matriz de saída (valor padrão é None)
    :param D: matriz de transmissão direta (valor padrão é None)
    :param x0: estado atual do sistema (list or numpy.matrix)

    Essa classe utiliza o tipo numpy.matrix como objeto padrão para
    armazenar as variáveis matriciais.

    TODO:
    - adicionar suporte para transmissão direta
    """

    def __init__(self, A, B=None, C=None, D=None, x0=None):
        # Número de variáveis de estado
        self.nx = np.shape(A)[0]

        # Número de canais de controle
        self.nu = np.shape(B)[1] if B is not None else 0

        # Número de saídas medidas
        self.ny = np.shape(C)[0] if C is not None else 0

        # Check de sanidade
        problema = self.verificar_dimensoes_matrizes(A, B, C, D)

        # Armazena as matrizes do modelo
        if problema is False:
            self.A = np.matrix(A)
            self.B = np.matrix(B) if B is not None else None
            self.C = np.matrix(C) if C is not None else None

            if D is not None:
                self.D = np.matrix(D)
            else:
                self.D = np.matrix(np.zeros((self.ny, self.nu)))

        # Armazena o vetor de estados e de saída
        problema = self.verificar_dimensoes_vetor_estados(x0)
        if problema is False:
            if x0 is None:
                self.x = np.matrix(np.zeros((self.nx, 1)))
                self.y = np.matrix(np.zeros((self.ny, self.nx)))
            else:
                self.x = np.matrix(x0)
                self.y = self.C * self.x

    def verificar_dimensoes_matrizes(self, A, B, C, D, gerar_erro=False):
        """
        Verifica se as dimensões das matrizes A, B, C e D estão corretas.

        Essa função irá gerar um erro caso as dimensões não estejam de acordo

        :param A: é a matriz de estados, que deveria ter dimensões nx por nx
        :param B: é a matriz de controle, que deveria ter dimensões nx por nu
        :param C: é a matriz de saída, que deveria ter dimensões ny por nx
        :param D: é a matriz de transmissão direta, que deveria ter dimensões ny por nu
        :param gerar_erro: gerar um erro com o raise? Esse parâmetro não é obrigatório e tem como valor padrão False

        :return problema: False se as dimensões estão de acordo e True se as dimensões não estão de acordo
        """

        problema = False
        if np.shape(A)[0] != np.shape(A)[1]:
            problema = True
            if gerar_erro is True:
                msg = "A matriz A deve ser quadrada"
                raise NameError(msg)

        if self.nx != np.shape(B)[0]:
            problema = True
            if gerar_erro is True:
                msg = "A matriz B deve ter %d coluna(s)" % self.nx
                raise NameError(msg)

        if self.nx != np.shape(C)[1]:
            problema = True
            if gerar_erro is True:
                msg = "A matriz C deve ter %d linha(s)" % self.nx
                raise NameError(msg)

        # Haverá transmissão direta apenas se nu > 0 e ny > 0
        if self.nu > 0 and self.ny > 0 and D is not None:
            if self.ny != np.shape(D)[0] or self.nu != np.shape(D)[1]:
                problema = True
                if gerar_erro is True:
                    msg = "A matriz D deve ter %d linha(s) e %d coluna(s)" % (self.ny, self.nu)
                    raise NameError(msg)

        return problema

    def verificar_dimensoes_vetor_estados(self, x, gerar_erro=False):
        """
        Verifica se a dimensão do vetor de estados x está correta

        :param x: é o vetor de estados, que deveria ter dimensões nx por 1
        :param gerar_erro: gerar um erro com o raise? Esse parâmetro não é obrigatório e tem como valor padrão False

        :return True: se as dimensões estão de acordo
        """

        problema = False
        if self.nx != np.shape(x)[0]:
            problema = True
            if gerar_erro is True:
                msg = "O vetor de estados deveria ter %d linhas e 1 coluna" % self.nx
                raise NameError(msg)

        return problema

    def saida(self, u=0):
        """
        Calcula a saída do sistema

        :param u: ação de controle no instante atual (apenas se houver transmissão direta)

        :return y: o vetor de saída
        """

        self.y = self.C * self.x + self.D * u

        return self.y

    def set_x(self, x):
        """
        Atualiza o vetor de estados x

        :param x: novo vetor de estados
        """

        problema = self.verificar_dimensoes_vetor_estados(x)
        if problema is False:
            self.x = x

    def auto_valores(self, K=None):
        """
        Se a matriz de ganhos K não for fornecida, calcula os auto-valores da matriz A, isto é, do sistema em malha
        aberta. Caso contrário, calcula os autovalores do sistema em malha fechada, isto é, A - B*F.

        :param K: matriz de ganhos K (opcional). Deve ter dimensões :math:`\dot{x}(t)` :math:`n_u \times n_x`

        :return: um vetor coluna com os auto-valores da matriz A
        """

        if K is None:
            return np.matrix(np.linalg.eig(self.A)[0]).T
        else:
            return np.matrix(np.linalg.eig(self.A - self.B*K)[0]).T

    def criar_vetor_unidimensional(self, vetor):
        """
        Retorna uma cópia criar_vetor_unidimensional do vetor informado

        :param vetor: vetor do tipo np.array ou np.matrix
        :return: vetor criar_vetor_unidimensional
        """

        return vetor.flatten().tolist()[0]


class ss(EspacoEstados):
    """"
    Essa classe é uma implementação do modelo a tempo contínuo :eq:`eq-EspacoEstados-dx` - :eq:`eq-EspacoEstados-y`.

    O nome dessa classe foi inspirado no software Matlab, que utiliza ss como State Space.

    Os parâmetros de entrada são:

    :param A: matriz de estados
    :param B: matriz de controle
    :param C: matriz de saída
    :param D: matriz de transmissão direta
    :param x0: estado atual do sistema (list or numpy.matrix)

    Essa classe utiliza o tipo numpy.matrix como objeto padrão para armazenar as variáveis matriciais.

    .. todo::
       criar uma função para informar se o modelo tem polos instáveis
    """

    def __init__(self, A, B=None, C=None, D=None, x0=None):
        super().__init__(A, B, C, D, x0=x0)

    def instavel(self):
        """
        Esse método informa se a matriz A possui auto-valores positivos, isto é, modos instáveis

        :return: True se há auto-valores positivos e False se todos os auto-valores são zero ou negativos
        """

        return any(np.real(self.auto_valores()) > 0)

    def estavel(self):
        """
        Esse método informa se a matriz A possui auto-valores positivos, isto é, modos instáveis

        :return: False se há auto-valores positivos e True se todos os auto-valores são zero ou negativos
        """

        return not self.instavel()

    def dx(self, x, u):
        """
        Calcula a derivada do estado :math:`\dot{x}(t)`. Considera-se que :math:`u(t)` é constante e com dimensões
        apropriadas. Para calcular a derivada do estado em malha aberta, pode-se passar :math:`u(t) = 0`.

        Por questões de velocidade, essa função não testa as dimensões do :math:`u(t)` informado.

        :param x: é o vetor de estados
        :param u: é o vetor de controle
        :return: a derivada do estado :math:`\dot{x}(t)`
        """

        return self.A*np.matrix(x) + self.B*np.matrix(u)

    def propagar_estado(self, tf, u):
        """
        Propaga, numericamente, o estado da planta a partir do instante de tempo atual até tf segundos, supondo que
        :math:`u(t)` seja constante e igual ao parâmetro u informado.

        .. note::
            É assumido que o maior passo dado na integração será 10x menor que o tf especificado.

        :param tf: tempo final da integração numérica
        :param u: vetor de controle constante durante a integração
        :return: a solução obtida do integrador numérico (contém todas as informações relativas à integração)
        """

        fun = lambda t, y: self.dx(y, u)
        sol = solve_ivp(fun, (0, tf), self.criar_vetor_unidimensional(self.x), max_step=tf/10, vectorized=True)

        # Pega o último ponto obtido e configura a dimensão dele para ser uma coluna com nx linhas
        self.x = np.matrix(sol.y[:, -1]).reshape(self.nx, 1)

        return sol

class dss(EspacoEstados):
    """
    Essa classe é uma implementação do modelo a tempo discreto :eq:`eq-EspacoEstados-xk1` - :eq:`eq-EspacoEstados-yk`.

    O nome dessa classe foi inspirado no software Matlab, que utiliza ss como State Space.

    Os parâmetros de entrada são:

    :param Ts: período de amostragem (em segundos)
    :param A: matriz de estados
    :param B: matriz de controle
    :param C: matriz de saída
    :param D: matriz de transmissão direta
    :param x0: estado atual do sistema (list or numpy.matrix)

    Essa classe utiliza o tipo numpy.matrix como objeto padrão para armazenar as variáveis matriciais.
    """

    def __init__(self, Ts, A, B=None, C=None, D=None, x0=None):
        super().__init__(A, B, C, D, x0=x0)
        self.Ts = Ts
        self.nome = "dss"

    def __name__(self):
        return self.nome

    def instavel(self):
        """
        Esse método informa se a matriz A possui auto-valores fora do círculo unitário, isto é, modos instáveis.

        :return: True se há algum auto-valor com módulo maior que a unidade. False caso contrário.
        """

        return any(np.abs(self.auto_valores()) > 1)

    def estavel(self):
        """
        Esse método informa se todos os auto-valores da matriz A estão dentro do círculo unitário.

        :return: True se todos os auto-valores estão dentro do círculo unitário. False caso contrário.
        """

        return not self.instavel()

    def propagar_estado(self, u):
        """
        Aplica a ação de controle :math:`u(k)` ao sistema, atualiza o estado interno e retorna x(k+1)

        :param u: vetor de controle a ser aplicado ao sistema
        :return: estado do sistema no instante k+1
        """

        self.x = self.A*self.x + self.B*np.matrix(u)

        return self.x

class tf:
    """
    Essa class é uma implementação do modelo no formato de função de transferência :eq:`eq-FuncaoDeTransferencia-Gs`.

    Por enquanto há suporte para sistemas SISO - *Single Input, Single Output*.

    .. todo ::
        - Adicionar suporte para sistemas MIMO
        - Adicionar funções de propagação do estado
        - Adicionar funções para avaliar estabilidade

    """

    def __init__(self, num, den, T=None):
        """
        Construtor da classe

        :param num: numerador da função de transferência
        :param den: denominador da função de transferência
        :param T (opcional): período de amostragem (em segundos)
        """
        self.num = Polynomial(num)
        self.den = Polynomial(den)
        self.T = T

    def polos(self):
        return np.roots(self.den)

    def tempo_continuo(self):
        """
        O sistema criado é a tempo contínuo?

        :return: True se o sistema é a tempo contínuo. Senão, False.
        """
        return True if self.T is None else False

    def instavel(self):
        """
        Esse método verifica se o modelo apresenta algum polo instável.

        :return: True se é instável. False caso contrário.
        """

        if self.tempo_continuo():
            return any(np.real(self.polos()) > 0)
        else:
            return any(np.abs(self.polos()) > 1)

    def estavel(self):
        """
        Esse método verifica se o modelo apresenta algum polo estável.

        :return: True se é estável. False caso contrário.
        """

        return not self.instavel()


def series(sys1, sys2):
    num = sys1.num + sys2.num
    den = sys1.den + sys2.den

    return tf(num, den)

def testar_tf():
    print("Teste da classe tf\n")

    num = [1, 3]
    den = [1, 2, -3]

    sys = tf(num, den)

    return sys

if __name__ == '__main__':
    # # A = [[1, 2], [3, 4]]
    # A = [[0, 1], [-1, -2]]
    # # A = [[0, 0], [0, 0]]
    # B = [[2], [1]]
    # C = [[1, 0]]
    #
    # # Estado inicial
    # x0 = [[1], [2]]
    #
    # # Instancia o modelo no espaço de estados a tempo contínuo
    # # m = EspacoEstados(A, B, C, x0)
    # m = ss(A, B, C, None, x0=x0)
    # msg = "É estável" if m.estavel() is True else "Não é estável"
    # print(msg)
    #
    # sol = m.propagar_estado(1, 0)

    sys = testar_tf()




