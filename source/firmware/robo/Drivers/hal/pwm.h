#ifndef HAL_PWM_H_
#define HAL_PWM_H_

#include <stdint.h>
#include <stdbool.h>
#include "stm32f3xx.h"
#include "stm32f3xx_hal_tim.h"

#ifdef __cplusplus
extern "C" {
#endif

void set_duty(TIM_HandleTypeDef* timer, uint32_t channel, float duty);
void set_pwm_frequency(TIM_HandleTypeDef* timer, float new_freq);

#ifdef __cplusplus
}
#endif

#endif /* HAL_PWM_H_ */
