#include "pid.h"

PID::PID()
{
}

PID::~PID()
{
}

void PID::begin(float Kp, float Ki, float Kd, float Kf, float umin, float umax, float dumax, float Ts)
{
    // Copy the PID configuration
    this->Kp = Kp;
    this->Ki = Ki;
    this->Kd = Kd;
    this->Kf = Kf;
    this->umin = umin;
    this->umax = umax;
    this->dumax = dumax;

    // Initialize variables with zeros
    u = 0;
    ui = 0;
    e0 = 0;
    e1 = 0;
    up = 0;
    ud = 0;
    uold = 0;
}

void PID::house_keeping()
{
    e1 = e0;
    uold = u;
}

void PID::limit_u()
{
    if (u > umax)
        u = umax;

    if (u < umin)
        u = umin;

    if (ui > umax)
        ui = umax;

    if (ui < umin)
        ui = umin;
}

void PID::limit_du(){
    float rate = u - uold;
    if (abs(rate) > dumax)
    {
        if (rate > 0)
            uold = uold + dumax;
        else
            uold = uold - dumax;
    }
}

float PID::update(float y, float r)
{
    house_keeping();
    e0 = r - y;

    // Proportional gain
    up = Kp * e0 + Kf * y;

    // Integral gain
    ui = ui + Ki * Ts * (e0 + e1)/2.0f;

    // Derivative gain
    ud = Kd * (e0 - e1)/Ts;

    // Calculate control
    u = up + ui + ud;

    // Limit rate of u, umin and umax
    limit_du();
    limit_u();

    return u;
}

float PID::get_u()
{
    return u;
}


