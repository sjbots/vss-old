#ifndef SIGNAL_PID_H_
#define SIGNAL_PID_H_

#include <math.h>

/**
 * This class implement a PID controller
 *
 * Equation:
 *
 * u(k) = Kp * e(k) + Ki*(\int e(k)) + Kd * de(k)/dt
 */

class PID {
private:
    // Internal variables
    float u, ui, up, ud, uold;
    float e0, e1;
    float Kp, Ki, Kd, Kf;
    float umax, umin, dumax, Ts;

    // Internal methods
    void house_keeping();
    void limit_u();
    void limit_du();

public:
    PID();
    ~PID();
    void begin (float Kp, float Ki, float Kd, float Kf, float umin, float umax, float dumax,float Ts);
    float update(float y, float r);
    float get_u();
};


#endif /* SIGNAL_PID_H_ */
