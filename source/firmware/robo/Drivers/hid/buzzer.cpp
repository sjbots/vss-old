#include "buzzer.h"

namespace hid {
namespace buzzer {

void set_frequency(float new_frequency) {
    set_pwm_frequency(&(BUZZER_PWM_TIMER), new_frequency);
}

void mute()
{
    set_duty(&(BUZZER_PWM_TIMER), BUZZER_PWM_CHANNEL, 0);
}

void beep()
{
    set_duty(&(BUZZER_PWM_TIMER), BUZZER_PWM_CHANNEL, 0.5);
}

}
}



