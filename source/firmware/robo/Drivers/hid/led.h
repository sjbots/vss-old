#ifndef HID_LED_H_
#define HID_LED_H_

#include "main.h"

namespace hid {
namespace led {

extern void enable();

extern void disable();

}
}



#endif /* HID_LED_H_ */
