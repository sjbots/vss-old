#ifndef HID_BUZZER_H_
#define HID_BUZZER_H_

#include "Config/board.h"
#include "Drivers/hal/pwm.h"

namespace hid {
namespace buzzer {

extern void set_frequency(float new_frequency);

extern void mute();

extern void beep();

}
}


#endif /* HID_BUZZER_H_ */
