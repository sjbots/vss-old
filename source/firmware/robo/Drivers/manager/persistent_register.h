#ifndef MANAGER_PERSISTENT_REGISTER_H_
#define MANAGER_PERSISTENT_REGISTER_H_

#include <stdint.h>
#include "registers_defs.h"
#include "../../Drivers/eeprom_memory/m24.h"

namespace regs {

template <typename T>
class PersistentRegister {
protected:
	const uint16_t addr_on_table, addr_on_chip;
	const T initial_value;
	const uint8_t access;
	M24* eeprom;
	T value;

public:
	PersistentRegister(uint16_t _addr_on_table, uint16_t _addr_on_chip, T _initial_value, bool writable, M24* handler) :
		addr_on_table(_addr_on_table),
		addr_on_chip(_addr_on_chip),
		initial_value(_initial_value),
		access(writable)
	{
	    eeprom = handler;
	}

    T read()
    {
        eeprom->read(addr_on_chip, sizeof(T), (uint8_t*) &value);
        return value;
    }

	T read_buffer()
	{
		return value;
	}

	void write(const T new_value)
	{
        eeprom->write(addr_on_chip, sizeof(T), (uint8_t*) &new_value);
        value = new_value;
	}

	void buffer_write(const T new_value)
	{
	    value = new_value;
	}

	void set_bit(const T what_bit)
	{
		write(value | (1 << what_bit));
	}

	void clear_bit(const T what_bit)
	{
		write(value & ~(1 << what_bit));
	}

	uint16_t get_addr() {
	    return addr_on_table;
	}

	void reset() {
	    write(initial_value);
	}

    void operator=(const T new_value)
    {
        write(new_value);
    }

    T operator()()
    {
        return read_buffer();
    }

    void operator|=(T new_mask)
    {
        write(value | new_mask);
    }

    void operator&=(T new_mask)
    {
        write(value & new_mask);
    }

    T begin()
    {
        read();
        return value;
    }
};

} // namespace regs



#endif /* MANAGER_PERSISTENT_REGISTER_H_ */
