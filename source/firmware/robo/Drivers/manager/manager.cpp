#include "manager.h"

using namespace regs;

Manager::Manager()
{
    new_ad_data = false;
    new_radio_message = false;
}

Manager::~Manager()
{
}

void Manager::begin_adc(u32* raw_values)
{
    adc_raw_values = raw_values;
    HAL_ADC_Start_DMA(&hadc1, adc_raw_values, NUMBER_OF_ADS);
}

bool Manager::is_buzzer_enabled()
{
    bool is_enabled = false;
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_BUZZER_MASK)) > 0)
        is_enabled = true;

    return is_enabled;
}

void Manager::buzzer_begin()
{
    HAL_TIM_Base_Start(&(BUZZER_PWM_TIMER));
    HAL_TIM_PWM_Start(&(BUZZER_PWM_TIMER), BUZZER_PWM_CHANNEL);
    set_buzzer_frequency((float) BUZZER_FREQUENCY());
}

void Manager::enable_buzzer()
{
    hid::buzzer::mute();
    POWER_CONFIG.set_bit(static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_BUZZER));
}

void Manager::set_buzzer_frequency(uint16_t new_frequency)
{
    hid::buzzer::set_frequency(new_frequency);
    BUZZER_FREQUENCY = new_frequency;
}

void Manager::buzzer_on()
{
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_BUZZER_MASK)) > 0)
    {
        hid::buzzer::beep();
        HID_ON |= static_cast<uint8_t>(positions::HID_ON::BUZZER_MASK);
    }

    else
    {
        buzzer_off();
    }
}

void Manager::buzzer_off()
{
    hid::buzzer::mute();
    HID_ON &= ~static_cast<uint8_t>(positions::HID_ON::BUZZER_MASK);
}

void Manager::has_to_beep()
{
    bool need_to_beep = false;

    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_BAT_CUTOFF_MASK)))
        if (BATTERY_VOLTAGE_MV() < BATTERY_CUTOFF_LEVEL_MV())
            need_to_beep = true;

    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_LOW_BAT_MASK)))
        if (BATTERY_VOLTAGE_MV() < BATTERY_MIN_LEVEL_MV())
            need_to_beep = true;

    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_BAT_OVERVOLTAGE_MASK)))
        if (BATTERY_VOLTAGE_MV() > BATTERY_MAX_LEVEL_MV())
            need_to_beep = true;

//    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_MOTOR_FAILURE_MASK)))
//        need_to_beep = true;

    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_HIGH_TEMP_MASK)))
        if (TEMPERATURE() > TEMPERATURE_MAX_LEVEL())
            need_to_beep = true;

//    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_CONNECTION_MASK)))
//            need_to_beep = true;

//    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_CONNECTION_LOSS_MASK)))
//            need_to_beep = true;

    if (need_to_beep == true)
        buzzer_on();
    else
        buzzer_off();
}

void Manager::disable_buzzer()
{
    hid::buzzer::mute();
    POWER_CONFIG &= ~static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_BUZZER_MASK);
    HID_ON &= ~static_cast<uint8_t>(positions::HID_ON::BUZZER_MASK);
}

bool Manager::is_motors_enabled()
{
    bool is_enabled = false;
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_MOTORS_MASK)) > 0)
        is_enabled = true;

    return is_enabled;
}

bool is_motors_in_fault()
{
    return hbridge.fault_has_occurred();
}

void Manager::begin_motors()
{
    // First set all H-bridge pins
    hbridge.set_en_fault_pin(MOTOR_EN_Pin, MOTOR_EN_GPIO_Port);
    hbridge.set_stdby_pin(MOTOR_STBY_Pin, MOTOR_STBY_GPIO_Port);
    hbridge.set_pha_pin(MOTOR_R_PH_Pin, MOTOR_R_PH_GPIO_Port);
    hbridge.set_phb_pin(MOTOR_L_PH_Pin, MOTOR_L_PH_GPIO_Port);

    // Then set the motors phase
    hbridge.set_phase_cha(R_MOTOR_POS_DUTY_IS_FORWARD());
    hbridge.set_phase_chb(L_MOTOR_POS_DUTY_IS_FORWARD());

    // Then set timers handler
    hbridge.set_timer_cha(&MOTOR_PWM_TIMER, MOTOR_R_PWM_CHANNEL);
    hbridge.set_timer_chb(&MOTOR_PWM_TIMER, MOTOR_L_PWM_CHANNEL);

    // Set PWM frequency
    set_pwm_frequency(&MOTOR_PWM_TIMER, MOTORS_PWM_FREQ());

    // Set initial duty cycle
    hbridge.set_duty_chA(0);
    hbridge.set_duty_chB(0);

    // Finally initialize the H-bridge
    hbridge.begin();
    enable_motors();
}

void Manager::enable_motors()
{
    hbridge.enable();
    POWER_CONFIG |= static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_MOTORS_MASK);
}

void Manager::disable_motors()
{
    hbridge.disable();
    POWER_CONFIG &= ~static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_MOTORS_MASK);
}

void set_duty_motor_L(float duty)
{
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_MOTORS_MASK)) > 0)
        hbridge.set_duty_chB(duty / 100.0f);
    else
        hbridge.set_duty_chB(0.0f);
}

void set_duty_motor_R(float duty)
{
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_MOTORS_MASK)) > 0)
        hbridge.set_duty_chA(duty / 100.0f);
    else
        hbridge.set_duty_chA(0.0f);
}

void Manager::update_motor_status()
{
    if (hbridge.fault_has_occurred() == true)
    {
        // Set bit showing that the H-bridge overloaded
        STATUS |= static_cast<uint8_t>(positions::STATUS::H_BRIDGE_OVERLOAD_MASK);

        // Not lets try to trace the cause of the overload
        if (L_MOTOR_CURRENT_MA() >= L_MOTOR_MAX_CURRENT_MA())
            MOTOR_STATUS |= static_cast<uint8_t>(positions::MOTOR_STATUS::L_MOTOR_SHORTED_MASK);
        else
            MOTOR_STATUS &= ~static_cast<uint8_t>(positions::MOTOR_STATUS::L_MOTOR_SHORTED_MASK);

        if (R_MOTOR_CURRENT_MA() >= R_MOTOR_MAX_CURRENT_MA())
            MOTOR_STATUS |= static_cast<uint8_t>(positions::MOTOR_STATUS::R_MOTOR_SHORTED_MASK);
        else
            MOTOR_STATUS &= ~static_cast<uint8_t>(positions::MOTOR_STATUS::R_MOTOR_SHORTED_MASK);
    }

    else
    {
        // TODO
        // If the motors are active but no current is drained,
        // then must set that the motors are disconnected
    }
}

void Manager::begin_pid()
{
    float Kp, Ki, Kd, Kf;
    float umin, umax, dumax, Ts;

    // Left motor
    Kp = L_MOTOR_P_GAIN();
    Ki = L_MOTOR_I_GAIN();
    Kd = L_MOTOR_D_GAIN();
    Kf = L_MOTOR_FEEDFORWARD_GAIN();
    umin = L_MOTOR_U_MIN();
    umax = L_MOTOR_U_MAX();
    dumax = L_MOTOR_U_MAX_RATE();
    Ts = static_cast<float>(1000 * SAMPLE_TIME_MS());
    pid_left_motor.begin(Kp, Ki, Kd, Kf, umin, umax, dumax, Ts);

    // Right motor
    Kp = R_MOTOR_P_GAIN();
    Ki = R_MOTOR_I_GAIN();
    Kd = R_MOTOR_D_GAIN();
    Kf = R_MOTOR_FEEDFORWARD_GAIN();
    umin = R_MOTOR_U_MIN();
    umax = R_MOTOR_U_MAX();
    dumax = R_MOTOR_U_MAX_RATE();
    pid_right_motor.begin(Kp, Ki, Kd, Kf, umin, umax, dumax, Ts);
}

void Manager::update_control_law()
{
    float y, r;
    float u_left_volts, u_right_volts;
    float u_left_duty, u_right_duty;

    // Update PID of left motor
    y = L_MOTOR_SPEED_RPM();
    r = L_MOTOR_SPEED_REF_RPM();
    u_left_volts = pid_left_motor.update(y, r);

    // Update PID of right motor
    y = R_MOTOR_SPEED_RPM();
    r = R_MOTOR_SPEED_REF_RPM();
    u_right_volts = pid_right_motor.update(y, r);

    // Convert the control action from Volts to duty cycle
    float battery_voltage = static_cast<float>(BATTERY_VOLTAGE_MV()) / 1000.0f;
    u_left_duty = 100 * (u_left_volts / battery_voltage);
    u_right_duty = 100 * (u_right_volts / battery_voltage);

    // Finally apply the duty cycle to the motors
    set_duty_motor_L(u_left_duty);
    set_duty_motor_R(u_right_duty);
}

bool Manager::is_radio_enabled()
{
    bool is_enabled = false;
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_RADIO_MASK)) > 0)
        is_enabled = true;

    return is_enabled;
}

void Manager::begin_radio()
{
    radio.begin();

    if (radio.isChipConnected() == true)
    {
        radio.setPayloadSize(NRF_PAYLOAD_SIZE());
        radio.setChannel(NRF_CHANNEL_CONFIG());

        const uint8_t data_rate_mask = static_cast<uint8_t>(positions::NRF_CONFIG::DATA_RATE_MASK);
        const uint8_t pos_data_rate = static_cast<uint8_t>(positions::NRF_CONFIG::DATA_RATE);
        rf24_datarate_e data_rate = (rf24_datarate_e) ((NRF_CONFIG() & data_rate_mask) >> pos_data_rate);
        radio.setDataRate(data_rate);

        // mask all TX events and allow only the RX
        radio.maskIRQ(1, 1, 0);

        // set the CRC configuration
        const uint8_t crc_mask = static_cast<uint8_t>(positions::NRF_CONFIG::CRC_LENGTH_MASK);
        const uint8_t pos_crc = static_cast<uint8_t>(positions::NRF_CONFIG::CRC_LENGTH);
        rf24_crclength_e crc_length = (rf24_crclength_e) ((NRF_CONFIG() & crc_mask) >> pos_crc);
        if (crc_length == rf24_crclength_e::RF24_CRC_DISABLED)
            radio.disableCRC();
        else
            radio.setCRCLength(crc_length);

        const uint8_t pa_level_mask = static_cast<uint8_t>(positions::NRF_CONFIG::PA_LEVEL_MASK);
        const uint8_t pos_palevel = static_cast<uint8_t>(positions::NRF_CONFIG::PA_LEVEL);
        uint8_t pa_level = ((NRF_CONFIG() & pa_level_mask) >> pos_palevel);
        radio.setPALevel(pa_level);
        HAL_Delay(10);

        char addr[6] = {
            NRF_MASTER_ADDRESS(),
            NRF_ADDRESS_2(),
            NRF_ADDRESS_3(),
            NRF_ADDRESS_4(),
            NRF_ADDRESS_5(),
            0
        };
        radio.openWritingPipe((uint8_t*) addr);

        addr[0] = NRF_ROBOT_ADDRESS();
        radio.openReadingPipe(1, (uint8_t*) addr);

        addr[0] = NRF_BROADCAST_ADDRESS();
        radio.openReadingPipe(2, (uint8_t*) addr);

        if ((NRF_CONFIG() & static_cast<uint8_t>(positions::NRF_CONFIG::ENABLE_AUTO_ACKNOWLEDGE_MASK)) > 0)
            radio.setAutoAck(1, true);
        else
            radio.setAutoAck(1, false);

        // disable auto-acknowledge for pipe 2 (broadcast transmissions)
        radio.setAutoAck(2, false);

        // make the radio keep listening
        radio.startListening();

        if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_RADIO_MASK)) == 0)
            radio.powerDown();

        // the radio was found
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::RADIO_NOT_FOUND_MASK);
    }

    else // if the radio was not found
    {
        STATUS |= static_cast<uint8_t>(positions::STATUS::RADIO_NOT_FOUND_MASK);
    }
}

void Manager::enable_radio()
{
    radio.powerUp();
    POWER_CONFIG |= static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_RADIO_MASK);
}

void Manager::disable_radio()
{
    radio.powerDown();
    POWER_CONFIG &= ~static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_RADIO_MASK);
}

void Manager::read_radio_msg()
{
    uint8_t pipe_number;

    if (radio.available(&pipe_number))
    {
        // Robot address
        if (pipe_number == 1)
            radio.read(rx_data, NRF_PAYLOAD_SIZE());

        // Broadcast address
        else if (pipe_number == 2)
            radio.read(rx_broadcast_data, NRF_PAYLOAD_SIZE());
    }
}

void Manager::deal_with_new_radio_msg()
{
    if (new_radio_message == true)
    {
        read_radio_msg();
        new_radio_message = false;
    }
}

void Manager::has_new_radio_msg()
{
    new_radio_message = true;
}

void Manager::enable_imu()
{
}

void Manager::disable_imu()
{
}

void Manager::enable_display()
{
}

void Manager::disable_display()
{
}

bool Manager::is_led_enabled()
{
    bool is_enabled = false;
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_LED_MASK)) > 0)
        is_enabled = true;

    return is_enabled;
}

void Manager::enable_led()
{
    POWER_CONFIG.set_bit(static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_LED));
    led_off();
}

void Manager::disable_led()
{
    POWER_CONFIG.clear_bit(static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_LED));
    led_off();
}

void Manager::led_on()
{
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_LED_MASK)) > 0)
    {
        hid::led::enable();
        HID_ON |= static_cast<uint8_t>(positions::HID_ON::LED_MASK);
    }

    else
    {
        led_off();
    }
}

void Manager::led_off()
{
    hid::led::disable();
    HID_ON &= ~static_cast<uint8_t>(positions::HID_ON::LED_MASK);
}

bool Manager::is_led_heart_beat_enabled()
{
    bool is_enabled = false;
    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::ENABLE_LED_HEART_BEAT_MASK)) > 0)
        is_enabled = true;

    return is_enabled;
}

void Manager::enable_led_heart_beat()
{
    WARNINGS_CONFIG.set_bit(static_cast<uint8_t>(positions::WARNINGS_CONFIG::ENABLE_LED_HEART_BEAT));
}

void Manager::disable_led_heart_beat()
{
    WARNINGS_CONFIG.clear_bit(static_cast<uint8_t>(positions::WARNINGS_CONFIG::ENABLE_LED_HEART_BEAT));
}

void Manager::power_cutoff()
{
    if ((WARNINGS_CONFIG() & static_cast<uint8_t>(positions::WARNINGS_CONFIG::BEEP_ON_BAT_CUTOFF_MASK)) == 0)
        disable_buzzer();

    disable_motors();
    disable_led();
    disable_radio();

    disable_display();
    is_display_enabled = false;

    disable_imu();
    is_imu_enabled = false;
}

void Manager::update_battery_status()
{
    if (BATTERY_VOLTAGE_MV() < BATTERY_CUTOFF_LEVEL_MV())
    {
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::BATTERY_OVER_VOLTAGE_MASK);
        STATUS |=  static_cast<uint8_t>(positions::STATUS::DISCHARGED_BATTERY_MASK);
        power_cutoff();
    }

    else if (BATTERY_VOLTAGE_MV() < BATTERY_MIN_LEVEL_MV())
    {
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::BATTERY_OVER_VOLTAGE_MASK);
        STATUS |=  static_cast<uint8_t>(positions::STATUS::DISCHARGED_BATTERY_MASK);
    }

    else if (BATTERY_VOLTAGE_MV() < BATTERY_MAX_LEVEL_MV())
    {
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::BATTERY_OVER_VOLTAGE_MASK);
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::DISCHARGED_BATTERY_MASK);
    }

    else // BATTERY_VOLTAGE_MV() > BATTERY_MAX_LEVEL_MV()
    {
        STATUS |= static_cast<uint8_t>(positions::STATUS::BATTERY_OVER_VOLTAGE_MASK);
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::DISCHARGED_BATTERY_MASK);
    }
}

void Manager::update_temperature_status()
{
    if (TEMPERATURE() > TEMPERATURE_MAX_LEVEL())
    {
        STATUS |= static_cast<uint8_t>(positions::STATUS::TEMPERATURE_TOO_HIGH_MASK);
    }

    else
    {
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::TEMPERATURE_TOO_HIGH_MASK);
    }
}

void Manager::update_from_power_config()
{
    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_DISPLAY_MASK)) > 0)
        is_display_enabled = true;
    else
        is_display_enabled = false;

    if ((POWER_CONFIG() & static_cast<uint8_t>(positions::POWER_CONFIG::ENABLE_IMU_MASK)) > 0)
        is_imu_enabled = true;
    else
        is_imu_enabled = false;
}

void Manager::update_register_total_current()
{
    float voltage = Kadc * static_cast<float>(adc_raw_values[0]);
    float sensor_gain = 1000 * static_cast<float>(TOTAL_CURRENT_SENSOR_GAIN());
    TOTAL_CURRENT_MA = (uint16_t) (sensor_gain * voltage);
}

void Manager::update_register_battery_voltage()
{
    float voltage = Kadc * static_cast<float>(adc_raw_values[1]);
    float sensor_gain = 1000 * static_cast<float>(BATTERY_SENSOR_GAIN());
    BATTERY_SENSOR_GAIN = (uint16_t) (sensor_gain * voltage);
}

void Manager::update_register_current_left_motor()
{
    float voltage = Kadc * static_cast<float>(adc_raw_values[2]);
    float sensor_gain = 1000 * static_cast<float>(L_MOTOR_CURRENT_SENSOR_GAIN());
    L_MOTOR_CURRENT_MA = (uint16_t) (sensor_gain * voltage);
}

void Manager::update_register_current_right_motor()
{
    float voltage = Kadc * static_cast<float>(adc_raw_values[3]);
    float sensor_gain = 1000 * static_cast<float>(R_MOTOR_CURRENT_SENSOR_GAIN());
    R_MOTOR_CURRENT_MA = (uint16_t) (sensor_gain * voltage);
}

void Manager::update_register_temperature()
{
    float voltage = Kadc * static_cast<float>(adc_raw_values[4]);
    float v25 = static_cast<float>(TEMP_SENSOR_V25());
    float avg_slope = static_cast<float>(TEMP_SENSOR_AVG_SLOPE());
    float temperature = (((v25 - voltage) / avg_slope) + 25);
    TEMPERATURE = (uint8_t) temperature;
}

void Manager::deal_with_new_ad_data()
{
    if (new_ad_data == true)
    {
        update_register_total_current();
        update_register_battery_voltage();
        update_register_current_left_motor();
        update_register_current_right_motor();
        update_register_temperature();

        new_ad_data = false;
    }
}

void Manager::has_new_ad_data()
{
    new_ad_data = true;
}

