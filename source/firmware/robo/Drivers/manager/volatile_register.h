#ifndef MANAGER_VOLATILE_REGISTER_H_
#define MANAGER_VOLATILE_REGISTER_H_

#include <stdint.h>
#include "registers_defs.h"

namespace regs {

template <typename T>
class VolatileRegister {
protected:
	const uint16_t address;
	T value;
	const uint8_t access;

public:
	VolatileRegister(uint16_t addr, const T initial_value, bool writable) :
		address(addr),
		value(initial_value),
		access(writable)
	{
	}

	T read()
	{
		return value;
	}

	void write(T new_value)
	{
        value = new_value;
	}

	void set_bit(T what_bit)
	{
		write(value | (1 << what_bit));
	}

	void clear_bit(T what_bit)
	{
		write(value & ~(1 << what_bit));
	}

    uint16_t get_addr() {
        return address;
    }

    void operator=(const T new_value)
    {
        write(new_value);
    }

    T operator()()
    {
        return read();
    }

    void operator|=(T new_mask)
    {
        write(value | new_mask);
    }

    void operator&=(T new_mask)
    {
        write(value & new_mask);
    }
};

} // namespace registers



#endif /* MANAGER_VOLATILE_REGISTER_H_ */
