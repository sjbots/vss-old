#ifndef MANAGER_REGISTERS_DEFS_H_
#define MANAGER_REGISTERS_DEFS_H_

namespace regs {

enum access_t {
    READ_ONLY = 0,
    READ_WRITE,
};

} // namespace regs

#endif /* MANAGER_REGISTERS_DEFS_H_ */
