#ifndef MANAGER_MANAGER_H_
#define MANAGER_MANAGER_H_

#include <stdbool.h>
#include <stdint.h>
#include "main.h"
#include "adc.h"

#include "registers.h"
#include "hardware_init.h"
#include "Drivers/hal/pwm.h"
#include "Drivers/hid/buzzer.h"
#include "Drivers/hid/led.h"
#include "Drivers/actuators/stspin240.h"
#include "Drivers/util/types.h"
#include "Drivers/signal/pid.h"

using namespace regs;

class Manager {
protected:
    // Enabled hardware
    bool is_display_enabled;
    bool is_imu_enabled;

    // NRF auxiliary variables
    char rx_data[32];
    char rx_broadcast_data[32];
    char tx_data[32];

    // A/D data pointer
    uint32_t* adc_raw_values;

    // Pending operations
    bool new_ad_data;
    bool new_radio_message;

    // PID control
    PID pid_left_motor;
    PID pid_right_motor;

    // Registers API
    void update_register_total_current();
    void update_register_battery_voltage();
    void update_register_current_left_motor();
    void update_register_current_right_motor();
    void update_register_temperature();

public:
    Manager();
    ~Manager();

    // Begin API
    void begin_encoders();
    void begin_imu();

    // A/D API
    void begin_adc(u32* raw_values);
    void deal_with_new_ad_data();
    void has_new_ad_data();

    // Buzzer API
    bool is_buzzer_enabled();
    void buzzer_begin();
    void enable_buzzer();
    void set_buzzer_frequency(uint16_t new_frequency);
    void buzzer_on();
    void buzzer_off();
    void has_to_beep();
    void disable_buzzer();

    // LED API
    bool is_led_enabled();
    void enable_led();
    void disable_led();
    void led_on();
    void led_off();
    bool is_led_heart_beat_enabled();
    void enable_led_heart_beat();
    void disable_led_heart_beat();

    // Display SSD1306 API
    void enable_display();
    void disable_display();

    // Motors API
    bool is_motors_enabled();
    bool is_motors_in_fault();
    void begin_motors();
    void enable_motors();
    void disable_motors();
    void set_duty_motor_L(float duty);
    void set_duty_motor_R(float duty);
    void update_motor_status();

    // Control API
    void begin_pid();
    void update_control_law();

    // IMU API
    void enable_imu();
    void disable_imu();

    // Radio API
    bool is_radio_enabled();
    void begin_radio();
    void enable_radio();
    void disable_radio();
    void read_radio_msg();
    void deal_with_new_radio_msg();
    void has_new_radio_msg();

    // Power API
    void power_cutoff();

    // Health API
    void update_battery_status();
    void update_temperature_status();

    // Registers API
    void update_from_power_config();
};

#endif /* MANAGER_MANAGER_H_ */
