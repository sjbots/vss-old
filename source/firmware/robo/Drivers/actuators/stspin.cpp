#include "stspin240.h"

STSPIN240::STSPIN240()
{
    en_fault_port = nullptr;
    stdby_port = nullptr;
    pha_port = nullptr;
    phb_port = nullptr;
    timer_cha = nullptr;
    timer_chb = nullptr;
}

STSPIN240::~STSPIN240()
{
}

void STSPIN240::set_pha_pin(uint32_t pin_number, GPIO_TypeDef* port)
{
    pha_pin = pin_number;
    pha_port = port;
}

void STSPIN240::set_phb_pin(uint32_t pin_number, GPIO_TypeDef* port)
{
    phb_pin = pin_number;
    phb_port = port;
}

void STSPIN240::set_stdby_pin(uint32_t pin_number, GPIO_TypeDef* port)
{
    stdby_pin = pin_number;
    stdby_port = port;
}

void STSPIN240::set_en_fault_pin(uint32_t pin_number, GPIO_TypeDef* port)
{
    en_fault_pin = pin_number;
    en_fault_port = port;
}

void STSPIN240::set_timer_cha(TIM_HandleTypeDef* timer, u32 channel)
{
    timer_cha = timer;
    timer_channel_cha = channel;
}

void STSPIN240::set_timer_chb(TIM_HandleTypeDef* timer, u32 channel)
{
    timer_chb = timer;
    timer_channel_chb = channel;
}

void STSPIN240::set_phase_cha(bool phase)
{
    phase_cha = phase;
}

void STSPIN240::set_phase_chb(bool phase)
{
    phase_chb = phase;
}

void STSPIN240::begin()
{
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

    // PHA pin
    if (pha_port != nullptr)
    {
        GPIO_InitStruct.Pin = pha_pin;
        HAL_GPIO_DeInit(pha_port, pha_pin);
        HAL_GPIO_Init(pha_port, &GPIO_InitStruct);
    }

    // PHB pin
    if (phb_port != nullptr)
    {
        GPIO_InitStruct.Pin = phb_pin;
        HAL_GPIO_DeInit(phb_port, phb_pin);
        HAL_GPIO_Init(phb_port, &GPIO_InitStruct);
    }

    // STDBY pin
    if (stdby_port != nullptr)
    {
        GPIO_InitStruct.Pin = stdby_pin;
        HAL_GPIO_DeInit(stdby_port, stdby_pin);
        HAL_GPIO_Init(stdby_port, &GPIO_InitStruct);
    }

    // EN/FAULT pin
    if (en_fault_port != nullptr)
    {
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
        GPIO_InitStruct.Pin = en_fault_pin;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        HAL_GPIO_DeInit(en_fault_port, en_fault_pin);
        HAL_GPIO_Init(en_fault_port, &GPIO_InitStruct);
    }

    // Start the PWM timer
    if (timer_cha != nullptr)
    {
        HAL_TIM_PWM_Start(timer_cha, timer_channel_cha);
    }

    if (timer_chb != nullptr)
    {
        HAL_TIM_PWM_Start(timer_chb, timer_channel_chb);
    }
}

void STSPIN240::enable()
{
    HAL_GPIO_WritePin(stdby_port, stdby_pin, GPIO_PIN_SET);
}

void STSPIN240::disable()
{
    HAL_GPIO_WritePin(stdby_port, stdby_pin, GPIO_PIN_RESET);
}

bool STSPIN240::fault_has_occurred()
{
    bool has_problem;
    if (HAL_GPIO_ReadPin(en_fault_port, en_fault_pin) == GPIO_PIN_RESET)
        has_problem = true;
    else
        has_problem = false;

    return has_problem;
}

void STSPIN240::set_duty_chA(float duty)
{
    if (duty > 0)
    {
        if (phase_cha > 0)
            HAL_GPIO_WritePin(pha_port, pha_pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(pha_port, pha_pin, GPIO_PIN_RESET);

        set_duty(timer_cha, timer_channel_cha, duty);
    }

    else
    {
        if (phase_cha > 0)
            HAL_GPIO_WritePin(pha_port, pha_pin, GPIO_PIN_RESET);
        else
            HAL_GPIO_WritePin(pha_port, pha_pin, GPIO_PIN_SET);

        set_duty(timer_cha, timer_channel_cha, -duty);
    }
}

void STSPIN240::set_duty_chB(float duty)
{
    if (duty > 0)
    {
        if (phase_chb > 0)
            HAL_GPIO_WritePin(phb_port, phb_pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(phb_port, phb_pin, GPIO_PIN_RESET);

        set_duty(timer_chb, timer_channel_chb, duty);
    }

    else
    {
        if (phase_chb > 0)
            HAL_GPIO_WritePin(phb_port, phb_pin, GPIO_PIN_RESET);
        else
            HAL_GPIO_WritePin(phb_port, phb_pin, GPIO_PIN_SET);

        set_duty(timer_chb, timer_channel_chb, -duty);
    }
}



