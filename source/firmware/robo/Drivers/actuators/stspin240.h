#ifndef ACTUATORS_STSPIN240_H_
#define ACTUATORS_STSPIN240_H_

#include <stm32f3xx.h>
#include "Drivers/util/types.h"
#include "Drivers/hal/pwm.h"


class STSPIN240 {
protected:
    u32 pha_pin;                           ///< PHA pin number
    GPIO_TypeDef* pha_port;                             ///< PHA port
    u32 phb_pin;                           ///< PHB pin number
    GPIO_TypeDef* phb_port;                             ///< PHB port
    u32 stdby_pin;                         ///< STDBY pin number
    GPIO_TypeDef* stdby_port;                           ///< STDBY port
    u32 en_fault_pin;                      ///< EN/FAULT pin number
    GPIO_TypeDef* en_fault_port;                        ///< EN/FAULT port

    TIM_HandleTypeDef* timer_cha;
    u32 timer_channel_cha;
    TIM_HandleTypeDef* timer_chb;
    u32 timer_channel_chb;

    u8 phase_cha;
    u8 phase_chb;

public:
    STSPIN240();
    ~STSPIN240();

    void set_pha_pin(u32 pin_number, GPIO_TypeDef* port);
    void set_phb_pin(u32 pin_number, GPIO_TypeDef* port);
    void set_stdby_pin(u32 pin_number, GPIO_TypeDef* port);
    void set_en_fault_pin(u32 pin_number, GPIO_TypeDef* port);
    void set_timer_cha(TIM_HandleTypeDef* timer, u32 channel);
    void set_timer_chb(TIM_HandleTypeDef* timer, u32 channel);
    void set_phase_cha(bool phase);
    void set_phase_chb(bool phase);
    void begin();

    void enable();
    void disable();

    void set_duty_chA(float duty);
    void set_duty_chB(float duty);

    bool fault_has_occurred();
};


#endif /* ACTUATORS_STSPIN240_H_ */
