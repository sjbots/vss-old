#ifndef EEPROM_MEMORY_M24_H_
#define EEPROM_MEMORY_M24_H_

#include <stdint.h>
#include <stm32f3xx.h>
#include "stm32f3xx_hal_i2c.h"

class M24 {
protected:
	uint8_t i2c_address;
	static const uint16_t size_bytes;
	static const uint8_t timeout = 10;

	I2C_HandleTypeDef* hi2c;							///< I2C handle object
	GPIO_InitTypeDef rw_pin;							///< RW pin number
	GPIO_TypeDef* rw_pin_port;							///< RW pin port

public:
	M24();
    ~M24();
    void begin(I2C_HandleTypeDef* hi2c, uint8_t i2c_address);
    void begin(I2C_HandleTypeDef* hi2c, uint8_t i2c_address, uint32_t rw_pin_number, GPIO_TypeDef* rw_pin_port);
	void unlock_writing();
	void lock_writing();
	void write(uint16_t address, uint16_t length, uint8_t* values);
	void read(uint16_t address, uint16_t length, uint8_t* values);
	void reset(uint16_t address);
	void reset_all();
};


class M24_C01 : public M24 {
protected:
	static const uint16_t size_bytes = 128;
};

class M24_C02 : public M24 {
protected:
	static const uint16_t size_bytes = 256;
};

class M24_LC128 : public M24 {
protected:
	static const uint16_t size_bytes = 128;
};

class M24_LC256 : public M24 {
protected:
	static const uint16_t size_bytes = 256;
};


#endif /* EEPROM_MEMORY_M24_H_ */
