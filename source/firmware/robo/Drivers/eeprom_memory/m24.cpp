#include "m24.h"

M24::M24()
{
}

M24::~M24()
{
}

void M24::begin(I2C_HandleTypeDef* hi2c, uint8_t i2c_address)
{
    this->hi2c = hi2c;
    this->rw_pin_port = nullptr;
    this->i2c_address = i2c_address;
}

void M24::begin(I2C_HandleTypeDef* hi2c, uint8_t i2c_address, uint32_t rw_pin_number, GPIO_TypeDef* rw_pin_port)
{
    this->hi2c = hi2c;
    this->i2c_address = i2c_address;
    rw_pin.Pin = rw_pin_number;
    rw_pin.Mode = GPIO_MODE_OUTPUT_PP;
    rw_pin.Pull = GPIO_NOPULL;
    rw_pin.Speed = GPIO_SPEED_FREQ_HIGH;
    this->rw_pin_port = rw_pin_port;
    unlock_writing();
}

void M24::unlock_writing()
{
    if (rw_pin_port != nullptr)
    {
        HAL_GPIO_WritePin(rw_pin_port, rw_pin.Pin, GPIO_PIN_RESET);
    }
}

void M24::lock_writing()
{
    if (rw_pin_port != nullptr)
    {
        HAL_GPIO_WritePin(rw_pin_port, rw_pin.Pin, GPIO_PIN_SET);
    }
}

void M24::write(uint16_t address, uint16_t length, uint8_t* values)
{
    uint8_t i;
    for (i = 0; i < length; i++)
    {
        if (address < 127)
            HAL_I2C_Mem_Write(hi2c, i2c_address, address+i, I2C_MEMADD_SIZE_8BIT, &values[i], 1, timeout);
        else
            HAL_I2C_Mem_Write(hi2c, i2c_address+1, address+i, I2C_MEMADD_SIZE_8BIT, &values[i], 1, timeout);
        HAL_Delay(5);
    }
}

void M24::read(uint16_t address, uint16_t length, uint8_t* values)
{
    uint8_t i;
    for (i = 0; i < length; i++)
    {
        if (address < 127)
            HAL_I2C_Mem_Read(hi2c, i2c_address, address+i, I2C_MEMADD_SIZE_8BIT, &values[i], 1, timeout);
        else
            HAL_I2C_Mem_Read(hi2c, i2c_address+1, address+i, I2C_MEMADD_SIZE_8BIT, &values[i], 1, timeout);
        HAL_Delay(5);
    }
}

void M24::reset(uint16_t address)
{
    uint8_t reset_value = 0xFF;
    write(address, 1, &reset_value);
}

void M24::reset_all()
{
    uint16_t addr;
    uint8_t reset_value = 0xFF;
    for (addr = 0; addr < 256; addr++)
        write(addr, 1, &reset_value);
}
