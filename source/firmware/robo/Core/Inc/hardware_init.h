#ifndef INC_HARDWARE_INIT_H_
#define INC_HARDWARE_INIT_H_

#include <stdint.h>
#include "i2c.h"

#include "Config/board.h"
#include "Drivers/eeprom_memory/m24.h"
#include "Drivers/communication/nrf24l01/RF24.h"
#include "Drivers/actuators/stspin240.h"

constexpr uint8_t m24c02_address = 0b10100000;
extern M24_C02 m24c02;
extern M24_C02* EEPROM_HANDLER;

extern RF24 radio;

extern STSPIN240 hbridge;

#endif /* INC_HARDWARE_INIT_H_ */
