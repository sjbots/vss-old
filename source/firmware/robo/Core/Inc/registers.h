#ifndef MANAGER_REGISTERS_H_
#define MANAGER_REGISTERS_H_

#include "Drivers/manager/volatile_register.h"
#include "Drivers/manager/persistent_register.h"
#include "hardware_init.h"

namespace regs {

constexpr uint8_t offset = 64;


extern VolatileRegister<int16_t>    L_MOTOR_SPEED_RPM;               ///< Left wheel speed (RPM)
extern VolatileRegister<uint16_t>   L_MOTOR_CURRENT_MA;              ///< Left wheel current (mA)
extern VolatileRegister<int16_t>    R_MOTOR_SPEED_RPM;               ///< Right wheel speed (RPM)
extern VolatileRegister<float>      ANGULAR_SPEED_DEG;               ///< Robot angular speed (degrees)
extern VolatileRegister<uint16_t>   R_MOTOR_CURRENT_MA;              ///< Right wheel current (mA)
extern VolatileRegister<uint16_t>   TOTAL_CURRENT_MA;                ///< Total current drained from battery (mA)
extern VolatileRegister<uint16_t>   BATTERY_VOLTAGE_MV;              ///< Battery voltage (mV)
extern VolatileRegister<int8_t>     TEMPERATURE;                     ///< Robot temperature (ºC)
extern VolatileRegister<int8_t>     STATUS;
extern VolatileRegister<int8_t>     MOTOR_STATUS;
extern VolatileRegister<int16_t>    L_MOTOR_SPEED_REF_RPM;
extern VolatileRegister<int16_t>    R_MOTOR_SPEED_REF_RPM;
extern VolatileRegister<uint8_t>    POWER_CONFIG;
extern VolatileRegister<uint8_t>    DISPLAY_CONFIG;
extern VolatileRegister<uint8_t>    HID_ON;


extern PersistentRegister<int8_t>   L_MOTOR_ENC_CW_INCREMENT;
extern PersistentRegister<float>    L_MOTOR_KENC_TIME;
extern PersistentRegister<float>    L_MOTOR_KENC_COUNT;
extern PersistentRegister<float>    L_MOTOR_P_GAIN;
extern PersistentRegister<float>    L_MOTOR_I_GAIN;
extern PersistentRegister<float>    L_MOTOR_D_GAIN;
extern PersistentRegister<float>    L_MOTOR_I_SAT;
extern PersistentRegister<float>    L_MOTOR_U_MIN;
extern PersistentRegister<float>    L_MOTOR_U_MAX;
extern PersistentRegister<float>    L_MOTOR_U_MAX_RATE;
extern PersistentRegister<float>    L_MOTOR_FEEDFORWARD_GAIN;
extern PersistentRegister<int8_t>   R_MOTOR_ENC_CW_INCREMENT;
extern PersistentRegister<float>    R_MOTOR_KENC_TIME;
extern PersistentRegister<float>    R_MOTOR_KENC_COUNT;
extern PersistentRegister<float>    R_MOTOR_P_GAIN;
extern PersistentRegister<float>    R_MOTOR_I_GAIN;
extern PersistentRegister<float>    R_MOTOR_D_GAIN;
extern PersistentRegister<float>    R_MOTOR_I_SAT;
extern PersistentRegister<float>    R_MOTOR_U_MIN;
extern PersistentRegister<float>    R_MOTOR_U_MAX;
extern PersistentRegister<float>    R_MOTOR_U_MAX_RATE;
extern PersistentRegister<float>    R_MOTOR_FEEDFORWARD_GAIN;
extern PersistentRegister<uint8_t>  SAMPLE_TIME_MS;
extern PersistentRegister<uint8_t>  NRF_CHANNEL_CONFIG;
extern PersistentRegister<uint8_t>  NRF_PAYLOAD_SIZE;
extern PersistentRegister<char>     NRF_ROBOT_ADDRESS;
extern PersistentRegister<char>     NRF_MASTER_ADDRESS;
extern PersistentRegister<char>     NRF_BROADCAST_ADDRESS;
extern PersistentRegister<char>     NRF_ADDRESS_2;
extern PersistentRegister<char>     NRF_ADDRESS_3;
extern PersistentRegister<char>     NRF_ADDRESS_4;
extern PersistentRegister<char>     NRF_ADDRESS_5;
extern PersistentRegister<uint8_t>  NRF_CONFIG;
extern PersistentRegister<uint16_t> BATTERY_CUTOFF_LEVEL_MV;
extern PersistentRegister<uint16_t> BATTERY_MIN_LEVEL_MV;
extern PersistentRegister<uint16_t> BATTERY_MAX_LEVEL_MV;
extern PersistentRegister<float>    L_MOTOR_CURRENT_SENSOR_GAIN;
extern PersistentRegister<float>    R_MOTOR_CURRENT_SENSOR_GAIN;
extern PersistentRegister<float>    TOTAL_CURRENT_SENSOR_GAIN;
extern PersistentRegister<float>    BATTERY_SENSOR_GAIN;
extern PersistentRegister<uint16_t> MOTORS_PWM_FREQ;
extern PersistentRegister<uint16_t> L_MOTOR_MAX_CURRENT_MA;
extern PersistentRegister<uint16_t> R_MOTOR_MAX_CURRENT_MA;
extern PersistentRegister<uint8_t>  EEPROM_INITILIZED;
extern PersistentRegister<float>    TEMP_SENSOR_V25;
extern PersistentRegister<float>    TEMP_SENSOR_AVG_SLOPE;
extern PersistentRegister<uint16_t> BUZZER_FREQUENCY;
extern PersistentRegister<int8_t>   TEMPERATURE_MAX_LEVEL;
extern PersistentRegister<uint8_t>  WARNINGS_CONFIG;
extern PersistentRegister<uint8_t>  L_MOTOR_POS_DUTY_IS_FORWARD;
extern PersistentRegister<uint8_t>  R_MOTOR_POS_DUTY_IS_FORWARD;

extern void init();
extern void reset_all();

namespace positions {

/**
 * This register stores information about possible errors with general hardware in the board
 */
enum class STATUS: uint8_t {
    BATTERY_OVER_VOLTAGE = 0,                                   ///< This bit is set if the battery voltage is too high
    DISCHARGED_BATTERY = 1,                                     ///< This bit is set if the battery voltage is too low
    RADIO_NOT_FOUND = 2,                                        ///< This bit is set if the NRF24L01 was not found
    TOTAL_CURRENT_SENSOR_FAIL = 4,
    BATTERY_VOLTAGE_SENSOR_FAIL = 5,
    TEMPERATURE_TOO_HIGH = 6,                                   ///< This bit is set if the intern temperature is too high
    H_BRIDGE_OVERLOAD = 7,                                      ///< This bit is set if overcurrent or thermal shutdown are detected
    BATTERY_OVER_VOLTAGE_MASK = (1 << BATTERY_OVER_VOLTAGE),    ///< Wrong battery mask
    DISCHARGED_BATTERY_MASK = (1 << DISCHARGED_BATTERY),        ///< Discharged battery mask
    RADIO_NOT_FOUND_MASK = (1 << RADIO_NOT_FOUND),              ///< Radio not found mask
    H_BRIDGE_OVERLOAD_MASK = (1 << H_BRIDGE_OVERLOAD),          ///< H-bridge overload mask
    TEMPERATURE_TOO_HIGH_MASK = (1 << TEMPERATURE_TOO_HIGH),    ///< Temperature too high mask
};

enum class MOTOR_STATUS: uint8_t {
    R_MOTOR_OPEN = 0,
    R_MOTOR_OPEN_MASK = (1 << R_MOTOR_OPEN),
    L_MOTOR_OPEN = 1,
    L_MOTOR_OPEN_MASK = (1 << L_MOTOR_OPEN),
    R_MOTOR_SHORTED = 2,
    R_MOTOR_SHORTED_MASK = (1 << R_MOTOR_SHORTED),
    L_MOTOR_SHORTED = 3,
    L_MOTOR_SHORTED_MASK = (1 << L_MOTOR_SHORTED),
    R_MOTOR_ENCODER_FAIL = 4,
    R_MOTOR_ENCODER_FAIL_MASK = (1 << R_MOTOR_ENCODER_FAIL),
    L_MOTOR_ENCODER_FAIL = 5,
    L_MOTOR_ENCODER_FAIL_MASK = (1 << L_MOTOR_ENCODER_FAIL),
    R_MOTOR_ENCODER_BAD_CABLING = 6,
    R_MOTOR_ENCODER_BAD_CABLING_MASK = (1 << R_MOTOR_ENCODER_BAD_CABLING),
    L_MOTOR_ENCODER_BAD_CABLING = 7,
    L_MOTOR_ENCODER_BAD_CABLING_MASK = (1 << L_MOTOR_ENCODER_BAD_CABLING),
};

enum class POWER_CONFIG: uint8_t {
    ENABLE_DISPLAY = 0,                             ///< Enable display bit
    ENABLE_MOTORS = 1,                              ///< Enable motors bit
    ENABLE_IMU = 2,                                 ///< Enable IMU bit
    ENABLE_RADIO = 3,                               ///< Enable radio bit
    ENABLE_BUZZER = 4,                              ///< Enable buzzer bit
    ENABLE_LED = 5,                                 ///< Enable LED bit
    ENABLE_DISPLAY_MASK = (1 << ENABLE_DISPLAY),    ///< Enable display mask
    ENABLE_MOTORS_MASK = (1 << ENABLE_MOTORS),      ///< Enable motors mask
    ENABLE_IMU_MASK = (1 << ENABLE_IMU),            ///< Enable IMU mask
    ENABLE_RADIO_MASK = (1 << ENABLE_RADIO),        ///< Enable radio mask
    ENABLE_BUZZER_MASK = (1 << ENABLE_BUZZER),      ///< Enable buzzer mask
    ENABLE_LED_MASK = (1 << ENABLE_LED),            ///< Enable LED mask
};


enum class DISPLAY_CONFIG: uint8_t {
    SHOW_BATTERY_LEVEL = 0,
    SHOW_NRF_RSSI_LEVEL = 1,
    SHOW_TEMPERATURE = 2,

    /**
     * Something like the N1 indication of Boeing 737
     * (it includes the ref speed and actual speed in very intuitive draw)
     *
     * https://photos.smugmug.com/Aviation/Aircraft/i-tpZrGrz/2/cfe4cc2d/M/_KCB0803-M.jpg
     */
    SHOW_MOTORS_SPEED = 3,
    SHOW_MOTORS_STATUS = 4,
    SHOW_ROBOT_ID = 5,
    SHOW_HAPPYNESS = 6,
    SHOW_SADNESS = 7,
};

enum class WARNINGS_CONFIG: uint8_t {
    BEEP_ON_BAT_CUTOFF = 0,                                     ///< If set the buzzer will beep on cutoff
    BEEP_ON_LOW_BAT = 1,                                        ///< If set the buzzer will beep on low battery voltage
    BEEP_ON_BAT_OVERVOLTAGE = 2,                                ///< If set the buzzer will bee on battery overvoltage
    BEEP_ON_MOTOR_FAILURE = 3,
    BEEP_ON_HIGH_TEMP = 4,
    BEEP_ON_CONNECTION = 5,
    BEEP_ON_CONNECTION_LOSS = 6,
    ENABLE_LED_HEART_BEAT = 7,                                  ///< If set the led will blink like a heart-beat (I am alive!)
    BEEP_ON_BAT_CUTOFF_MASK = (1 << BEEP_ON_BAT_CUTOFF),        ///< Beep on battery cut-off voltage mask
    BEEP_ON_LOW_BAT_MASK = (1 << BEEP_ON_LOW_BAT),              ///< Beep on low battery mask
    BEEP_ON_BAT_OVERVOLTAGE_MASK = (1 << BEEP_ON_BAT_OVERVOLTAGE), ///< Beep on battery over voltage mask
    BEEP_ON_MOTOR_FAILURE_MASK = (1 << BEEP_ON_MOTOR_FAILURE),  ///< Beep on motor failure
    BEEP_ON_HIGH_TEMP_MASK = (1 << BEEP_ON_HIGH_TEMP),          ///< Beep on high temperature
    BEEP_ON_CONNECTION_MASK = (1 << BEEP_ON_CONNECTION),        ///< Beep on connection
    BEEP_ON_CONNECTION_LOSS_MASK = (1 << BEEP_ON_CONNECTION_LOSS), ///< Beep on connection
    ENABLE_LED_HEART_BEAT_MASK = (1 << ENABLE_LED_HEART_BEAT),  ///< Heart-beat enable mask
};

enum class HID_ON: uint8_t {
    LED = 0,                                                    ///< This bit is set if the board LED is turned on
    BUZZER = 1,                                                 ///< This bit is set if the buzzer is buzzing
    LED_MASK = (1 << LED),                                      ///< LED turned on mask
    BUZZER_MASK = (1 << BUZZER),                                ///< Buzzer turned on mask
};

enum class NRF_CONFIG: uint8_t {
    /**
     * Sets the transmission power level. Choose accordingly to the following table:
     *
     * | Power level | Value |
     * | :---------- | :---: |
     * | MIN         | 0     |
     * | LOW         | 1     |
     * | HIGH        | 2     |
     * | MAX         | 3     |
     */
    PA_LEVEL = 0,
    PA_LEVEL_MASK = (0b11 << PA_LEVEL),

    /**
     * Sets the data rate used for transmission. Choose accordingly to the following table:
     *
     * | Data rate |
     * | :-------- |
     * | 250KBPS   |
     * | 1MBPS     |
     * | 2MBPS     |
     */
    DATA_RATE = 2,
    DATA_RATE_MASK = (0b11 << DATA_RATE),

    /**
     * Enables the auto-acknowledge.
     */
    ENABLE_AUTO_ACKNOWLEDGE = 4,
    ENABLE_AUTO_ACKNOWLEDGE_MASK = (1 << ENABLE_AUTO_ACKNOWLEDGE),

    /**
     * Set CRC used in transmission. Choose accordingly to the following table:
     *
     * | CRC |
     * | :-------- |
     * | DISABLED  |
     * | LENGTH_8  |
     * | LENGTH_16 |
     */
    CRC_LENGTH = 5,
    CRC_LENGTH_MASK = (0b11 << CRC_LENGTH),
};

}  // namespace registers
}  // namespace positions


#endif /* MANAGER_REGISTERS_H_ */
