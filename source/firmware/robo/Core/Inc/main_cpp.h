#ifndef INC_MAIN_CPP_H_
#define INC_MAIN_CPP_H_


#include <stdint.h>
#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void main_cpp();

#ifdef __cplusplus
}
#endif


#endif /* INC_MAIN_CPP_H_ */
