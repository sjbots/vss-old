#include "hardware_init.h"

// EEPROM memory
M24_C02 m24c02;
M24_C02* EEPROM_HANDLER = &m24c02;

// NRF24L01
RF24 radio;

// H-BRIDGE
STSPIN240 hbridge;
