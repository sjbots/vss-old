#include "registers.h"

namespace regs {

VolatileRegister<int16_t>    L_MOTOR_SPEED_RPM              (  0,    0,   READ_ONLY);
VolatileRegister<uint16_t>   L_MOTOR_CURRENT_MA             (  2,    0,   READ_ONLY);
VolatileRegister<int16_t>    R_MOTOR_SPEED_RPM              (  4,    0,   READ_ONLY);
VolatileRegister<float>      ANGULAR_SPEED_DEG              (  6,    0,   READ_ONLY);
VolatileRegister<uint16_t>   R_MOTOR_CURRENT_MA             ( 10,    0,   READ_ONLY);
VolatileRegister<uint16_t>   TOTAL_CURRENT_MA               ( 12,    0,   READ_ONLY);
VolatileRegister<uint16_t>   BATTERY_VOLTAGE_MV             ( 14,    0,   READ_ONLY);
VolatileRegister<int8_t>     TEMPERATURE                    ( 16,    0,   READ_ONLY);
VolatileRegister<int8_t>     STATUS                         ( 17,    0,   READ_ONLY);
VolatileRegister<int8_t>     MOTOR_STATUS                   ( 18,    0,   READ_ONLY);
VolatileRegister<int16_t>    L_MOTOR_SPEED_REF_RPM          ( 32,    0,   READ_WRITE);
VolatileRegister<int16_t>    R_MOTOR_SPEED_REF_RPM          ( 34,    0,   READ_WRITE);
VolatileRegister<uint8_t>    POWER_CONFIG                   ( 36, 0x3F,   READ_WRITE);
VolatileRegister<uint8_t>    DISPLAY_CONFIG                 ( 37,    0,   READ_WRITE);
VolatileRegister<uint8_t>    HID_ON                         ( 38,    0,   READ_WRITE);

// max address of volatile registers is offset-1 = 63

PersistentRegister<int8_t>   L_MOTOR_ENC_CW_INCREMENT       (offset+  0,   0,      1, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_KENC_TIME              (offset+  1,   1,   1e-6, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_KENC_COUNT             (offset+  5,   5,    1.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_P_GAIN                 (offset+  9,   9,    1.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_I_GAIN                 (offset+ 13,  13,    0.1, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_D_GAIN                 (offset+ 17,  17,    0.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_I_SAT                  (offset+ 21,  21,  100.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_U_MIN                  (offset+ 25,  25, -100.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_U_MAX                  (offset+ 29,  29,  100.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_U_MAX_RATE             (offset+ 33,  33,   10.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_FEEDFORWARD_GAIN       (offset+ 37,  37,    0.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<int8_t>   R_MOTOR_ENC_CW_INCREMENT       (offset+ 41,  41,     -1, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_KENC_TIME              (offset+ 42,  42,   1e-6, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_KENC_COUNT             (offset+ 46,  46,    1.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_P_GAIN                 (offset+ 50,  50,    1.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_I_GAIN                 (offset+ 54,  54,    0.1, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_D_GAIN                 (offset+ 58,  58,    0.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_I_SAT                  (offset+ 62,  62,  100.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_U_MIN                  (offset+ 66,  66, -100.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_U_MAX                  (offset+ 70,  70,  100.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_U_MAX_RATE             (offset+ 74,  74,   10.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_FEEDFORWARD_GAIN       (offset+ 78,  78,    0.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint8_t>  SAMPLE_TIME_MS                 (offset+ 82,  82,     10, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint8_t>  NRF_CHANNEL_CONFIG             (offset+ 83,  83,     23, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint8_t>  NRF_PAYLOAD_SIZE               (offset+ 84,  84,     32, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<char>     NRF_ROBOT_ADDRESS              (offset+ 85,  85,    'a', READ_WRITE, EEPROM_HANDLER);
PersistentRegister<char>     NRF_MASTER_ADDRESS             (offset+ 86,  86,    'A', READ_WRITE, EEPROM_HANDLER);
PersistentRegister<char>     NRF_BROADCAST_ADDRESS          (offset+ 87,  87,    '*', READ_WRITE, EEPROM_HANDLER);
PersistentRegister<char>     NRF_ADDRESS_2                  (offset+ 88,  88,    'I', READ_WRITE, EEPROM_HANDLER);
PersistentRegister<char>     NRF_ADDRESS_3                  (offset+ 89,  89,    'F', READ_WRITE, EEPROM_HANDLER);
PersistentRegister<char>     NRF_ADDRESS_4                  (offset+ 90,  90,    'S', READ_WRITE, EEPROM_HANDLER);
PersistentRegister<char>     NRF_ADDRESS_5                  (offset+ 91,  91,    'P', READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint8_t>  NRF_CONFIG                     (offset+ 92,  92,     55, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint16_t> BATTERY_CUTOFF_LEVEL_MV        (offset+ 93,  93,   6000, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint16_t> BATTERY_MIN_LEVEL_MV           (offset+ 95,  95,   7400, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint16_t> BATTERY_MAX_LEVEL_MV           (offset+ 97,  97,   8510, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    L_MOTOR_CURRENT_SENSOR_GAIN    (offset+ 99,  99,    1.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    R_MOTOR_CURRENT_SENSOR_GAIN    (offset+103, 103,    1.0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    TOTAL_CURRENT_SENSOR_GAIN      (offset+107, 107,   0.64, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    BATTERY_SENSOR_GAIN            (offset+111, 111,    3.2, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint16_t> MOTORS_PWM_FREQ                (offset+115, 115,    1e4, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint16_t> L_MOTOR_MAX_CURRENT_MA         (offset+119, 119,   1000, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint16_t> R_MOTOR_MAX_CURRENT_MA         (offset+121, 121,   1000, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint8_t>  EEPROM_INITILIZED              (offset+123, 123,      1, READ_ONLY,  EEPROM_HANDLER);
PersistentRegister<float>    TEMP_SENSOR_V25                (offset+124, 124,   1.43, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<float>    TEMP_SENSOR_AVG_SLOPE          (offset+128, 128, 4.3e-3, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint16_t> BUZZER_FREQUENCY               (offset+132, 132,   1000, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<int8_t>   TEMPERATURE_MAX_LEVEL          (offset+134, 134,     60, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint8_t>  WARNINGS_CONFIG                (offset+135, 135,      0, READ_WRITE, EEPROM_HANDLER); // initial value is 0x87
PersistentRegister<uint8_t>  L_MOTOR_POS_DUTY_IS_FORWARD    (offset+136, 136,      0, READ_WRITE, EEPROM_HANDLER);
PersistentRegister<uint8_t>  R_MOTOR_POS_DUTY_IS_FORWARD    (offset+137, 137,      1, READ_WRITE, EEPROM_HANDLER);

void reset_all()
{
    L_MOTOR_ENC_CW_INCREMENT.reset();
    L_MOTOR_KENC_TIME.reset();
    L_MOTOR_KENC_COUNT.reset();
    L_MOTOR_P_GAIN.reset();
    L_MOTOR_I_GAIN.reset();
    L_MOTOR_D_GAIN.reset();
    L_MOTOR_I_SAT.reset();
    L_MOTOR_U_MIN.reset();
    L_MOTOR_U_MAX.reset();
    L_MOTOR_U_MAX_RATE.reset();
    L_MOTOR_FEEDFORWARD_GAIN.reset();
    R_MOTOR_ENC_CW_INCREMENT.reset();
    R_MOTOR_KENC_TIME.reset();
    R_MOTOR_KENC_COUNT.reset();
    R_MOTOR_P_GAIN.reset();
    R_MOTOR_I_GAIN.reset();
    R_MOTOR_D_GAIN.reset();
    R_MOTOR_I_SAT.reset();
    R_MOTOR_U_MIN.reset();
    R_MOTOR_U_MAX.reset();
    R_MOTOR_U_MAX_RATE.reset();
    R_MOTOR_FEEDFORWARD_GAIN.reset();
    SAMPLE_TIME_MS.reset();
    NRF_CHANNEL_CONFIG.reset();
    NRF_PAYLOAD_SIZE.reset();
    NRF_ROBOT_ADDRESS.reset();
    NRF_MASTER_ADDRESS.reset();
    NRF_BROADCAST_ADDRESS.reset();
    NRF_ADDRESS_2.reset();
    NRF_ADDRESS_3.reset();
    NRF_ADDRESS_4.reset();
    NRF_ADDRESS_5.reset();
    NRF_CONFIG.reset();
    BATTERY_CUTOFF_LEVEL_MV.reset();
    BATTERY_MIN_LEVEL_MV.reset();
    BATTERY_MAX_LEVEL_MV.reset();
    L_MOTOR_CURRENT_SENSOR_GAIN.reset();
    R_MOTOR_CURRENT_SENSOR_GAIN.reset();
    TOTAL_CURRENT_SENSOR_GAIN.reset();
    BATTERY_SENSOR_GAIN.reset();
    MOTORS_PWM_FREQ.reset();
    L_MOTOR_MAX_CURRENT_MA.reset();
    R_MOTOR_MAX_CURRENT_MA.reset();
    EEPROM_INITILIZED.reset();
    TEMP_SENSOR_V25.reset();
    TEMP_SENSOR_AVG_SLOPE.reset();
    BUZZER_FREQUENCY.reset();
    TEMPERATURE_MAX_LEVEL.reset();
    WARNINGS_CONFIG.reset();
    L_MOTOR_POS_DUTY_IS_FORWARD.reset();
    R_MOTOR_POS_DUTY_IS_FORWARD.reset();
}

void init()
{
    uint8_t is_eeprom_initilized = EEPROM_INITILIZED.read();
    if (is_eeprom_initilized != 1)
    {
        reset_all();
    }

    else
    {
        L_MOTOR_ENC_CW_INCREMENT.begin();
        L_MOTOR_KENC_TIME.begin();
        L_MOTOR_KENC_COUNT.begin();
        L_MOTOR_P_GAIN.begin();
        L_MOTOR_I_GAIN.begin();
        L_MOTOR_D_GAIN.begin();
        L_MOTOR_I_SAT.begin();
        L_MOTOR_U_MIN.begin();
        L_MOTOR_U_MAX.begin();
        L_MOTOR_U_MAX_RATE.begin();
        L_MOTOR_FEEDFORWARD_GAIN.begin();
        R_MOTOR_ENC_CW_INCREMENT.begin();
        R_MOTOR_KENC_TIME.begin();
        R_MOTOR_KENC_COUNT.begin();
        R_MOTOR_P_GAIN.begin();
        R_MOTOR_I_GAIN.begin();
        R_MOTOR_D_GAIN.begin();
        R_MOTOR_I_SAT.begin();
        R_MOTOR_U_MIN.begin();
        R_MOTOR_U_MAX.begin();
        R_MOTOR_U_MAX_RATE.begin();
        R_MOTOR_FEEDFORWARD_GAIN.begin();
        SAMPLE_TIME_MS.begin();
        NRF_CHANNEL_CONFIG.begin();
        NRF_PAYLOAD_SIZE.begin();
        NRF_ROBOT_ADDRESS.begin();
        NRF_MASTER_ADDRESS.begin();
        NRF_BROADCAST_ADDRESS.begin();
        NRF_ADDRESS_2.begin();
        NRF_ADDRESS_3.begin();
        NRF_ADDRESS_4.begin();
        NRF_ADDRESS_5.begin();
        NRF_CONFIG.begin();
        BATTERY_CUTOFF_LEVEL_MV.begin();
        BATTERY_MIN_LEVEL_MV.begin();
        BATTERY_MAX_LEVEL_MV.begin();
        L_MOTOR_CURRENT_SENSOR_GAIN.begin();
        R_MOTOR_CURRENT_SENSOR_GAIN.begin();
        TOTAL_CURRENT_SENSOR_GAIN.begin();
        BATTERY_SENSOR_GAIN.begin();
        MOTORS_PWM_FREQ.begin();
        L_MOTOR_MAX_CURRENT_MA.begin();
        R_MOTOR_MAX_CURRENT_MA.begin();
        EEPROM_INITILIZED.begin();
        TEMP_SENSOR_V25.begin();
        TEMP_SENSOR_AVG_SLOPE.begin();
        BUZZER_FREQUENCY.begin();
        TEMPERATURE_MAX_LEVEL.begin();
        WARNINGS_CONFIG.begin();
        L_MOTOR_POS_DUTY_IS_FORWARD.begin();
        R_MOTOR_POS_DUTY_IS_FORWARD.begin();
    }
}

}  // namespace registers

