#include "main_cpp.h"

#include "Drivers/manager/manager.h"
#include "hardware_init.h"

// ADC auxiliary variables
uint32_t adc_buffer_raw[NUMBER_OF_ADS];
uint16_t counter = 0;

Manager manager;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    if (htim->Instance == TIM6)
    {
        // Time base (1 ms)
        counter = counter + 1;

        // Update the encoders time measurement
//        uint8_t i;
//        for (i = 0; i < NUMBER_OF_ENCODERS; i++)
//        {
//            if (encs[i].is_enabled())
//            {
//                encs[i].update_on_overflow();
//            }
//        }

        // 100 ms interrupt
        if ((counter % 100) == 0)
        {
            manager.update_battery_status();
            manager.has_to_beep();
        }

        // 1000 ms interrupt
        if ((counter % 1000) == 0)
        {
            counter = 0;
        }

        // Trigger an Analog Conversion
        HAL_ADC_Start_IT(&hadc1);
    }
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    manager.has_new_ad_data();
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == NRF_IRQ_Pin)
    {
        manager.has_new_radio_msg();
    }
}

void main_cpp()
{
    // Initialize the EEPROM memory and persistent registers
    m24c02.begin(&hi2c2, m24c02_address, EEPROM_RW_Pin, EEPROM_RW_GPIO_Port);
    regs::init();

    // Start buzzer
    manager.buzzer_begin();

    // Start the radio
    manager.begin_radio();

    // Start the h-bridge
    manager.begin_motors();

    // Start the ADC
    manager.begin_adc(adc_buffer_raw);

    // Start main system timer (1 ms interrupt)
    HAL_TIM_Base_Start_IT(&(BOARD_CLOCK_TIMER));

	while (1)
	{
	    if (manager.is_led_heart_beat_enabled() != false)
	    {
	        manager.led_on();
            HAL_Delay(100);
            manager.led_off();
            HAL_Delay(200);
            manager.led_on();
            HAL_Delay(100);
            manager.led_off();
            HAL_Delay(600);
	    }

	    manager.deal_with_new_ad_data();
	    manager.deal_with_new_radio_msg();
	}
}




