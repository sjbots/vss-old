#ifndef BOARD_H_
#define BOARD_H_

#include <stm32f3xx.h>
#include "tim.h"
#include <math.h>

// Clock definitions
#define F_CPU									SystemCoreClock
#define BOARD_CLOCK_TIMER						htim6
#define BOARD_CLOCK_RESOLUTION_US				1
#define BOARD_CLOCK_OVERFLOW_US					1000

// A/D definitions
#define NUMBER_OF_ADS                           5
constexpr float Kadc =                          0.0008058608058608059; // (3.3 / (2^12 - 1));

// Encoder definitions
#define ENCODER_1								0
#define ENCODER_2								1
#define ENC1_CHA_EXTI_IRQ						EXTI4_IRQn
#define ENC1_CHB_EXTI_IRQ						EXTI9_5_IRQn
#define ENC2_CHA_EXTI_IRQ						EXTI15_10_IRQn
#define ENC2_CHB_EXTI_IRQ						EXTI15_10_IRQn
//constexpr uint32_t ENCODER_PREEMPT_PRIORITY =	0;
//constexpr uint32_t ENCODER_SUB_PRIORITY =		0;
//constexpr int32_t ENC_DEFAULT_TIMEOUT_MS =		100;
//constexpr int32_t ENC_DEFAULT_TIMEOUT_US =		1000 * ENC_DEFAULT_TIMEOUT_MS;

// PWM definitions
#define MOTOR_PWM_TIMER							(htim15)
#define MOTOR_R_PWM_CHANNEL                     TIM_CHANNEL_1
#define MOTOR_L_PWM_CHANNEL                     TIM_CHANNEL_2

#define BUZZER_PWM_TIMER                        htim17
#define BUZZER_PWM_CHANNEL                      TIM_CHANNEL_1

//constexpr float PWM_MIN_FREQUENCY =				0.02;
//constexpr float PWM_MAX_FREQUENCY =				720000;
//constexpr float PWM_MIN_DUTY =					0;
//constexpr float PWM_MAX_DUTY =					1;

// Communication definitions
//constexpr uint8_t BOARD_ID = 					0x40;
//constexpr uint8_t BROADCAST_ID = 				0xFE;
//constexpr uint8_t TIMEOUT_COMMUNICATION_MS =	2;
//constexpr uint16_t MEMORY_TABLE_SIZE =			128;


#endif /* BOARD_H_ */
