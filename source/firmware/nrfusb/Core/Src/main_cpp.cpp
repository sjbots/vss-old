#include "main_cpp.h"
#include "../../Drivers/manager/manager.h"
#include "../../Drivers/communication/messenger.h"

using namespace regs;

Manager manager;
messenger::Messenger usb_msgr;

void ISR_USB(uint8_t* buffer, uint32_t length)
{
    uint32_t i;
    for (i = 0; i < length; i++)
    {
        usb_msgr.isr_received_new_byte(buffer[i]);
    }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    manager.radio_is_asking_to_interrupt();
}

void main_cpp()
{
    radio = new RF24;
    HAL_Delay(100);
    radio->begin();

    if (radio->isChipConnected() == false)
    {
        led_rx_on();
        led_tx_on();

        bool is_connected = false;
        while (is_connected == false)
        {
            radio->begin();
            HAL_Delay(100);
            is_connected = radio->isChipConnected();
            HAL_Delay(900);
        }

        led_rx_off();
        led_tx_off();
    }

    radio->setPALevel(rf24_pa_dbm_e::RF24_PA_MAX);

    manager.messenger_begin(&usb_msgr);

    while (1)
    {
        manager.radio_isr();

        if ((usb_msgr.new_message) && (manager.is_radio_busy() == false))
        {
            if (usb_msgr.instruction_packet.is_for_me() == true)
            {
                manager.process_instruction_packet_for_me();
            }

            else if (usb_msgr.instruction_packet.is_for_broadcast() == true)
            {
//                manager.process_instruction_packet_for_broadcast();
            }

            else // if the message is not for me
            {
            }

            usb_msgr.instruction_packet_was_processed();
        }
    }
}


