#ifndef MANAGER_REGISTERS_H_
#define MANAGER_REGISTERS_H_

#include "register.h"

namespace regs {

extern Register<uint8_t>        STATUS;
extern Register<uint8_t>        POWER_CONFIG;
extern Register<uint8_t>        CHANNEL;
extern Register<uint8_t>        TRANSMISSION;
extern Register<uint8_t>        PAYLOAD_SIZE;
extern Register<uint8_t>        AUTO_ACK;
extern Register<uint8_t>        ADDRESS_WIDTH;
extern ArrayRegister<char>      PIPE0_ADDRESS;
extern ArrayRegister<char>      PIPE1_ADDRESS;
extern Register<char>           PIPE2_ADDRESS;
extern Register<char>           PIPE3_ADDRESS;
extern Register<char>           PIPE4_ADDRESS;
extern Register<char>           PIPE5_ADDRESS;
extern Register<uint8_t>        OPENED_PIPES;
extern Register<uint8_t>        SPI_SPEED;
extern Register<uint8_t>        WHO_AM_I;

extern Register<uint8_t>        RETRIES;
extern Register<uint8_t>        WRITE_AS;

extern ArrayRegister<uint8_t>   PIPE0_DATA;
extern ArrayRegister<uint8_t>   PIPE1_DATA;
extern ArrayRegister<uint8_t>   PIPE2_DATA;
extern ArrayRegister<uint8_t>   PIPE3_DATA;
extern ArrayRegister<uint8_t>   PIPE4_DATA;
extern ArrayRegister<uint8_t>   PIPE5_DATA;

namespace positions {

enum class STATUS: uint8_t {
    /**
     * This bit is set if the radio is connected to SPI bus
     */
    IS_CHIP_CONNECTED = 0,
    IS_CHIP_CONNECTED_MASK = (1 << IS_CHIP_CONNECTED),

    /**
     * This bit is set if the radio is nRF24L01+ (or compatible).
     */
    IS_P_VARIANT = 1,
    IS_P_VARIANT_MASK = (1 << IS_P_VARIANT),

    /**
     * This bit is set if the radio is legitimate.
     */
    IS_LEGITIMATE_RADIO = 2,
    IS_LEGITIMATE_RADIO_MASK = (1 << IS_LEGITIMATE_RADIO),

    /**
     * This bit is set if there was a carrier on the line for the
     * previous listening period. It is useful to check for
     * interference on the current channel. It is valid for any type
     * of NRF24L01.
     */
    CARRIER_ON_CURRENT_CHANNEL = 3,
    CARRIER_ON_CURRENT_CHANNEL_MASK = (1 << CARRIER_ON_CURRENT_CHANNEL),

    /**
     * This bit is set if a signal (carrier or otherwise) greater than or
     * equal to -64dBm is present on the channel. It is only valid for
     * nRF24L01P (+) hardware. It is useful to check for interference on
     * the current channel.
     */
    IS_CURRENT_CHANNEL_NOISY = 4,
    IS_CURRENT_CHANNEL_NOISY_MASK = (1 << IS_CURRENT_CHANNEL_NOISY),

    /**
     * This bit is set if the send was successful.
     */
    TX_OK = 5,
    TX_OK_MASK = (1 << TX_OK),

    /**
     * This bit is set if the send failed even with too many retries.
     */
    TX_FAIL = 6,
    TX_FAIL_MASK = (1 << TX_FAIL),
};

enum class POWER_CONFIG: uint8_t {
    /**
     * If this bit is set then the radio will enter in low-power mode.
     * To return to active mode write 0 to it.
     */
    LOW_POWER_MODE = 0,
    LOW_POWER_MODE_MASK = (1 << LOW_POWER_MODE),

    /**
     * These two bits set the Power Amplifier (PA) to one of the four
     * levels: MIN, LOW, HIGH or MAX.
     */
    PA_LEVEL = 1,
    PA_LEVEL_MASK = (0b11 << PA_LEVEL),

    /**
     * If this bit is set, then the radio is put in listening mode.
     */
    LISTENING = 3,
    LISTENING_MASK = (1 << LISTENING),
};

enum class TRANSMISSION: uint8_t {
    /**
     * These two bits specify the transmission data rate to one of
     * the following: 250 KBPS, 1 MBPS, 2 MBPS.
     */
    DATA_RATE = 0,
    DATA_RATE_MASK = (0b11 << DATA_RATE),

    ENABLE_DYNAMIC_PAYLOADS = 2,
    ENABLE_DYNAMIC_PAYLOADS_MASK = (1 << ENABLE_DYNAMIC_PAYLOADS),

    ENABLE_DYNAMIC_ACK = 3,
    ENABLE_DYNAMIC_ACK_MASK = (1 << ENABLE_DYNAMIC_ACK),

    ENABLE_ACK_PAYLOAD = 4,
    ENABLE_ACK_PAYLOAD_MASK = (1 << ENABLE_ACK_PAYLOAD),

    CRC_LENGTH = 5,
    CRC_LENGTH_MASK = (0b11 << CRC_LENGTH),
};

enum class AUTO_ACK: uint8_t {
    PIPE0 = 0,
    PIPE0_MASK = (1 << PIPE0),

    PIPE1 = 1,
    PIPE1_MASK = (1 << PIPE1),

    PIPE2 = 2,
    PIPE2_MASK = (1 << PIPE2),

    PIPE3 = 3,
    PIPE3_MASK = (1 << PIPE3),

    PIPE4 = 4,
    PIPE4_MASK = (1 << PIPE4),

    PIPE5 = 5,
    PIPE5_MASK = (1 << PIPE5),
};

enum class OPENED_PIPES: uint8_t {
    PIPE0_AS_WRITING = 0,
    PIPE0_AS_WRITING_MASK = (1 << PIPE0_AS_WRITING),

    PIPE0_AS_READING = 1,
    PIPE0_AS_READING_MASK = (1 << PIPE0_AS_READING),

    PIPE1 = 2,
    PIPE1_MASK = (1 << PIPE1),

    PIPE2 = 3,
    PIPE2_MASK = (1 << PIPE2),

    PIPE3 = 4,
    PIPE3_MASK = (1 << PIPE3),

    PIPE4 = 5,
    PIPE4_MASK = (1 << PIPE4),

    PIPE5 = 6,
    PIPE5_MASK = (1 << PIPE5),
};

enum class RETRIES: uint8_t {
    RETRIES = 0,
    RETRIES_MASK = 0x0F,
    DELAY = 4,
    DELAY_MASK = 0xF0,
};

enum class WRITE_AS: uint8_t {
    /**
     * If set this bit will send data stored in TX_DATA register.
     */
    SEND = 0,
    SEND_MASK = (1 << SEND),

    MULTICAST = 1,
    MULTICAST_MASK = (1 << MULTICAST),

    FAST = 2,
    FAST_MASK = (1 << FAST),

    BLOCKING = 3,
    BLOCKING_MASK = (1 << BLOCKING),

    WRITE_ACK_PAYLOAD = 4,
    WRITE_ACK_PAYLOAD_MASK = (1 << WRITE_ACK_PAYLOAD),

    FLUSH_TX = 5,
    FLUSH_TX_MASK = (1 << FLUSH_TX),

    HAS_TO_WRITE = SEND_MASK | WRITE_ACK_PAYLOAD_MASK | FLUSH_TX_MASK,
};

enum class SPI_SPEED: uint16_t {
    SPEED_18MBITS = 4,
    SPEED_9MBITS = 8,
    SPEED_4MBITS = 16,
    SPEED_2MBITS = 32,
    SPEED_1MBITS = 64,
    SPEED_562KBITS = 128,
    SPEED_281KBITS = 256,
};

}  // namespace registers
}  // namespace positions


#endif /* MANAGER_REGISTERS_H_ */
