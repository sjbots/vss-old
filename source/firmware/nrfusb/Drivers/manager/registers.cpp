#include "registers.h"

namespace regs {

Register<uint8_t>       STATUS                      (  0,    0, READ_ONLY);
Register<uint8_t>       POWER_CONFIG                (  1,    6, READ_WRITE);
Register<uint8_t>       CHANNEL                     (  2,   76, READ_WRITE);
Register<uint8_t>       TRANSMISSION                (  3,   64, READ_WRITE);
Register<uint8_t>       PAYLOAD_SIZE                (  4,   32, READ_WRITE);
Register<uint8_t>       AUTO_ACK                    (  5,    0, READ_WRITE);
Register<uint8_t>       ADDRESS_WIDTH               (  6,    5, READ_WRITE);

ArrayRegister<char>     PIPE0_ADDRESS               (  7,    5, READ_WRITE);
ArrayRegister<char>     PIPE1_ADDRESS               ( 12,    5, READ_WRITE);
Register<char>          PIPE2_ADDRESS               ( 17,    0, READ_WRITE);
Register<char>          PIPE3_ADDRESS               ( 18,    0, READ_WRITE);
Register<char>          PIPE4_ADDRESS               ( 19,    0, READ_WRITE);
Register<char>          PIPE5_ADDRESS               ( 20,    0, READ_WRITE);
Register<uint8_t>       OPENED_PIPES                ( 21,    0, READ_WRITE);
Register<uint8_t>       SPI_SPEED                   ( 22,    0, READ_WRITE);
Register<uint8_t>       WHO_AM_I                    ( 23,   65, READ_ONLY);

Register<uint8_t>       RETRIES                     ( 62,   95, READ_WRITE);
Register<uint8_t>       WRITE_AS                    ( 63,    0, READ_WRITE);
ArrayRegister<uint8_t>  PIPE0_DATA                  ( 64,   32, READ_WRITE);
ArrayRegister<uint8_t>  PIPE1_DATA                  ( 96,   32, READ_WRITE);
ArrayRegister<uint8_t>  PIPE2_DATA                  (128,   32, READ_WRITE);
ArrayRegister<uint8_t>  PIPE3_DATA                  (160,   32, READ_WRITE);
ArrayRegister<uint8_t>  PIPE4_DATA                  (192,   32, READ_WRITE);
ArrayRegister<uint8_t>  PIPE5_DATA                  (224,   32, READ_WRITE);

}  // namespace regs

