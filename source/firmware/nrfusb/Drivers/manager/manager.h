#ifndef MANAGER_MANAGER_H_
#define MANAGER_MANAGER_H_

#include <stdint.h>
#include "../../Drivers/manager/registers.h"
#include "hardware_handlers.h"
#include "../../Drivers/hid/led.h"
#include "../../Drivers/communication/messenger.h"

using namespace regs;

class Manager {
protected:
    void radio_rx_is_ready();
    volatile bool radio_is_busy;
    volatile bool radio_asked_to_interrupt;
    messenger::Messenger* usb_msgr;

    // Messenger API
    void update_register_status();
    void update_register_power_config(uint8_t new_config);
    void update_register_channel(uint8_t new_channel);
    void update_register_transmission(uint8_t new_config);
    void update_register_payload_size(uint8_t new_payload_size);
    void update_register_auto_ack(uint8_t new_config);
    void update_register_address_width(uint8_t new_address_width);
    void update_pipe_address(uint8_t pipe, char* new_address);
    void update_register_opened_pipes(uint8_t new_config);
    void update_register_retries(uint8_t new_config);
    void update_register_pipe0_data(uint8_t* values);
    void radio_write();

public:
    Manager();
    ~Manager();

    // Radio API
    void radio_is_asking_to_interrupt();
    void radio_isr();
    bool is_radio_busy();

    // Messenger API
    void messenger_begin(messenger::Messenger* msgr);
    void process_instruction_packet_for_me();
    void process_instruction_packet_for_broadcast();
    void deal_with_read_command();
    void deal_with_write_command();
    void make_response_for_ping();
    void make_response_for_read(uint8_t first_register_address, uint8_t length);
    void make_response_for_invalid_instruction();
    void make_response_for_write();
    void instruction_packet_was_processed();
};



#endif /* MANAGER_MANAGER_H_ */
