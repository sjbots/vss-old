#include "manager.h"

using namespace regs;

Manager::Manager()
{
}

Manager::~Manager()
{
}

void Manager::radio_rx_is_ready()
{
    // Is the device using dynamic payload size?
    uint8_t mask = static_cast<uint8_t>(positions::TRANSMISSION::ENABLE_DYNAMIC_PAYLOADS_MASK);
    bool using_dynamic_payload_size = (TRANSMISSION() & mask) > 0;

    uint8_t pipeNum;
    uint8_t* ptr;
    while (radio->available())
    {
        led_rx_on();

        if(radio->available(&pipeNum))
        {
            switch (pipeNum)
            {
            case 0:
                ptr = PIPE0_DATA.get_pointer();
                break;

            case 1:
                ptr = PIPE1_DATA.get_pointer();
                break;

            case 2:
                ptr = PIPE2_DATA.get_pointer();
                break;

            case 3:
                ptr = PIPE3_DATA.get_pointer();
                break;

            case 4:
                ptr = PIPE4_DATA.get_pointer();
                break;

            case 5:
                ptr = PIPE5_DATA.get_pointer();
                break;

            default:
                ptr = nullptr;
                break;
            }

            if (ptr != nullptr)
            {
                if (using_dynamic_payload_size > 0)
                {
                    uint8_t len = radio->getDynamicPayloadSize();
                    radio->read(ptr, len);
                }

                else // static payload size
                    radio->read(ptr, PAYLOAD_SIZE());
            }
        } // if(radio->available(&pipeNum))
    } // while (radio->available() == true)
    led_rx_off();
}

void Manager::radio_is_asking_to_interrupt()
{
    radio_asked_to_interrupt = true;
    radio_isr();
}

void Manager::radio_isr()
{
    if (radio_asked_to_interrupt == true)
    {
        if (radio_is_busy == false)
        {
            radio_is_busy = true;
            bool tx_ok, tx_fail, rx_ready;
            radio->whatHappened(tx_ok, tx_fail, rx_ready);

            if (tx_ok == true)
            {
                STATUS |= static_cast<uint8_t>(positions::STATUS::TX_OK_MASK);
            }
            else
            {
                STATUS &= ~static_cast<uint8_t>(positions::STATUS::TX_OK_MASK);
            }

            if (tx_fail == true)
            {
                STATUS |= static_cast<uint8_t>(positions::STATUS::TX_FAIL_MASK);
            }
            else
            {
                STATUS &= ~static_cast<uint8_t>(positions::STATUS::TX_FAIL_MASK);
            }

            if (rx_ready == true)
            {
                radio_rx_is_ready();
            }

            radio_asked_to_interrupt = false;
            radio_is_busy = false;
        }
    }
}

bool Manager::is_radio_busy()
{
    return radio_is_busy;
}

void Manager::messenger_begin(messenger::Messenger* msgr)
{
    usb_msgr = msgr;
}

void Manager::process_instruction_packet_for_me()
{
    if (usb_msgr->is_valid == true)
    {
        radio_is_busy = true;

        switch (usb_msgr->instruction_packet.get_command())
        {
        case protocol::Command::PING:
            make_response_for_ping();
            usb_msgr->send_packet(&usb_msgr->status_packet);
            break;

        case protocol::Command::READ:
            deal_with_read_command();
            usb_msgr->send_packet(&usb_msgr->status_packet);
            break;

        case protocol::Command::WRITE:
            deal_with_write_command();

            if ((WRITE_AS() & static_cast<uint8_t>(positions::WRITE_AS::HAS_TO_WRITE)) > 0)
            {
                led_tx_on();
                radio_write();
                led_tx_off();
            }

            make_response_for_write();
            usb_msgr->send_packet(&usb_msgr->status_packet);
            break;

        case protocol::Command::REBOOT:
            HAL_NVIC_SystemReset();
            break;

        default:
            break;
        }

        radio_is_busy = false;
    }

    else // if the message is not valid
    {
//        manager.make_response_packet_for_invalid_instruction_packet();
//        CDC_Transmit_FS(usb.status_packet., Len);
    }
}

void Manager::process_instruction_packet_for_broadcast()
{
    if (usb_msgr->is_valid == true)
    {
        switch (usb_msgr->instruction_packet.get_command())
        {
        case protocol::Command::WRITE:
            deal_with_write_command();
            // WRITE commands in broadcast do not generate status packet
            break;

        case protocol::Command::REBOOT:
            HAL_NVIC_SystemReset();
            break;

        default:
            break;
        }
    }
}

void Manager::deal_with_read_command()
{
    uint8_t first_register = usb_msgr->instruction_packet.get_parameter(0);
    uint8_t length = usb_msgr->instruction_packet.get_parameter(1);

    if (first_register == 0)
        update_register_status();

    make_response_for_read(first_register, length);
}

void Manager::deal_with_write_command()
{
    uint8_t* parameters = usb_msgr->instruction_packet.get_addr_of_parameters();
    uint8_t address = parameters[0];
    uint8_t* values = &parameters[1];

    if (address == POWER_CONFIG.get_addr())
    {
        update_register_power_config(values[0]);
    }

    else if (address == CHANNEL.get_addr())
    {
        update_register_channel(values[0]);
    }

    else if (address == TRANSMISSION.get_addr())
    {
        update_register_transmission(values[0]);
    }

    else if (address == PAYLOAD_SIZE.get_addr())
    {
        update_register_payload_size(values[0]);
    }

    else if (address == AUTO_ACK.get_addr())
    {
        update_register_auto_ack(values[0]);
    }

    else if (address == ADDRESS_WIDTH.get_addr())
    {
        update_register_address_width(values[0]);
    }

    else if (address == PIPE0_ADDRESS.get_addr(0))
    {
        update_pipe_address(0, (char*) values);
    }

    else if (address == PIPE1_ADDRESS.get_addr(0))
    {
        update_pipe_address(1, (char*) values);
    }

    else if (address == PIPE2_ADDRESS.get_addr())
    {
        update_pipe_address(2, (char*) values);
    }

    else if (address == PIPE3_ADDRESS.get_addr())
    {
        update_pipe_address(3, (char*) values);
    }

    else if (address == PIPE4_ADDRESS.get_addr())
    {
        update_pipe_address(4, (char*) values);
    }

    else if (address == PIPE5_ADDRESS.get_addr())
    {
        update_pipe_address(5, (char*) values);
    }

    else if (address == OPENED_PIPES.get_addr())
    {
        update_register_opened_pipes(values[0]);
    }

    else if (address == SPI_SPEED.get_addr())
    {
    }

    else if (address == WRITE_AS.get_addr())
    {
        // The writing commands are called after this function
        WRITE_AS = values[0];
    }

    else if (address == PIPE0_DATA.get_addr(0))
    {
        update_register_pipe0_data(values);
    }
}

void Manager::make_response_for_ping()
{
    usb_msgr->status_packet.create_new_msg();
    usb_msgr->status_packet.update_checksum_value();
}

void Manager::make_response_for_read(uint8_t first_register_address, uint8_t length)
{
    usb_msgr->status_packet.create_new_msg();
    uint16_t final_address = first_register_address + length;
    if (final_address > TOTAL_TABLE_SIZE)
        final_address = TOTAL_TABLE_SIZE;

    uint8_t address;
    for (address = first_register_address; address < final_address; address++)
    {
        usb_msgr->status_packet.append_parameter(_memory[address]);
    }

    usb_msgr->status_packet.update_checksum_value();
}

void Manager::make_response_for_invalid_instruction()
{
    usb_msgr->status_packet.create_new_msg();

    if (!usb_msgr->instruction_packet.is_checksum_valid())
        usb_msgr->status_packet.append_error(protocol::Error::INVALID_CHECKSUM);

    if (!usb_msgr->instruction_packet.is_command_valid())
        usb_msgr->status_packet.append_error(protocol::Error::INVALID_INSTRUCTION);

    usb_msgr->status_packet.update_checksum_value();
}

void Manager::make_response_for_write()
{
    usb_msgr->status_packet.create_new_msg();
    usb_msgr->status_packet.update_checksum_value();
}

void Manager::update_register_status()
{
    bool is_chip_connected = radio->isChipConnected();
    bool is_p_variant = radio->isPVariant();
    bool is_legitimate_radio = radio->isValid();
    bool carrier_on_current_channel = radio->testCarrier();
    bool is_current_channel_noisy = radio->testRPD();

    if (is_chip_connected == true)
        STATUS |= static_cast<uint8_t>(positions::STATUS::IS_CHIP_CONNECTED_MASK);
    else
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::IS_CHIP_CONNECTED_MASK);

    if (is_p_variant == true)
        STATUS |= static_cast<uint8_t>(positions::STATUS::IS_P_VARIANT_MASK);
    else
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::IS_P_VARIANT_MASK);

    if (is_legitimate_radio == true)
        STATUS |= static_cast<uint8_t>(positions::STATUS::IS_LEGITIMATE_RADIO_MASK);
    else
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::IS_LEGITIMATE_RADIO_MASK);

    if (carrier_on_current_channel == true)
        STATUS |= static_cast<uint8_t>(positions::STATUS::CARRIER_ON_CURRENT_CHANNEL_MASK);
    else
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::CARRIER_ON_CURRENT_CHANNEL_MASK);

    if (is_current_channel_noisy == true)
        STATUS |= static_cast<uint8_t>(positions::STATUS::IS_CURRENT_CHANNEL_NOISY_MASK);
    else
        STATUS &= ~static_cast<uint8_t>(positions::STATUS::IS_CURRENT_CHANNEL_NOISY_MASK);
}

void Manager::update_register_power_config(uint8_t new_config)
{
    uint8_t mask, pos, actual_config_masked, new_config_masked;
    bool changed;

    // LOW_POWER_MODE
    mask = static_cast<uint8_t>(positions::POWER_CONFIG::LOW_POWER_MODE_MASK);
    actual_config_masked = POWER_CONFIG() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->powerDown();
            POWER_CONFIG |= mask;
        }
        else
        {
            radio->powerUp();
            POWER_CONFIG &= ~mask;
        }
    }

    // PA_LEVEL
    mask = static_cast<uint8_t>(positions::POWER_CONFIG::PA_LEVEL_MASK);
    pos = static_cast<uint8_t>(positions::POWER_CONFIG::PA_LEVEL);
    actual_config_masked = POWER_CONFIG() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        uint8_t new_pa = (new_config_masked >> pos);

        // Verify if the new PA_LEVEL is valid
        if (new_pa < static_cast<uint8_t>(rf24_pa_dbm_e::RF24_PA_ERROR))
        {
            radio->setPALevel(new_pa);
            POWER_CONFIG &= ~mask;
            POWER_CONFIG |= new_config_masked;
        }
    }

    // LISTENING
    mask = static_cast<uint8_t>(positions::POWER_CONFIG::LISTENING_MASK);
    actual_config_masked = POWER_CONFIG() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->startListening();
            POWER_CONFIG |= mask;
        }
        else
        {
            radio->stopListening();
            POWER_CONFIG &= ~mask;
        }
    }
}

void Manager::update_register_channel(uint8_t new_channel)
{
    if ((new_channel >= 0) && (new_channel <= 125))
    {
        radio->setChannel(new_channel);
        CHANNEL = new_channel;
    }
}

void Manager::update_register_transmission(uint8_t new_config)
{
    uint8_t mask, pos, actual_config_masked, new_config_masked;
    bool changed;

    // DATA_RATE
    mask = static_cast<uint8_t>(positions::TRANSMISSION::DATA_RATE_MASK);
    pos = static_cast<uint8_t>(positions::TRANSMISSION::DATA_RATE);
    actual_config_masked = TRANSMISSION() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        rf24_datarate_e new_dr = (rf24_datarate_e) (new_config_masked >> pos);

        // Verify if the new DATA_RATE is valid
        if ((new_dr == RF24_250KBPS) || (new_dr == RF24_1MBPS) || (new_dr == RF24_2MBPS))
        {
            radio->setDataRate(new_dr);
            TRANSMISSION &= ~mask;
            TRANSMISSION |= new_config_masked;
        }
    }

    // ENABLE_DYNAMIC_PAYLOADS
    mask = static_cast<uint8_t>(positions::TRANSMISSION::ENABLE_DYNAMIC_PAYLOADS_MASK);
    actual_config_masked = TRANSMISSION() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->enableDynamicPayloads();
            TRANSMISSION |= mask;
        }

        else
        {
            radio->disableDynamicPayloads();
            TRANSMISSION &= ~mask;
        }
    }

    // ENABLE_DYNAMIC_ACK
    mask = static_cast<uint8_t>(positions::TRANSMISSION::ENABLE_DYNAMIC_ACK_MASK);
    actual_config_masked = TRANSMISSION() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->enableDynamicAck();
            TRANSMISSION |= mask;
        }
    }

    // ENABLE_ACK_PAYLOAD
    mask = static_cast<uint8_t>(positions::TRANSMISSION::ENABLE_ACK_PAYLOAD_MASK);
    actual_config_masked = TRANSMISSION() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->enableAckPayload();
            TRANSMISSION |= mask;
        }
    }

    // CRC_LENGTH
    mask = static_cast<uint8_t>(positions::TRANSMISSION::CRC_LENGTH_MASK);
    pos = static_cast<uint8_t>(positions::TRANSMISSION::CRC_LENGTH);
    actual_config_masked = TRANSMISSION() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        rf24_crclength_e new_crc = (rf24_crclength_e) (new_config_masked >> pos);

        // Verify if the new CRC length is valid
        if (new_crc == RF24_CRC_DISABLED)
        {
            radio->disableCRC();
            TRANSMISSION &= ~mask;
            TRANSMISSION |= new_config_masked;
        }

        else if ((new_crc == RF24_CRC_8) || (new_crc == RF24_CRC_8))
        {
            radio->setCRCLength(new_crc);
            TRANSMISSION &= ~mask;
            TRANSMISSION |= new_config_masked;
        }
    }
}

void Manager::update_register_payload_size(uint8_t new_payload_size)
{
    if ((new_payload_size >= 0) && (new_payload_size <= 32))
    {
        radio->setPayloadSize(new_payload_size);
        PAYLOAD_SIZE = new_payload_size;
    }
}

void Manager::update_register_address_width(uint8_t new_address_width)
{
    if (new_address_width >= 3 || new_address_width <= 5)
    {
        radio->setAddressWidth(new_address_width);
    }
}

void Manager::update_register_auto_ack(uint8_t new_config)
{
    uint8_t mask, pos, actual_config_masked, new_config_masked;
    bool changed;

    // PIPE0
    mask = static_cast<uint8_t>(positions::AUTO_ACK::PIPE0_MASK);
    pos = static_cast<uint8_t>(positions::AUTO_ACK::PIPE0);
    actual_config_masked = AUTO_ACK() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->setAutoAck(pos, true);
            AUTO_ACK |= mask;
        }

        else
        {
            radio->setAutoAck(pos, false);
            AUTO_ACK &= ~mask;
        }
    }

    // PIPE1
    mask = static_cast<uint8_t>(positions::AUTO_ACK::PIPE1_MASK);
    pos = static_cast<uint8_t>(positions::AUTO_ACK::PIPE1);
    actual_config_masked = AUTO_ACK() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->setAutoAck(pos, true);
            AUTO_ACK |= mask;
        }

        else
        {
            radio->setAutoAck(pos, false);
            AUTO_ACK &= ~mask;
        }
    }

    // PIPE2
    mask = static_cast<uint8_t>(positions::AUTO_ACK::PIPE2_MASK);
    pos = static_cast<uint8_t>(positions::AUTO_ACK::PIPE2);
    actual_config_masked = AUTO_ACK() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->setAutoAck(pos, true);
            AUTO_ACK |= mask;
        }

        else
        {
            radio->setAutoAck(pos, false);
            AUTO_ACK &= ~mask;
        }
    }

    // PIPE3
    mask = static_cast<uint8_t>(positions::AUTO_ACK::PIPE3_MASK);
    pos = static_cast<uint8_t>(positions::AUTO_ACK::PIPE3);
    actual_config_masked = AUTO_ACK() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->setAutoAck(pos, true);
            AUTO_ACK |= mask;
        }

        else
        {
            radio->setAutoAck(pos, false);
            AUTO_ACK &= ~mask;
        }
    }

    // PIPE4
    mask = static_cast<uint8_t>(positions::AUTO_ACK::PIPE4_MASK);
    pos = static_cast<uint8_t>(positions::AUTO_ACK::PIPE4);
    actual_config_masked = AUTO_ACK() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->setAutoAck(pos, true);
            AUTO_ACK |= mask;
        }

        else
        {
            radio->setAutoAck(pos, false);
            AUTO_ACK &= ~mask;
        }
    }

    // PIPE5
    mask = static_cast<uint8_t>(positions::AUTO_ACK::PIPE5_MASK);
    pos = static_cast<uint8_t>(positions::AUTO_ACK::PIPE5);
    actual_config_masked = AUTO_ACK() & mask;
    new_config_masked = new_config & mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->setAutoAck(pos, true);
            AUTO_ACK |= mask;
        }

        else
        {
            radio->setAutoAck(pos, false);
            AUTO_ACK &= ~mask;
        }
    }
}

void Manager::update_register_opened_pipes(uint8_t new_config)
{
    // Auxiliary variables
    uint8_t pipe_mask, actual_config_masked, new_config_masked;
    uint8_t pipe0W_mask = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE0_AS_WRITING_MASK);
    uint8_t pipe0R_mask = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE0_AS_READING_MASK);
    char aux_address[5];  // for use in PIPES 2...5
    aux_address[1] = PIPE1_ADDRESS.read(1);
    aux_address[2] = PIPE1_ADDRESS.read(2);
    aux_address[3] = PIPE1_ADDRESS.read(3);
    aux_address[4] = PIPE1_ADDRESS.read(4);
    bool changed;

    // PIPE0
    pipe_mask = pipe0W_mask | pipe0R_mask;
    actual_config_masked = OPENED_PIPES() & pipe_mask;
    new_config_masked = new_config & pipe_mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        // Verify if new config is valid, because can not use this
        // pipe as writing and reading
        if ((new_config & pipe_mask) != pipe_mask)
        {
            // Deal with pipe0 as reading
            pipe_mask = pipe0R_mask;
            actual_config_masked = OPENED_PIPES() & pipe_mask;
            new_config_masked = new_config & pipe_mask;
            changed = (actual_config_masked != new_config_masked);
            if (changed == true)
            {
                if (new_config_masked > 0)
                {
                    radio->openReadingPipe(0, (uint8_t*) PIPE0_ADDRESS.get_pointer());
                    OPENED_PIPES |= pipe_mask;
                }

                else
                {
                    radio->closeReadingPipe(0);
                    OPENED_PIPES &= ~pipe_mask;
                }
            }

            // Deal with pipe0 as writing
            pipe_mask = pipe0W_mask;
            actual_config_masked = OPENED_PIPES() & pipe_mask;
            new_config_masked = new_config & pipe_mask;
            changed = (actual_config_masked != new_config_masked);
            if (changed == true)
            {
                if (new_config_masked > 0)
                {
                    radio->openWritingPipe((uint8_t*) PIPE0_ADDRESS.get_pointer());
                    OPENED_PIPES |= pipe_mask;
                }
            }
        }
    }

    // PIPE1
    pipe_mask = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE1_MASK);
    actual_config_masked = OPENED_PIPES() & pipe_mask;
    new_config_masked = new_config & pipe_mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            radio->openReadingPipe(1, (uint8_t*) PIPE1_ADDRESS.get_pointer());
            OPENED_PIPES |= pipe_mask;
        }

        else
        {
            radio->closeReadingPipe(1);
            OPENED_PIPES &= ~pipe_mask;
        }
    }

    // PIPE2
    pipe_mask = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE2_MASK);
    actual_config_masked = OPENED_PIPES() & pipe_mask;
    new_config_masked = new_config & pipe_mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            aux_address[0] = PIPE2_ADDRESS();
            radio->openReadingPipe(2, (uint8_t*) aux_address);
            OPENED_PIPES |= pipe_mask;
        }

        else
        {
            radio->closeReadingPipe(2);
            OPENED_PIPES &= ~pipe_mask;
        }
    }

    // PIPE3
    pipe_mask = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE3_MASK);
    actual_config_masked = OPENED_PIPES() & pipe_mask;
    new_config_masked = new_config & pipe_mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            aux_address[0] = PIPE3_ADDRESS();
            radio->openReadingPipe(3, (uint8_t*) aux_address);
            OPENED_PIPES |= pipe_mask;
        }

        else
        {
            radio->closeReadingPipe(3);
            OPENED_PIPES &= ~pipe_mask;
        }
    }

    // PIPE4
    pipe_mask = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE4_MASK);
    actual_config_masked = OPENED_PIPES() & pipe_mask;
    new_config_masked = new_config & pipe_mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            aux_address[0] = PIPE4_ADDRESS();
            radio->openReadingPipe(4, (uint8_t*) aux_address);
            OPENED_PIPES |= pipe_mask;
        }

        else
        {
            radio->closeReadingPipe(4);
            OPENED_PIPES &= ~pipe_mask;
        }
    }

    // PIPE5
    pipe_mask = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE5_MASK);
    actual_config_masked = OPENED_PIPES() & pipe_mask;
    new_config_masked = new_config & pipe_mask;
    changed = (actual_config_masked != new_config_masked);
    if (changed == true)
    {
        if (new_config_masked > 0)
        {
            aux_address[0] = PIPE5_ADDRESS();
            radio->openReadingPipe(5, (uint8_t*) aux_address);
            OPENED_PIPES |= pipe_mask;
        }

        else
        {
            radio->closeReadingPipe(5);
            OPENED_PIPES &= ~pipe_mask;
        }
    }
}

void Manager::update_pipe_address(uint8_t pipe, char* new_address)
{
    uint8_t i;
    if (pipe == 0)
    {
        for (i = 0; i < 5; i++)
        {
            PIPE0_ADDRESS.write(i, new_address[i]);
        }
    }

    else if (pipe == 1)
    {
        for (i = 0; i < 5; i++)
        {
            PIPE1_ADDRESS.write(i, new_address[i]);
        }
    }

    else if (pipe == 2)
    {
        for (i = 0; i < 5; i++)
        {
            PIPE2_ADDRESS = new_address[0];
        }
    }

    else if (pipe == 3)
    {
        for (i = 0; i < 5; i++)
        {
            PIPE3_ADDRESS = new_address[0];
        }
    }

    else if (pipe == 4)
    {
        for (i = 0; i < 5; i++)
        {
            PIPE4_ADDRESS = new_address[0];
        }
    }

    else if (pipe == 5)
    {
        for (i = 0; i < 5; i++)
        {
            PIPE5_ADDRESS = new_address[0];
        }
    }
}

void Manager::update_register_retries(uint8_t new_config)
{
    uint8_t mask, pos, new_config_masked;
    uint8_t new_delay, new_count;

    if (new_config != RETRIES())
    {
        mask = static_cast<uint8_t>(positions::RETRIES::RETRIES_MASK);
        pos = static_cast<uint8_t>(positions::RETRIES::RETRIES);
        new_config_masked = new_config & mask;
        new_count = (new_config_masked >> pos);

        mask = static_cast<uint8_t>(positions::RETRIES::DELAY_MASK);
        pos = static_cast<uint8_t>(positions::RETRIES::DELAY);
        new_config_masked = new_config & mask;
        new_delay = (new_config_masked >> pos);

        radio->setRetries(new_delay, new_count);
        RETRIES = new_config;
    }
}

void Manager::update_register_pipe0_data(uint8_t* values)
{
    // it is assumed that always 32 bytes were received from USB
    uint8_t i;
    for (i = 0; i < 32; i++)
    {
        PIPE0_DATA.write(i, values[i]);
    }
}

void Manager::radio_write()
{
    bool multicast;
    if ((WRITE_AS() & static_cast<uint8_t>(positions::WRITE_AS::MULTICAST_MASK)) > 0)
        multicast = true;
    else
        multicast = false;

    if ((WRITE_AS() & static_cast<uint8_t>(positions::WRITE_AS::SEND_MASK)) > 0)
    {
        constexpr uint8_t pipe0W = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE0_AS_WRITING_MASK);
        constexpr uint8_t pipe0R = static_cast<uint8_t>(positions::OPENED_PIPES::PIPE0_AS_READING_MASK);
        if ((OPENED_PIPES() & pipe0W) == 0)
        {
            if ((OPENED_PIPES() & pipe0R) > 0)
            {
                radio->closeReadingPipe(0);
            }

            radio->openWritingPipe((uint8_t*) PIPE0_ADDRESS.get_pointer());
        }

        if ((WRITE_AS() & static_cast<uint8_t>(positions::WRITE_AS::FAST_MASK)) > 0)
        {
            radio->writeFast(PIPE0_DATA.get_pointer(), PAYLOAD_SIZE(), multicast);
        }

        else if ((WRITE_AS() & static_cast<uint8_t>(positions::WRITE_AS::BLOCKING_MASK)) > 0)
        {
            radio->writeBlocking(PIPE0_DATA.get_pointer(), PAYLOAD_SIZE(), 10);
        }

        else
        {
            radio->write(PIPE0_DATA.get_pointer(), PAYLOAD_SIZE(), multicast);
        }
    }

    else if ((WRITE_AS() & static_cast<uint8_t>(positions::WRITE_AS::WRITE_ACK_PAYLOAD_MASK)) > 0)
    {
        // TODO: add support to it
//        radio->writeAckPayload(pipe, buf, len);
    }

    else if ((WRITE_AS() & static_cast<uint8_t>(positions::WRITE_AS::FLUSH_TX_MASK)) > 0)
    {
        radio->flush_tx();
    }

    WRITE_AS = 0;
}

