#ifndef MANAGER_REGISTERS_DEFS_H_
#define MANAGER_REGISTERS_DEFS_H_

namespace regs {

enum access_t {
    READ_ONLY = 0,
    READ_WRITE,
};

constexpr uint16_t TOTAL_TABLE_SIZE = 256;

} // namespace regs

#endif /* MANAGER_REGISTERS_DEFS_H_ */
