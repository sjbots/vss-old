#ifndef MANAGER_REGISTER_H_
#define MANAGER_REGISTER_H_

#include <stdint.h>
#include "registers_defs.h"

namespace regs {

extern volatile uint8_t _memory[TOTAL_TABLE_SIZE];

class AbstractRegister {
protected:
    const uint16_t address = 0;
    const uint8_t access = READ_ONLY;
    AbstractRegister(uint16_t addr, uint16_t writable) :
        address(addr), access(writable) {}

public:
    virtual ~AbstractRegister(){}

    virtual const uint16_t get_addr() {
        return address;
    }

    bool is_writable()
    {
        return access == READ_WRITE;
    }
};


template <typename T>
class Register: public AbstractRegister {
protected:
    T* value;

public:
    Register(uint16_t addr, const T initial_value, bool writable) :
        AbstractRegister(addr, writable)
    {
//        address = addr;
        value = (T*) &_memory[address];
        *value = initial_value;
//        access = writable;
    }

    T read()
    {
        return *value;
    }

    T* get_pointer()
    {
        return value;
    }

    const uint16_t get_addr() {
        return address;
    }

    void write(T new_value)
    {
        *value = new_value;
    }

    void set_bit(T what_bit)
    {
        write(*value | (1 << what_bit));
    }

    void clear_bit(T what_bit)
    {
        write(*value & ~(1 << what_bit));
    }

    void operator=(const T new_value)
    {
        write(new_value);
    }

    T operator()()
    {
        return read();
    }

    void operator|=(T new_mask)
    {
        write(*value | new_mask);
    }

    void operator&=(T new_mask)
    {
        write(*value & new_mask);
    }

    uint16_t get_total_size()
    {
        return sizeof(T);
    }
};


template <typename T>
class ArrayRegister: public AbstractRegister {
protected:
    T* value;
    uint8_t number_of_elements;

public:
    ArrayRegister(uint16_t addr, const uint16_t length, bool writable) :
        AbstractRegister(addr, writable)
    {
//        address = addr;
        value = (T*) &_memory[address];
        number_of_elements = length;
//        access = writable;
    }

    ~ArrayRegister()
    {
        delete value;
    }

    T read(uint16_t position)
    {
        return value[position];
    }

    T* get_pointer()
    {
        return value;
    }

    void write(uint16_t position, T new_value)
    {
        value[position] = new_value;
    }

    uint16_t get_addr(uint16_t position)
    {
        return address + position;
    }

    uint8_t get_number_of_elements()
    {
        return number_of_elements;
    }

    uint16_t get_total_size()
    {
        return number_of_elements * sizeof(T);
    }
};

} // namespace registers



#endif /* MANAGER_REGISTER_H_ */
