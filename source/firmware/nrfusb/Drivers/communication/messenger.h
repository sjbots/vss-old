#ifndef COMMUNICATION_MESSENGER_H_
#define COMMUNICATION_MESSENGER_H_

#include <stdint.h>
#include "../../USB_DEVICE/App/usbd_cdc_if.h"
#include "protocol.h"

namespace messenger {

    enum class StateReceivingMsg : uint8_t {
        LOOKING_FOR_HEADER_1 = 0,
        LOOKING_FOR_HEADER_2,
        LOOKING_FOR_ID,
        LOOKING_FOR_LENGTH,
        LOOKING_FOR_CMD,
        RECEIVING_MSG,
        LOOKING_FOR_CHECKSUM,
    };

    class Messenger
    {
        private:
            StateReceivingMsg state_receiving_msg;

            const uint8_t timeout_max = TIMEOUT_COMMUNICATION_MS;
            uint32_t time_received_header_1;
            uint8_t n_parameters_received;

        public:
            protocol::InstructionPacket instruction_packet;
            protocol::StatusPacket status_packet;

            // Properties of the received message
            volatile bool new_message;
            bool is_valid;
            uint8_t errors;

            Messenger();

            void isr_received_new_byte(uint8_t new_byte);

            void instruction_packet_was_processed();

            void send_packet(protocol::Message* packet);
    };

}

#endif /* COMMUNICATION_MESSENGER_H_ */
