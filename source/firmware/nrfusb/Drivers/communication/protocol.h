#ifndef COMMUNICATION_PROTOCOL_H_
#define COMMUNICATION_PROTOCOL_H_

/**
  ******************************************************************************
  * @file    protocol.h
  * @author  José Roberto Colombo Junior
  * @brief   Header file of the communication protocol used
  ******************************************************************************
  * @attention
  *
  * This is free software licensed under GPL.
  *
  ******************************************************************************
  */

#include <stdint.h>
#include "../board.h"

/**
 * @defgroup communication Communication
 *
 * The board is controlled from the computer. The communication protocol is
 * based on the DXL Version 1 protocol.
 *
 */

/**@{*/

/**
 * @brief Defines the protocol namespace
 */
namespace protocol
{
    constexpr uint16_t MSG_SIZE = 256;          ///< It is reserved 32 bytes of RAM for every message

    /**
     * @brief Field positions of the message
     */
    enum class Field : uint8_t {
        HEADER_1 = 0,                           ///< Header 1 field position
        HEADER_2 = 1,                           ///< Header 2 field position
        ID = 2,                                 ///< ID field position
        LENGTH = 3,                             ///< Length field position
        COMMAND = 4,                            ///< Command field position
        ERROR = 4,                              ///< Error field position
        PARAMETER_FIRST = 5,                    ///< First parameter field position
    };

    // Default values of the fields
    constexpr uint8_t HEADER_1_VALUE = 0xFF;    ///< Default HEADER 1 value
    constexpr uint8_t HEADER_2_VALUE = 0xFF;    ///< Default HEADER 2 value

    /**
     * @brief Types of commands
     */
    enum class Command : uint8_t {
        PING = 0x01,                            ///< PING command (verify if the device is working)
        READ = 0x02,
        WRITE = 0x03,
        REG_WRITE = 0x04,
        ACTION = 0x05,
        FACTORY_RESET = 0x06,
        REBOOT = 0x08,
        SYNC_WRITE = 0x83,
        BULK_READ = 0x92,
    };

    // Types of errors
    enum class Error : uint8_t {
        INVALID_INSTRUCTION = 1 << 6,
        OVERLOAD_ERROR = 1 << 5,
        INVALID_CHECKSUM = 1 << 4,
        RANGE_ERROR = 1 << 3,
        OVERHEATING_ERROR = 1 << 2,
        ANGLE_LIMIT_ERROR = 1 << 1,
        INPUT_VOLTAGE_ERROR = 1 << 0,
    };

    class Message
    {
    private:
    protected:
        uint8_t data[MSG_SIZE];                 ///< Message reserved RAM

        // Pointers to parameters and checksum
        uint8_t* parameters;                    ///< Pointer to parameters field
        uint8_t* checksum;                      ///< Pointer to checksum field

        void set_field(Field what_field, uint8_t new_value);
        uint8_t get_field(Field what_field);

    public:
        // Constructor
        Message();

        /**
         * @brief Sets the given value to the header 1 field
         *
         * @param new_value is the the value to be written in the header 1 field
         */
        void set_header_1(uint8_t new_value);
        void set_header_2(uint8_t new_value);
        void set_id(uint8_t new_value);
        void set_command(uint8_t new_value);
        void set_length(uint8_t new_value);
        void append_parameter(uint8_t value);
        void insert_parameter(uint8_t index, uint8_t value);
        void set_checksum(uint8_t new_value);
        void update_checksum_value();

        uint8_t get_parameter(uint8_t what_parameter);
        uint8_t* get_addr_of_parameters();
        uint8_t get_number_of_parameters();
        uint8_t get_id();
        uint8_t get_length();
        uint8_t get_checksum();
        uint8_t* get_addr_of_packet();
        uint8_t get_total_packet_size();

        void clean_message();

        void update_checksum_position();
        uint8_t calculate_checksum();
        bool is_checksum_valid();
        bool is_for_me();
        bool is_for_broadcast();
        virtual bool is_valid();

    };

    class InstructionPacket : public Message
    {
        private:
        public:
            // Constructor
            InstructionPacket();

            /**
             * @brief Verify if the received command is valid
             *
             * Only the supported commands will be considered valid.
             */
            bool is_command_valid();
            bool is_this_command_valid(uint8_t command);
            Command get_command();

            bool is_valid();
    };

    class StatusPacket : public Message
    {
        private:
        public:
            // Constructor
            StatusPacket();

            bool is_valid();

            void create_new_msg();

            void append_error(Error what_error);
            void clear_error(Error what_error);
            void clear_errors();
    };

}

/**@}*/

#endif /* COMMUNICATION_PROTOCOL_H_ */
