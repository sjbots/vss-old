#ifndef BOARD_H_
#define BOARD_H_


// Communication definitions
constexpr uint8_t BOARD_ID =                    0x41;
constexpr uint8_t BROADCAST_ID =                0xFE;
constexpr uint8_t TIMEOUT_COMMUNICATION_MS =    2;


#endif /* BOARD_H_ */
