#ifndef HID_LED_H_
#define HID_LED_H_

#include "main.h"
//#include "stm32f1xx_hal_gpio.h"

extern void led_rx_on();
extern void led_rx_off();
extern void led_tx_on();
extern void led_tx_off();

#endif /* HID_LED_H_ */
