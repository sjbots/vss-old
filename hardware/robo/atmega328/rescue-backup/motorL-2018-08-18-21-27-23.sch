EESchema Schematic File Version 2
LIBS:vss-rescue
LIBS:Regulators
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:RF_OEM
LIBS:boosterpack
LIBS:DRV8872
LIBS:nRF24L01
LIBS:stm32f303k8
LIBS:H-Bridge
LIBS:MC34063
LIBS:switches
LIBS:atmega328p-dip
LIBS:vss-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date "2016-11-27"
Rev "José Roberto"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X06 P1
U 1 1 58972BB0
P 8700 2450
F 0 "P1" H 8700 2800 50  0000 C CNN
F 1 "MOTOR_L" V 8800 2450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 8700 2450 50  0001 C CNN
F 3 "" H 8700 2450 50  0000 C CNN
	1    8700 2450
	1    0    0    -1  
$EndComp
Text Notes 8150 2500 0    60   ~ 0
VCC
Text Notes 8150 2600 0    60   ~ 0
M1
Text Notes 8150 2700 0    60   ~ 0
M2
Text Notes 8150 2400 0    60   ~ 0
OUT A
Text Notes 8150 2300 0    60   ~ 0
OUT B
Text Notes 8150 2200 0    60   ~ 0
GND
$Comp
L GND #PWR17
U 1 1 58972BC0
P 8100 2200
F 0 "#PWR17" H 8100 1950 50  0001 C CNN
F 1 "GND" H 8100 2050 50  0000 C CNN
F 2 "" H 8100 2200 50  0000 C CNN
F 3 "" H 8100 2200 50  0000 C CNN
	1    8100 2200
	0    1    1    0   
$EndComp
Text Notes 8000 2950 0    60   ~ 0
  POLOLU\nCONVENTION
Text HLabel 8050 1700 2    60   Output ~ 0
ENCODER_1
Text HLabel 8050 1850 2    60   Output ~ 0
ENCODER_2
$Comp
L GND #PWR24
U 1 1 5925AE8B
P 5600 3850
F 0 "#PWR24" H 5600 3600 50  0001 C CNN
F 1 "GND" H 5600 3700 50  0000 C CNN
F 2 "" H 5600 3850 50  0000 C CNN
F 3 "" H 5600 3850 50  0000 C CNN
	1    5600 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR22
U 1 1 5925AF73
P 2000 3100
F 0 "#PWR22" H 2000 2850 50  0001 C CNN
F 1 "GND" H 2000 2950 50  0000 C CNN
F 2 "" H 2000 3100 50  0000 C CNN
F 3 "" H 2000 3100 50  0000 C CNN
	1    2000 3100
	1    0    0    -1  
$EndComp
$Comp
L +BATT #PWR18
U 1 1 5925C713
P 2000 2400
F 0 "#PWR18" H 2000 2250 50  0001 C CNN
F 1 "+BATT" H 2000 2540 50  0000 C CNN
F 2 "" H 2000 2400 50  0001 C CNN
F 3 "" H 2000 2400 50  0001 C CNN
	1    2000 2400
	1    0    0    -1  
$EndComp
Text Notes 1400 3400 0    60   ~ 0
L298 BYPASS CAPACITORS
Text Notes 8650 1000 0    60   ~ 0
REQUISITO DE VELOCIDADE É 2m/s
$Comp
L +5V #PWR19
U 1 1 5932BC49
P 7500 2400
F 0 "#PWR19" H 7500 2250 50  0001 C CNN
F 1 "+5V" H 7500 2540 50  0000 C CNN
F 2 "" H 7500 2400 50  0001 C CNN
F 3 "" H 7500 2400 50  0001 C CNN
	1    7500 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2500 8500 2500
Wire Wire Line
	7650 2400 8500 2400
Wire Wire Line
	7800 2300 8500 2300
Wire Wire Line
	8500 2200 8100 2200
Wire Wire Line
	7650 1700 7650 2400
Wire Wire Line
	7800 1850 7800 2300
Wire Notes Line
	8100 2100 8100 2750
Wire Notes Line
	8100 2750 8450 2750
Wire Notes Line
	8450 2750 8450 2100
Wire Notes Line
	8450 2100 8100 2100
Wire Wire Line
	7800 1850 8050 1850
Wire Wire Line
	8050 1700 7650 1700
Wire Wire Line
	7500 2500 7500 2400
Wire Wire Line
	1750 2500 1750 2600
Wire Wire Line
	8500 2600 7350 2600
Wire Wire Line
	7350 2700 8500 2700
Wire Wire Line
	5350 3500 5850 3500
Wire Wire Line
	5850 3500 5850 3450
Wire Wire Line
	5700 3450 5700 3500
Connection ~ 5700 3500
Wire Wire Line
	5500 3450 5500 3500
Wire Wire Line
	5600 3500 5600 3850
Connection ~ 5600 3500
Wire Wire Line
	5350 3500 5350 3450
Connection ~ 5500 3500
Wire Wire Line
	7350 2800 7350 2700
Wire Wire Line
	7350 2600 7350 2500
Wire Wire Line
	7350 2500 6300 2500
Wire Wire Line
	6300 2800 7350 2800
$Comp
L +BATT #PWR16
U 1 1 59611C8E
P 4750 2100
F 0 "#PWR16" H 4750 1950 50  0001 C CNN
F 1 "+BATT" H 4750 2240 50  0000 C CNN
F 2 "" H 4750 2100 50  0001 C CNN
F 3 "" H 4750 2100 50  0001 C CNN
	1    4750 2100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR15
U 1 1 59611CB3
P 4450 2100
F 0 "#PWR15" H 4450 1950 50  0001 C CNN
F 1 "+5V" H 4450 2240 50  0000 C CNN
F 2 "" H 4450 2100 50  0001 C CNN
F 3 "" H 4450 2100 50  0001 C CNN
	1    4450 2100
	1    0    0    -1  
$EndComp
Text HLabel 4750 2500 0    60   Input ~ 0
PWM_1
Text HLabel 4750 2800 0    60   Input ~ 0
PWM_2
Wire Wire Line
	4950 2200 4750 2200
Wire Wire Line
	4750 2200 4750 2100
Wire Wire Line
	4450 2350 4950 2350
Wire Wire Line
	4450 2350 4450 2100
Wire Wire Line
	4750 2500 4950 2500
Wire Wire Line
	4950 2800 4750 2800
Wire Wire Line
	4950 2650 4850 2650
Wire Wire Line
	4850 2650 4850 2350
Connection ~ 4850 2350
$Comp
L GND #PWR23
U 1 1 5961260B
P 4850 3850
F 0 "#PWR23" H 4850 3600 50  0001 C CNN
F 1 "GND" H 4850 3700 50  0000 C CNN
F 2 "" H 4850 3850 50  0000 C CNN
F 3 "" H 4850 3850 50  0000 C CNN
	1    4850 3850
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky_ALT D4
U 1 1 59613B9A
P 6450 2200
F 0 "D4" H 6550 2100 50  0000 C CNN
F 1 "SS34" H 6300 2100 50  0000 C CNN
F 2 "Diodes_SMD:D_SMB_Handsoldering" H 6450 2200 50  0001 C CNN
F 3 "" H 6450 2200 50  0001 C CNN
	1    6450 2200
	0    1    1    0   
$EndComp
$Comp
L D_Schottky_ALT D5
U 1 1 59613BF4
P 6450 3100
F 0 "D5" H 6550 3000 50  0000 C CNN
F 1 "SS34" H 6300 3000 50  0000 C CNN
F 2 "Diodes_SMD:D_SMB_Handsoldering" H 6450 3100 50  0001 C CNN
F 3 "" H 6450 3100 50  0001 C CNN
	1    6450 3100
	0    1    1    0   
$EndComp
$Comp
L +BATT #PWR13
U 1 1 59613C4E
P 6450 1950
F 0 "#PWR13" H 6450 1800 50  0001 C CNN
F 1 "+BATT" H 6450 2090 50  0000 C CNN
F 2 "" H 6450 1950 50  0001 C CNN
F 3 "" H 6450 1950 50  0001 C CNN
	1    6450 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2350 6450 2950
Wire Wire Line
	6450 1950 6450 2050
Connection ~ 6450 2500
$Comp
L D_Schottky_ALT D7
U 1 1 59613D07
P 6850 3100
F 0 "D7" H 6950 3000 50  0000 C CNN
F 1 "SS34" H 6700 3000 50  0000 C CNN
F 2 "Diodes_SMD:D_SMB_Handsoldering" H 6850 3100 50  0001 C CNN
F 3 "" H 6850 3100 50  0001 C CNN
	1    6850 3100
	0    1    1    0   
$EndComp
$Comp
L D_Schottky_ALT D6
U 1 1 59613D5C
P 6850 2200
F 0 "D6" H 6950 2100 50  0000 C CNN
F 1 "SS34" H 6700 2100 50  0000 C CNN
F 2 "Diodes_SMD:D_SMB_Handsoldering" H 6850 2200 50  0001 C CNN
F 3 "" H 6850 2200 50  0001 C CNN
	1    6850 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 1950 6850 2050
Wire Wire Line
	6850 2350 6850 2950
Connection ~ 6850 2800
$Comp
L GND #PWR26
U 1 1 59613ED4
P 6850 3850
F 0 "#PWR26" H 6850 3600 50  0001 C CNN
F 1 "GND" H 6850 3700 50  0000 C CNN
F 2 "" H 6850 3850 50  0000 C CNN
F 3 "" H 6850 3850 50  0000 C CNN
	1    6850 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR25
U 1 1 59613EF7
P 6450 3850
F 0 "#PWR25" H 6450 3600 50  0001 C CNN
F 1 "GND" H 6450 3700 50  0000 C CNN
F 2 "" H 6450 3850 50  0000 C CNN
F 3 "" H 6450 3850 50  0000 C CNN
	1    6450 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3250 6450 3850
Wire Wire Line
	6850 3250 6850 3850
$Comp
L +BATT #PWR14
U 1 1 59614176
P 6850 1950
F 0 "#PWR14" H 6850 1800 50  0001 C CNN
F 1 "+BATT" H 6850 2090 50  0000 C CNN
F 2 "" H 6850 1950 50  0001 C CNN
F 3 "" H 6850 1950 50  0001 C CNN
	1    6850 1950
	1    0    0    -1  
$EndComp
$Comp
L L298 U2
U 1 1 59611B53
P 5600 2700
F 0 "U2" H 5250 3400 60  0000 C CNN
F 1 "L298" H 5950 3400 60  0000 C CNN
F 2 "lib:powerso-20" H 5950 2150 60  0001 C CNN
F 3 "" H 5950 2150 60  0001 C CNN
	1    5600 2700
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 5963D0B1
P 2250 2750
F 0 "C6" H 2275 2850 50  0000 L CNN
F 1 "1uF" H 2275 2650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2288 2600 50  0001 C CNN
F 3 "" H 2250 2750 50  0000 C CNN
	1    2250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 2500 2250 2500
Wire Wire Line
	2000 2500 2000 2400
Wire Wire Line
	2250 2500 2250 2600
Connection ~ 2000 2500
Wire Wire Line
	2000 3100 2000 3000
Wire Wire Line
	1750 3000 2250 3000
Wire Wire Line
	2250 3000 2250 2900
Wire Wire Line
	1750 3000 1750 2900
Connection ~ 2000 3000
Wire Notes Line
	1350 3450 2650 3450
Wire Notes Line
	2650 3450 2650 2200
Wire Notes Line
	2650 2200 1350 2200
Wire Notes Line
	1350 2200 1350 3450
$Comp
L CP C15
U 1 1 59641919
P 1750 2750
F 0 "C15" H 1775 2850 50  0000 L CNN
F 1 "100uF" H 1775 2650 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D6.3mm_P2.50mm" H 1788 2600 50  0001 C CNN
F 3 "" H 1750 2750 50  0000 C CNN
	1    1750 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2950 4850 2950
Wire Wire Line
	4850 2950 4850 3850
$Comp
L C C1
U 1 1 5999AA64
P 3000 2750
F 0 "C1" H 3025 2850 50  0000 L CNN
F 1 "1uF" H 3025 2650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3038 2600 50  0001 C CNN
F 3 "" H 3000 2750 50  0001 C CNN
	1    3000 2750
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR20
U 1 1 5999AA9F
P 3000 2500
F 0 "#PWR20" H 3000 2350 50  0001 C CNN
F 1 "+5V" H 3000 2640 50  0000 C CNN
F 2 "" H 3000 2500 50  0001 C CNN
F 3 "" H 3000 2500 50  0001 C CNN
	1    3000 2500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR21
U 1 1 5999AAC5
P 3000 3000
F 0 "#PWR21" H 3000 2750 50  0001 C CNN
F 1 "GND" H 3000 2850 50  0000 C CNN
F 2 "" H 3000 3000 50  0000 C CNN
F 3 "" H 3000 3000 50  0000 C CNN
	1    3000 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2600 3000 2500
Wire Wire Line
	3000 2900 3000 3000
$EndSCHEMATC
