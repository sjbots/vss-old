EESchema Schematic File Version 2
LIBS:vss-rescue
LIBS:Regulators
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:RF_OEM
LIBS:boosterpack
LIBS:DRV8872
LIBS:nRF24L01
LIBS:stm32f303k8
LIBS:H-Bridge
LIBS:MC34063
LIBS:switches
LIBS:atmega328p-dip
LIBS:vss-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +3.3V #PWR38
U 1 1 5912B0AE
P 4750 3450
F 0 "#PWR38" H 4750 3300 50  0001 C CNN
F 1 "+3.3V" H 4750 3590 50  0000 C CNN
F 2 "" H 4750 3450 50  0000 C CNN
F 3 "" H 4750 3450 50  0000 C CNN
	1    4750 3450
	1    0    0    -1  
$EndComp
$Comp
L nRF24L01+ U5
U 1 1 5912B0B4
P 5850 3750
F 0 "U5" H 5850 3450 50  0000 C CNN
F 1 "nRF24L01+" H 5850 4050 50  0000 C CNN
F 2 "lib:NRF24L01" H 5850 3850 50  0001 C CNN
F 3 "DOCUMENTATION" H 5850 3700 50  0001 C CNN
	1    5850 3750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR39
U 1 1 5912B0BB
P 5000 3450
F 0 "#PWR39" H 5000 3200 50  0001 C CNN
F 1 "GND" H 5000 3300 50  0000 C CNN
F 2 "" H 5000 3450 50  0000 C CNN
F 3 "" H 5000 3450 50  0000 C CNN
	1    5000 3450
	-1   0    0    1   
$EndComp
Text HLabel 4900 3850 0    60   Input ~ 0
CE
Text HLabel 4900 4000 0    60   Input ~ 0
CSN
Text HLabel 6800 4000 2    60   Input ~ 0
IRQ
Text HLabel 6800 3850 2    60   Input ~ 0
MISO
Text HLabel 6800 3700 2    60   Input ~ 0
MOSI
Text HLabel 6800 3550 2    60   Input ~ 0
SCK
Wire Wire Line
	6600 3550 6800 3550
Wire Wire Line
	6600 3700 6800 3700
Wire Wire Line
	6800 3850 6600 3850
Wire Wire Line
	6600 4000 6800 4000
Wire Wire Line
	5100 4000 4900 4000
Wire Wire Line
	4900 3850 5100 3850
Wire Wire Line
	5100 3700 4750 3700
Wire Wire Line
	5000 3550 5000 3450
Wire Wire Line
	4750 3700 4750 3450
Wire Wire Line
	5100 3550 5000 3550
$EndSCHEMATC
