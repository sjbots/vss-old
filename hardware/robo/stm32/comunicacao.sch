EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "VSS"
Date "2020-03-29"
Rev "REV1"
Comp "IFSP - Campus São José dos Campos"
Comment1 "Instituto Federal de Educação, Ciência e Tecnologia do Estado de São Paulo"
Comment2 "Prof. Colombo - colombo.junior@ifsp.edu.br"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L vss-rescue:+3.3V #PWR04
U 1 1 5C368155
P 4500 3250
F 0 "#PWR04" H 4500 3100 50  0001 C CNN
F 1 "+3.3V" H 4500 3390 50  0000 C CNN
F 2 "" H 4500 3250 50  0000 C CNN
F 3 "" H 4500 3250 50  0000 C CNN
	1    4500 3250
	1    0    0    -1  
$EndComp
$Comp
L nRF24L01:nRF24L01+ U2
U 1 1 5C36815B
P 5600 3550
F 0 "U2" H 5600 3250 50  0000 C CNN
F 1 "nRF24L01+" H 5600 3850 50  0000 C CNN
F 2 "lib:NRF24L01" H 5600 3650 50  0001 C CNN
F 3 "DOCUMENTATION" H 5600 3500 50  0001 C CNN
	1    5600 3550
	1    0    0    -1  
$EndComp
$Comp
L vss-rescue:GND #PWR05
U 1 1 5C368162
P 4750 3250
F 0 "#PWR05" H 4750 3000 50  0001 C CNN
F 1 "GND" H 4750 3100 50  0000 C CNN
F 2 "" H 4750 3250 50  0000 C CNN
F 3 "" H 4750 3250 50  0000 C CNN
	1    4750 3250
	-1   0    0    1   
$EndComp
Text HLabel 4650 3650 0    60   Input ~ 0
NRF_CE
Text HLabel 4650 3800 0    60   Input ~ 0
NRF_CSN
Text HLabel 6550 3800 2    60   Output ~ 0
NRF_IRQ
Text HLabel 6550 3650 2    60   Output ~ 0
NRF_MISO
Text HLabel 6550 3500 2    60   Input ~ 0
NRF_MOSI
Text HLabel 6550 3350 2    60   Input ~ 0
NRF_SCK
Wire Wire Line
	6350 3350 6550 3350
Wire Wire Line
	6350 3500 6550 3500
Wire Wire Line
	6550 3650 6350 3650
Wire Wire Line
	6350 3800 6550 3800
Wire Wire Line
	4850 3800 4650 3800
Wire Wire Line
	4650 3650 4850 3650
Wire Wire Line
	4850 3500 4500 3500
Wire Wire Line
	4750 3350 4750 3250
Wire Wire Line
	4500 3500 4500 3250
Wire Wire Line
	4850 3350 4750 3350
$Comp
L vss-rescue:+3.3V #PWR07
U 1 1 5CB3AADF
P 5600 4200
F 0 "#PWR07" H 5600 4050 50  0001 C CNN
F 1 "+3.3V" H 5600 4340 50  0000 C CNN
F 2 "" H 5600 4200 50  0000 C CNN
F 3 "" H 5600 4200 50  0000 C CNN
	1    5600 4200
	1    0    0    -1  
$EndComp
$Comp
L vss-rescue:GND #PWR08
U 1 1 5CB3AAF6
P 5600 4800
F 0 "#PWR08" H 5600 4550 50  0001 C CNN
F 1 "GND" H 5600 4650 50  0000 C CNN
F 2 "" H 5600 4800 50  0000 C CNN
F 3 "" H 5600 4800 50  0000 C CNN
	1    5600 4800
	1    0    0    -1  
$EndComp
$Comp
L vss-rescue:C C?
U 1 1 5CB3ACE4
P 5600 4500
AR Path="/583B6F17/5CB3ACE4" Ref="C?"  Part="1" 
AR Path="/5C41CF0A/5CB3ACE4" Ref="C2"  Part="1" 
F 0 "C2" H 5750 4550 50  0000 L CNN
F 1 "1uF" H 5750 4450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5638 4350 50  0001 C CNN
F 3 "" H 5600 4500 50  0001 C CNN
	1    5600 4500
	1    0    0    -1  
$EndComp
$Comp
L vss-rescue:C C?
U 1 1 5CB3B22C
P 6050 4500
AR Path="/583B6F17/5CB3B22C" Ref="C?"  Part="1" 
AR Path="/5C41CF0A/5CB3B22C" Ref="C3"  Part="1" 
F 0 "C3" H 6200 4550 50  0000 L CNN
F 1 "1uF" H 6200 4450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6088 4350 50  0001 C CNN
F 3 "" H 6050 4500 50  0001 C CNN
	1    6050 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4350 6050 4300
Wire Wire Line
	5600 4300 5600 4350
Wire Wire Line
	5600 4650 5600 4700
Wire Wire Line
	6050 4700 6050 4650
$Comp
L vss-rescue:C C?
U 1 1 5CB3D0B7
P 5050 4500
AR Path="/583B6F17/5CB3D0B7" Ref="C?"  Part="1" 
AR Path="/5C41CF0A/5CB3D0B7" Ref="C1"  Part="1" 
F 0 "C1" H 5200 4550 50  0000 L CNN
F 1 "100nF" H 5200 4450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5088 4350 50  0001 C CNN
F 3 "" H 5050 4500 50  0001 C CNN
	1    5050 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4300 5050 4350
Connection ~ 5600 4300
Wire Wire Line
	5050 4650 5050 4700
Connection ~ 5600 4700
Wire Wire Line
	5600 4300 6050 4300
Wire Wire Line
	5600 4700 6050 4700
Wire Wire Line
	5600 4800 5600 4700
Wire Wire Line
	5600 4300 5600 4200
Wire Wire Line
	6550 4350 6550 4300
Wire Wire Line
	6550 4300 6050 4300
Connection ~ 6050 4300
Wire Wire Line
	6050 4700 6550 4700
Wire Wire Line
	6550 4700 6550 4650
Connection ~ 6050 4700
$Comp
L vss-rescue:CP C?
U 1 1 5E2D7ABE
P 6550 4500
AR Path="/5884E5A9/5E2D7ABE" Ref="C?"  Part="1" 
AR Path="/5E2D7ABE" Ref="C?"  Part="1" 
AR Path="/5CB45FAB/5E2D7ABE" Ref="C?"  Part="1" 
AR Path="/5C41CF0A/5E2D7ABE" Ref="C19"  Part="1" 
F 0 "C19" H 6700 4550 50  0000 L CNN
F 1 "10uF" H 6700 4450 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 6588 4350 50  0001 C CNN
F 3 "" H 6550 4500 50  0000 C CNN
	1    6550 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4300 5600 4300
Wire Wire Line
	5050 4700 5600 4700
$EndSCHEMATC
