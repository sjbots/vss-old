#ifndef MANAGER_H_
#define MANAGER_H_

#include "../Drivers/NRF/nRF24L01.h"
#include "../Drivers/NRF/RF24.h"

/**
 * Portable functions
 *
 * These are the required low level functions
 *
 * To port this library to another MCU, it is only required that
 * these funcitons work as their descriptions
 */

/**
 * bool uartRead(uint8_t *buffer, uint8_t bytesNumber)
 *
 * This function reads bytesNumber from UART. This function has also a timeout, i.e.,
 * it will wait timeout ms and then drop
 *
 * @param buffer is the address of a buffer to store read data
 * @param bytesNumber is the number of bytes to read
 * @return a bool variable, true if reading was sucessful or false if failed
 */
bool uartRead(uint8_t* buffer, uint8_t bytesNumber);

/**
 * uartWrite(uint8_t *buffer, uint8_t bytesNumber)
 *
 * @param buffer is the address of a buffer with data to send
 * @param bytesNumber is the number of bytes to read
 */
void uartWrite(uint8_t* buffer, uint8_t bytesNumber);


/**
 * nRFwork(RF24 &radio)
 *
 * This function performs all the heavy work
 */
void nRFwork(RF24_t* radio);

#endif /* MANAGER_H_ */
