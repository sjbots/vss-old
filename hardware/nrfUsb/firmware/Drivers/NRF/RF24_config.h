
/*
 Copyright (C) 2011 J. Coliz <maniacbug@ymail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

/* spaniakos <spaniakos@gmail.com>
 Added __ARDUINO_X86__ support
*/

#ifndef __RF24_CONFIG_H__
#define __RF24_CONFIG_H__

/*** USER DEFINES:  ***/
#define MINIMAL

/**********************/
#define rf24_max(a, b) (a>b?a:b)
#define rf24_min(a, b) (a<b?a:b)

#include <string.h>
#include "stm32f1xx_hal.h"

#define strlen_P strlen
#define PROGMEM
#define pgm_read_byte(addr) (*(const unsigned char *)(addr))
#define pgm_read_word(p) (*(p))

#define _BV(bit) (1<<(bit))

#define delay(x) HAL_Delay(x)

void delay1Us();

void _delay_us(uint16_t us);

#define delayMicroseconds(x) _delay_us(x)

#endif // __RF24_CONFIG_H__

