//#include "Manager.h"
//
//bool uartRead(uint8_t *buffer, uint8_t bytesNumber) {
//    bool success;
//    int charactersReaded = Serial.readBytes(buffer, bytesNumber);
//    if (charactersReaded == 0)
//        success = false;
//    else
//        success = true;
//
//    return success;
//}
//
//void uartWrite(uint8_t *buffer, uint8_t bytesNumber) {
//    unsigned int i;
//    for (i = 0; i < bytesNumber; i++)
//        Serial.write(buffer[i]);
//}
//
//
//void nRFwork(RF24* radio) {
//    uint8_t whatToDo = 0;
//    uartRead(&whatToDo, 1);
//
//    /**
//     * void setChannel (uint8_t channel)
//     */
//    if (whatToDo == 1) {
//        uint8_t channel;
//        bool success = uartRead(&channel, 1);
//
//        if (success == true)
//            radio.setChannel(channel);
//    }
//
//        /**
//         * uint8_t getChannel (void)
//         */
//    else if (whatToDo == 2) {
//        uint8_t channel = radio.getChannel();
//        uartWrite(&channel, 1);
//    }
//
//        /**
//         * void setPayloadSize (uint8_t size)
//         *
//         * This function need to read 1 byte from UART
//         */
//    else if (whatToDo == 3) {
//        uint8_t payloadSize;
//        bool success = uartRead(&payloadSize, 1);
//
//        if (success == true)
//            radio.setPayloadSize(payloadSize);
//    }
//
//        /**
//         * uint8_t getPayloadSize (void)
//         */
//    else if (whatToDo == 4) {
//        uint8_t payloadSize = radio.getPayloadSize();
//        uartWrite(&payloadSize, 1);
//    }
//
//        /**
//         * uint8_t getDynamicPayloadSize (void)
//         */
//    else if (whatToDo == 5) {
//        uint8_t dynamicPayloadSize = radio.getDynamicPayloadSize();
//        uartWrite(&dynamicPayloadSize, 1);
//    }
//
//        /**
//         * void enableAckPayload (void)
//         */
//    else if (whatToDo == 6) {
//        radio.enableAckPayload();
//    }
//
//        /**
//         * void enableDynamicPayloads (void)
//         */
//    else if (whatToDo == 7) {
//        radio.enableDynamicPayloads();
//    }
//
//        /**
//         * void enableDynamicAck ()
//         */
//    else if (whatToDo == 8) {
//        radio.enableDynamicAck();
//    }
//
//        /**
//         * bool isPVariant (void)
//         */
//    else if (whatToDo == 9) {
//        uint8_t isPVariant = (uint8_t) radio.isPVariant();
//        uartWrite(&isPVariant, 1);
//    }
//
//        /**
//         * Receives at least 2 more bytes: [numberOfPackets, enable, pipe]
//         *
//         * void setAutoAck (bool enable)
//         * void setAutoAck (uint8_t pipe, bool enable)
//         */
//    else if (whatToDo == 10) {
//        uint8_t numberOfPackets;
//        bool success = uartRead(&numberOfPackets, 1);
//
//        // void setAutoAck (bool enable)
//        if (success == true) {
//            if (numberOfPackets == 1) {
//                uint8_t enable;
//                success = uartRead(&enable, 1);
//
//                if (success == true)
//                    radio.setAutoAck((bool) enable);
//            }
//
//                // void setAutoAck (uint8_t pipe, bool enable)
//            else {
//                uint8_t data[2];
//                success = uartRead(data, 2);
//
//                if (success == true)
//                    radio.setAutoAck(data[1], (bool) data[0]);
//            }
//        }
//    }
//
//        /**
//         * void setPALevel (uint8_t level)
//         *
//         * This function receives 1 more byte with information of the power level
//         */
//    else if (whatToDo == 11) {
//        uint8_t level;
//        bool success = uartRead(&level, 1);
//
//        if (success == true) {
//            if (level == 0)
//                radio.setPALevel(RF24_PA_MIN);
//            else if (level == 1)
//                radio.setPALevel(RF24_PA_LOW);
//            else if (level == 2)
//                radio.setPALevel(RF24_PA_HIGH);
//            else
//                radio.setPALevel(RF24_PA_MAX);
//        }
//    }
//
//        /**
//         * uint8_t getPALevel (void)
//         */
//    else if (whatToDo == 12) {
//        uint8_t whatToSend, level = radio.getPALevel();
//        if (level == RF24_PA_MIN)
//            whatToSend = 0;
//        else if (level == RF24_PA_LOW)
//            whatToSend = 1;
//        else if (level == RF24_PA_HIGH)
//            whatToSend = 2;
//        else
//            whatToSend = 3;
//
//        uartWrite(&whatToSend, 1);
//    }
//
//        /**
//         * bool setDataRate (rf24_datarate_e speed)
//         *
//         * This function receives 1 more byte with dataRate information
//         *
//         * * 0 means 250 KBPS
//         * * 1 means 1MBPS
//         * * 2 means 2MBPS
//         */
//    else if (whatToDo == 13) {
//        uint8_t dataRate;
//        bool success = uartRead(&dataRate, 1);
//
//        if (success == true) {
//            if (dataRate == 0)
//                radio.setDataRate(RF24_250KBPS);
//            else if (dataRate == 1)
//                radio.setDataRate(RF24_1MBPS);
//            else
//                radio.setDataRate(RF24_2MBPS);
//        }
//    }
//
//        /**
//         * rf24_datarate_e getDataRate (void)
//         *
//         * This function returns 1 byte with dataRate information:
//         *
//         * * 0 means 250 KBPS
//         * * 1 means 1MBPS
//         * * 2 means 2MBPS
//         */
//    else if (whatToDo == 14) {
//        rf24_datarate_e speed = radio.getDataRate();
//        uint8_t whatToSend;
//
//        if (speed == RF24_250KBPS)
//            whatToSend = 0;
//        else if (speed == RF24_1MBPS)
//            whatToSend = 1;
//        else
//            whatToSend = 2;
//
//        uartWrite(&whatToSend, 1);
//    }
//
//        /**
//         * void setCRCLength (rf24_crclength_e length)
//         *
//         * This function receives 1 more byte, with CRC length information
//         *
//         * * 0 means CRC DISABLED
//         * * 8 means CRC with length 8
//         * * 16 means CRC with legnth 16
//         */
//    else if (whatToDo == 15) {
//        uint8_t len;
//        bool success = uartRead(&len, 1);
//
//        if (success == true) {
//            if (len == 0)
//                radio.setCRCLength(RF24_CRC_DISABLED);
//            else if (len == 8)
//                radio.setCRCLength(RF24_CRC_8);
//            else
//                radio.setCRCLength(RF24_CRC_16);
//        }
//    }
//
//        /**
//         * rf24_crclength_e getCRCLength (void)
//         *
//         * This function sends 1 byte, with CRC length information
//         *
//         * * 0 means CRC DISABLED
//         * * 8 means CRC with length 8
//         * * 16 means CRC with legnth 16
//         */
//    else if (whatToDo == 16) {
//        rf24_crclength_e length = radio.getCRCLength();
//        uint8_t whatToSend;
//
//        if (length == RF24_CRC_DISABLED)
//            whatToSend = 0;
//        else if (length == RF24_CRC_8)
//            whatToSend = 8;
//        else
//            whatToSend = 16;
//
//        uartWrite(&whatToSend, 1);
//    }
//
//        /**
//         * void disableCRC (void)
//         */
//    else if (whatToDo == 17) {
//        radio.disableCRC();
//    }
//
//        /**
//         * void maskIRQ (bool tx_ok, bool tx_fail, bool rx_ready)
//         *
//         * This function receives 1 more byte
//         */
//    else if (whatToDo == 18) {
//        uint8_t mask;
//        bool success = uartRead(&mask, 1);
//
//        if (success == true) {
//            bool tx_ok = (bool) ((mask & 0b00000100) >> 2);
//            bool tx_fail = (bool) ((mask & 0b00000010) >> 1);
//            bool rx_ready = (bool) (mask & 0b00000001);
//            radio.maskIRQ(tx_ok, tx_fail, rx_ready);
//        }
//    }
//
//        /**
//         * void startListening (void)
//         */
//    else if (whatToDo == 19) {
//        radio.startListening();
//    }
//
//        /**
//         * void stopListening (void)
//         */
//    else if (whatToDo == 20) {
//        radio.stopListening();
//    }
//
//        /**
//         * bool available (void)
//         */
//    else if (whatToDo == 21) {
//        uint8_t isAvailable = (uint8_t) radio.available();
//        uartWrite(&isAvailable, 1);
//    }
//
//        /**
//         * bool available (uint8_t *pipe_num)
//         */
//    else if (whatToDo == 22) {
//        uint8_t pipe_num;
//        uint8_t isAvailable = (uint8_t) radio.available(&pipe_num);
//
//        uartWrite(&isAvailable, 1);
//        uartWrite(&pipe_num, 1);
//    }
//
//        /**
//         * void read (void *buf, uint8_t len)
//         *
//         * This function receives 1 more byte with the message length
//         *
//         * This function can read a maximum of 32 bytes
//         */
//    else if (whatToDo == 23) {
//        uint8_t len;
//        bool success = uartRead(&len, 1);
//
//        if ((success == true) && (len <= 32)) {
//            unsigned char rx_data[32];
//            radio.read(rx_data, len);
//
//            uartWrite(rx_data, len);
//        }
//    }
//
//        /**
//         * bool write (const void *buf, uint8_t len)
//         *
//         * This function receives 1 byte with number of bytes of the payLoad
//         *
//         * The maximum payLoad size that this function receives is 32 bytes
//         *
//         * This function also returns an error signal:
//         * * 0 means radio couln't delivery message (but UART was OK)
//         * * 1 means everything went good
//         * * 2 means nothing was written in UART
//         */
//    else if (whatToDo == 24) {
//        // How many bytes to send?
//        uint8_t len, error;
//        bool success = uartRead(&len, 1);
//
//        if (success == true) {
//            // Receives the message
//            unsigned char tx_data[32];
//            success = uartRead(tx_data, len);
//
//            if (success == true) {
//                radio.stopListening();
//                uint8_t OK = (uint8_t) radio.write(tx_data, len);
//                uartWrite(&OK, 1);
//                radio.startListening();
//            } else {
//                error = 2;
//                uartWrite(&error, 1);
//            }
//        } else {
//            error = 2;
//            uartWrite(&error, 1);
//        }
//    }
//
//        /**
//         * void openWritingPipe (const uint8_t *address)
//         *
//         * This function must receive more 5 bytes with the address
//         *
//         */
//    else if (whatToDo == 25) {
//        unsigned char address[5];
//        bool success = uartRead(address, 5);
//
//        if (success == true)
//            radio.openWritingPipe(address);
//    }
//
//        /**
//         * void openReadingPipe (uint8_t number, const uint8_t *address)
//         *
//         * This function must receive more 1 byte with what pipe to open (must be a number between 0 - 5)
//         * This function must receive more 5 bytes with the address
//         */
//    else if (whatToDo == 26) {
//        uint8_t number;
//        bool success = uartRead(&number, 1);
//
//        if (success == true) {
//            unsigned char address[5];
//            success = uartRead(address, 1);
//            if (success == true)
//                radio.openReadingPipe(number, address);
//        }
//    }
//
//        /**
//         * Set UART timeout
//         *
//         * This function receives 1 more byte, with the timout in ms
//         */
//    else if (whatToDo == 27) {
//        uint8_t timeout = 5;
//        bool success = uartRead(&timeout, 1);
//
//        if (success)
//            Serial.setTimeout(timeout);
//        else
//            Serial.setTimeout(5);
//    }
//
//
//}
