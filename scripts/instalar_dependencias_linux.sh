#!/bin/sh

# Instala dependências gerais
sudo apt-get install gcc-avr binutils-avr avr-libc \
                     build-essential libopenblas-dev libopenblas-base \
                     git \
                     python3-pip python3-dev libpython3-dev python-virtualenv -y

# Instala as dependências para compilar o pacote scipy (necessário para compilar o slycot)
sudo apt-get build-dep python-scipy -y

sudo pip3 install numpy --upgrade
sudo pip3 install scipy --upgrade
sudo pip3 install python-opencv --upgrade
sudo pip3 install matplotlib --upgrade
sudo pip3 install ipython --upgrade
sudo pip3 install jupyter --upgrade
sudo pip3 install sympy --upgrade
sudo pip3 install PyQt5 --upgrade
sudo pip3 install slycot --upgrade 
sudo pip3 install control --upgrade 
sudo pip3 install sphinx_rtd_theme --upgrade

