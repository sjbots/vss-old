Primeiros passos
=================

Antes de começar é necessário combinarmos algumas coisas. Trabalhar em equipe não é fácil: cada pessoa tem sua forma
preferida de trabalhar. Porém, robótica é ainda mais difícil. Trata-se uma área de pesquisa multi-disciplinar que
requer muita técnica, teoria e, principalmente, dedicação. Desse modo, precisamos definir algumas (poucas) coisas para
facilitar a  integração dos trabalhos desenvolvidos.

Esse documento tem por objetivo apresentar ao novato todos os passos que o grupo de robótica SJbots já tomou. Cada
integrante do grupo reporta nesse documento o trabalho que desenvolveu dentro do grupo. Essa é nossa primeira e mais
importante regra: **documentar tudo!**

Nessa página estão apresentados alguns pacotes computacionais que convencionamos utilizar. É recomendável manter esse
padrão para ser mais fácil conseguir ajuda. Em outras palavras, a chance dos membros mais velhos da equipe, com mais
experiência, ajudarem você é maior.


Linguagens de programação adotadas
-----------------------------------

De modo geral, na robótica, é necessário escrever código para rodar no:

* no computador (geralmente processador com com vários núcleos rodando a ~3 GHz)
* no robô (geralmente microcontrolador com um núcleo rodando a ~16 MHz)

É evidente que o computador dispõe de uma capacidade de processamento muito maior que o robô. Por esse motivo,
a escolha da linguagem de programação precisam ser feita com muito cuidado. As linguagens adotadas e os motivos para
a escolha de cada uma delas são apresentados a seguir.

No computador
~~~~~~~~~~~~~~~

A linguagem de programação adotada é o Python. Ele foi escolhido pelos seguintes motivos:

* É multiplataforma
* A sintaxe é mais simples que a do C++
* O gerenciamento das dependências (isto é, os pacotes adicionais necessários além do Python) é feito muito facilmente

A principal desvantagem do Python é ser uma linguagem interpretada. Logo, é muito mais lenta que C/C++, por exemplo.
Porém, esse inconveniente pode ser contornado mesclando Python com C++ (*binding*). Isto é, as partes 'pesadas' são
escritas em C++ e chamadas do Python. Isso pode ser feito de duas formas:

* automática (usando a biblioteca Python numba)
* manual (escrevendo código C++ e criando uma DLL que é chamável pelo Python com a ajuda do PyBind, uma biblioteca C++)

No momento da escrita desse texto, há duas principais versões do Python: a 2 e a 3.
A nova versão (isto é, a 3) foi feita com a intenção de ser incompatível com a 2, que será descontinuada.
Logo, adotou-se a versão 3.

No robô
~~~~~~~~

Nesse caso, as CPUs são microcontroladores, que em geral são mais lentos que os microprocessadores.
Por esse motivo, a linguagem de programação adotada deve ser capaz de prover o melhor desempenho possível.
Assim, adotou-se a linguagem C++.

IDE adotada
------------

Para o código que roda no computador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Recomenda-se a IDE PyCharm, da Jet Brains.
Embora não seja software livre, há uma versão grátis que pode ser obtida diretamente do site desse produto.
Trata-se da versão "Community".

:doc:`Clique aqui <configuracoes/pycharm>` para ver algumas configurações adicionais necessárias para essa interface.

Cabe salientar que outras IDEs também podem ser utilizadas, tais como:

- `Spyder <https://www.spyder-ide.org/>`_
- `Visual Studio Code <https://code.visualstudio.com/>`_
- `Atom <https://atom.io/>`_

Para o código que roda no robô
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Adotou-se o `TrueStudio, da Atollic <https://atollic.com/truestudio/>`_, que é baseado no Eclipse.
Essa IDE foi desenvolvida para trabalhar com os microcontroladores da ST.

No momento da escrita desse texto, a placa eletrônica dos robôs utiliza os microcontroladores da ATmega.

Pacotes computacionais adicionais
----------------------------------

Além do Python 3, faz-se necessária a instalação de alguns pacotes
computacionais para extender os recursos dessa linguagem. São eles:

* `IPython <https://ipython.org/>`_: esse pacote permite a
  executar código Python diretamente do browser. Ele também permite
  adicionar código/texto LaTeX. Desse modo, é bastante interessante
  para reproduzir o desenvolvimento matemático dos artigos utilizados
  como base desse projeto
* `Sympy <https://www.sympy.org>`_: permite definir variáveis
  simbólicas, isto é, a variável *x* pode ser manipulada explicitamente,
  não sendo necessário atribuir valores a ela. Esse pacote é mais
  utilizado em conjunto do IPython, ou seja, para reproduzir o
  desenvolvimento matemático dos artigos aqui utilizados.
* `OpenCV <https://opencv.org/>`_:
* `NumPy <http://www.numpy.org/>`_:
* `SciPy <https://www.scipy.org/>`_:
* `Matplotlib <https://matplotlib.org/>`_:
* `PyQt5 <https://pypi.org/project/PyQt5/>`_:
* `Sphinx <http://www.sphinx-doc.org/>`_:
* `Numba <https://numba.pydata.org/>`_:

Gerenciamento de código fonte
------------------------------

Quando há várias pessoas trabalhando num mesmo projeto, é fundamental utilizar uma ferramenta para gerenciamento de
código. Essa ferramenta provê muita segurança para a equipe, pois ela armazena **quais mudanças** (no código) foram
feitas por **quem** e **quando**.

Nós adotamos o `git <https://git-scm.com/>`_, que é suportado nativamente pelas IDEs PyCharm e TrueStudio.
No ambiente Microsoft Windows, existe uma interface gráfica muito amigável para o git, o
`TortoiseGit <https://tortoisegit.org/>`_.

Desenhos mecânicos
-------------------


Diagramas eletrônicos
----------------------



