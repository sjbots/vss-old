.. GRIF documentation master file, created by
   sphinx-quickstart on Sat Aug 25 06:58:48 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem-vindo à documentação do grupo de robótica sjbots!
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Conteúdos:
   
   primeiros_passos
   instalacao
   treinamento/treinamento
   codigo
   robos/main
   membros/membros



Índices e tabelas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
