:orphan:

Configurações extras da IDE PyCharm
=====================================

Correção ortográfica
----------------------

Por padrão o PyCharm vem apenas com corretor ortográfico em inglês. Para habilitar a correção em PT-BR, siga os
seguintes passos (estou assumindo que você clonou o repositório da equipe):

#.  Abra o PyCharm

#.  Clique em *File* e em seguida *Settings*

#.  Na caixa que abrir, localizado na esquerda clique em *Editor* e em seguida *Spelling*

#.  Nos botões da direita, clique no '+' e então navegue até a pasta do repositório da equipe

#.  Selecione todos os dicionários (arquivos .dic) que estão na pasta **documentacao/dicionario_ptbr**, conforme
    ilustrado na figura abaixo

    .. image:: tutorial_ilustrado.png
        :width: 50%
        :align: center

#.  Finalmente clique em *Apply*


