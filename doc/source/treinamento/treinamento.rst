Treinamento
============

Aqui é onde os estudantes devem começar seus estudos.

.. toctree::
   :maxdepth: 2
   :caption: Conteúdos:

   computacao/main
   controle/main
   eletronica/main
   mecanica/main




