Controle no espaço de estados
==============================

Tempo contínuo
---------------

Considere que a dinâmica a tempo contínuo de um sistema pode ser representada pelas seguintes equações:

.. math::
    :label: eq-EspacoEstados-dx

    \dot{x}(t) = A x(t) + B u(t)

.. math::
    :label: eq-EspacoEstados-y

    y(t) = C x(t) + D u(t)

sendo :math:`A \in \mathbb{R}^{n_x \times n_x}` a matriz de estados,
:math:`B \in \mathbb{R}^{n_x \times n_u}` a matriz de controle,
:math:`C \in \mathbb{R}^{n_x \times n_y}` a matriz de saída e
:math:`D \in \mathbb{R}^{n_y \times n_u}` a matriz de transmissão direta e :math:`x(t)`,
:math:`u(t)` e :math:`y(t)` os vetores de estados, controle e saída, respectivamente.
Vale mencionar que :eq:`eq-EspacoEstados-dx` - :eq:`eq-EspacoEstados-y` são conhecidas como ``equações de dinâmica`` do
inglês *dynamic equations*.

Tempo discreto
---------------

.. math::
    :label: eq-EspacoEstados-xk1

    x(k+1) = A x(k) + B u(k)

.. math::
    :label: eq-EspacoEstados-yk

    y(k) = C x(k) + D u(k)




