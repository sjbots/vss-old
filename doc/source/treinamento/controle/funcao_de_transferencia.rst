Controle com função de transferência
=====================================

.. math::
    :label: eq-FuncaoDeTransferencia-Gs

    \begin{equation}
        G(s) = \frac{Y(s)}{U(s)}
    \end{equation}



