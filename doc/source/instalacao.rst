Instalação dos programas
=========================

Nesse arquivo são apresentados os procedimentos necessários para a 
instalação dos pacotes necessários para utilização desse projeto.

Embora seja possível baixar o `Python <http://www.python.org>`_ diretamente (e será perfeitamente funcional), a Intel
distribui gratuitamente (sendo necessário registrar-se) o Python "linkado" com a biblioteca
`MKL <https://software.intel.com/en-us/mkl>`_, apresentando desempenho computacional muito superior.
Esse pacote da Intel pode ser obtido no `link <https://software.intel.com/en-us/distribution-for-python>`_.



GNU/Linux (Debian e derivados)
-------------------------------

Aqui serão apresentadas instruções específicas para os sistemas
operacionais Microsoft Windows e GNU/Linux (baseados no Debian/Ubuntu).

Microsoft Windows
------------------


