Comunicação
============

Requisitos:

- Ser independente do hardware
- Enviar referência de velocidades para cada uma das duas rodas
- Enviar os ganhos dos dois PID
- Ler o nível da bateria no robô
- Ler as velocidades das rodas
- Ler o consumo médio de corrente
- Ler a temperatura do robô
- Ler se há algum problema com algum dos dois motores (e qual problema)
- Permitir fazer um "ping" (para medir o tempo médio necessário para a comunicação)


nRF24L01
----------

Medi o tempo de transmissão do nRF24L01, transmitindo 32 bytes

- Usando a taxa 1 MBPS foram necessários 724 us
- Usando a taxa 2 MBPS foram necessários 560 us


Endereços
----------

Fica convencionado que os endereços dos robôs serão conforme a `Tabela de endereços`_
mostrada a seguir.

.. _Tabela de endereços:

+------------+--------+--------+--------+-----------+
| Computador | Robô 1 | Robô 2 | Robô 3 | Broadcast |
+============+========+========+========+===========+
| 0_sjc      | 1_sjc  | 2_sjc  | 3_sjc  | B_sjc     |
+------------+--------+--------+--------+-----------+

Vale salientar que os endereços estão em código ASCII e portanto, devem ser declarados como variáveis (preferencialmente
constantes) do tipo texto.








