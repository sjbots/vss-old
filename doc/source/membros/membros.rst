Membros da equipe
==================

.. Instruções para preenchimento desse documento:
   1 - Copie e cole uma tabela já existente
   2 - Adicione sua foto na pasta documentacao/source/membros/figs
   3 - O nome do arquivo precisa seguir o padrão da equipe:
       3.1 - prof_apelido.jpg
       3.2 - estudante_apelido.jpg
   4 - Para que a tabela não fique gigante, use um encurtador de links para o seu currículo Lattes

- Professores:

.. _Colombo:

+-------------------------------------+--------------------------------------------------------------+
|                                     | José Roberto Colombo Junior                                  |
|                                     |                                                              |
| .. image:: figs/prof_colombo.jpg    | Membro de 2020 - hoje                                        |
|    :width: 200 px                   |                                                              |
|    :align: center                   | `<http://lattes.cnpq.br/8510243973408376>`_                  |
|                                     |                                                              |
|                                     | E-mail: colombo.junior@ifsp.edu.br                           |
+-------------------------------------+--------------------------------------------------------------+


- Estudantes:

.. _AudreyAkazawa:

+-------------------------------------+--------------------------------------------------------------+
|                                     | Audrey Marcella Akazawa                                      |
|                                     |                                                              |
| .. image:: figs/audrey_akazawa.jpg  | Membro de 2020 - hoje                                        |
|    :width: 200 px                   |                                                              |
|    :align: center                   | `<http://lattes.cnpq.br/2262557474053107>`_                  |
|                                     |                                                              |
|                                     | E-mail: audrey.akazawa@aluno.ifsp.edu.br                     |
+-------------------------------------+--------------------------------------------------------------+



.. - Ex-membros:

